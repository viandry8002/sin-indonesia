import React from 'react';
import Routes from './src/Screens/Routes';
import 'react-native-gesture-handler';

const App = () => {
  return (
    <Routes />
  );
};

export default App;
