import React from 'react';
import { StyleSheet } from 'react-native';
import colors from './Colors';

const Font = StyleSheet.create({
  // bold 700
  bold8: {
    fontSize: 8,
    color: colors.black,
    fontWeight: '700',
  },
  bold10: {
    fontSize: 10,
    color: colors.black,
    fontWeight: '700',
  },
  bold12: {
    fontSize: 12,
    color: colors.black,
    fontWeight: '700',
  },
  bold14: {
    fontSize: 14,
    color: colors.black,
    fontWeight: '700',
  },
  bold16: {
    fontSize: 16,
    color: colors.black,
    fontWeight: '700',
  },
  bold18: {
    fontSize: 18,
    color: colors.black,
    fontWeight: '700',
  },
  bold26: {
    fontSize: 26,
    color: colors.black,
    fontWeight: '700',
  },
  bold28: {
    fontSize: 28,
    color: colors.black,
    fontWeight: '700',
  },
  // semi bold 600
  semiBold11: {
    fontSize: 11,
    color: colors.black,
    fontWeight: '600',
  },
  semiBold12: {
    fontSize: 12,
    color: colors.black,
    fontWeight: '600',
  },
  semiBold14: {
    fontSize: 14,
    color: colors.black,
    fontWeight: '600',
  },
  semiBold16: {
    fontSize: 16,
    color: colors.black,
    fontWeight: '600',
  },
  // medium 500
  medium12: {
    fontSize: 12,
    color: colors.black,
    fontWeight: '500',
  },
  medium14: {
    fontSize: 14,
    color: colors.black,
    fontWeight: '500',
  },
  // regular 400
  regular10: {
    fontSize: 10,
    color: colors.black,
    fontWeight: '400',
  },
  regular11: {
    fontSize: 11,
    color: colors.black,
    fontWeight: '400',
  },
  regular12: {
    fontSize: 12,
    color: colors.black,
    fontWeight: '400',
  },
  regular14: {
    fontSize: 14,
    color: colors.black,
    fontWeight: '400',
  },
});

export default Font;
