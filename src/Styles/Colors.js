const colors = {
  primary: '#0F3777',
  primaryShading: 'rgba(15, 55, 119, 0.07)',
  greenPrimary: '#3BA172',
  greenPrimaryShading: 'rgba(59, 161, 114, 0.5)',
  white: '#FFFFFF',
  whiteShading: 'rgba(255, 255, 255, 0.3)',
  black: '#000000',
  gray6: '#F2F2F2',
  gray5: '#E0E0E0',
  gray4: '#BDBDBD',
  gray3: '#828282',
  red: '#EF3A3A',
  redShading: '#FEF1F1',
  greenContain: '#ECFDF3',
  blueShading: 'rgba(15, 55, 119, 0.07)',
  yellowShading: 'rgba(242, 201, 76, 0.1)',
  grayShading: 'rgba(0, 0, 0, 0.07)',
  success: '#12B76A',
  fullGray: '#4F4F4F',
};

export default colors;
