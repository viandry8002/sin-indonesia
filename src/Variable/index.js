// const api = 'https://demoteknologi.com/iac.api/public/';
// export const api = 'https://generasibisa.com/sin-indonesia/api/public/api';
// export const api = 'https://api-sin-indonesia.serverwan.com/api';
// export const apiAgent = 'https://api-sin-indonesia.serverwan.com/api/agent';

export const api = 'https://sinindonesia.co.id/api-sin/api';
export const apiAgent = 'https://sinindonesia.co.id/api-sin/api/agent';

export const key = 'base64:FIZ6CJfJEELfJDU0zl5ROeVW1wQhHBoYEbbSmGfrqYk=';

// public
export const postProduct = api + '/product';
// export const getStoreAddress = api + '/courier/store-address';
export const getStoreAddress = api + '/courier';
export const postShipdeoPricings = api + '/courier/shipdeo-pricings';
export const postAddCart = api + '/cart/add';
// profile
export const getProfile = api + '/user';
export const getCardAgen = api + '/user/card-agent';

export const getBanks = api + '/banks';
// configuration
export const getConfiguration = api + '/configuration';

// agen ================================

// register agen
export const postValidateStep1 = apiAgent + '/register-step1-validation';
export const getDescription = apiAgent + '/description';
export const postRegistAgen = apiAgent + '/register';
// home
export const getAgentHome = apiAgent + '/home';
// poin order
export const getAgentPoinHistory = apiAgent + '/poin-orders/history';
export const getAgentPoinRedem = apiAgent + '/poin-orders/redeem';
// cart
export const postAgentCart = apiAgent + '/cart';
export const postAgentCheckout = apiAgent + '/order/checkout';
// referal
export const getPoinReferal = apiAgent + '/poin-referrals/amount';
export const getHistoryReferal = apiAgent + '/poin-referrals/history';
export const getRedeemReferal = apiAgent + '/poin-referrals/redeem';
// Rekening
export const postBankAccount = apiAgent + '/account';