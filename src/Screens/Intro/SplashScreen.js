import React, { useEffect, useRef } from 'react'
import { Image, StatusBar, StyleSheet, SafeAreaView, Animated, ImageBackground } from 'react-native'

const SplashScreen = () => {
    const fadeIn = useRef(new Animated.Value(0)).current

    useEffect(() => {
        Animated.timing(fadeIn, {
            toValue: 1,
            duration: 3000,
            useNativeDriver: false
        }).start()
    }, [fadeIn])

    return (
        <ImageBackground style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} source={require('../../Assets/image/bgSplash.png')}>
            <StatusBar backgroundColor='transparent' translucent />
            <Animated.View style={{ opacity: fadeIn }}>
                <Image source={require('../../Assets/image/logo1.png')} style={{ width: 130, height: 130 }} />
            </Animated.View>
        </ImageBackground>
    )
}

export default SplashScreen

const styles = StyleSheet.create({})
