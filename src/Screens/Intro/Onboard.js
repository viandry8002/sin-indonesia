import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import colors from '../../Styles/Colors';
// component
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios';
import {api, key} from '../../Variable';

const Onboard = ({navigation}) => {
  const [onboard, setOnboard] = useState([]);
  const [chose, setChose] = useState([]);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Axios.get(`${api}/onboardings`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setOnboard(res.data.data);
        setChose(res.data.data[0]);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const theChose = () => {
    const dtLength = onboard.length;

    if (onboard > 0) {
      if (index + 1 === dtLength) {
        navigation.navigate('BerandaS');
      } else {
        setIndex(index + 1);
        setChose(onboard[index + 1]);
      }
    } else {
      navigation.navigate('BerandaS');
    }
  };

  return (
    <SafeAreaView style={styles.wrap}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{x: -1, y: 0}}
        end={{x: 1, y: 0}}
        style={{flex: 1}}>
        <View style={styles.wrapContent}>
          {onboard.length > 0 ? (
            <Image
              source={{uri: chose.url_file}}
              style={styles.imageStyle}
              resizeMode="contain"
            />
          ) : (
            <View style={{height: 439}} />
          )}
          <TouchableOpacity
            style={styles.TouchableStyle}
            onPress={() => theChose()}>
            <Text style={styles.textTouchable}>Berikutnya</Text>
          </TouchableOpacity>
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Onboard;

const styles = StyleSheet.create({
  wrap: {flex: 1},
  wrapContent: {flex: 1, justifyContent: 'center', marginHorizontal: 16},
  imageStyle: {width: 353, height: 439, alignSelf: 'center'},
  textTouchable: {color: colors.primary, fontSize: 14, fontWeight: 'bold'},
  TouchableStyle: {
    backgroundColor: colors.white,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginTop: 43,
  },
});
