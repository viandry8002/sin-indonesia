import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import moment from 'moment';

const Sample = () => {
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');

  useEffect(() => {
    getToken();

    let timer = setInterval(() => {
      FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
        ? clearInterval(timer)
        : setdatess(moment(endDate).diff(moment(), 'seconds'));
    }, 1000);
  }, [endDate]);

  const getToken = () => {
    setendDate('2024-03-08 09:39:07');
  };

  const FormatTime = duration => {
    var day = Math.floor(duration / 86400);
    var hours = Math.floor((duration % 86400) / 3600);
    var minutes = Math.floor((duration % 3600) / 60);
    if (day < 10) {
      day = '0' + day;
    }
    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return day + ' Hari : ' + hours + ' Jam : ' + minutes + ' Menit';
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 20, color: 'black'}}>
        {FormatTime(moment(endDate).diff(moment(), 'seconds'))}
      </Text>
    </View>
  );
};

export default Sample;

const styles = StyleSheet.create({});
