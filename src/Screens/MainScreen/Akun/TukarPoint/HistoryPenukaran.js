import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import colors from '../../../../Styles/Colors';
import { api, key } from '../../../../Variable';
import FormatMoney from '../../../MultipleComponent/FormatMoney';

const ListProduct = ({ navigation, route }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    setRefreshing(true);
    setTimeout(() => {
      getToken();
    }, 1000);
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/history-redeem`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success
          ? (setData(res.data.data),
            res.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.next_page_url))
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 3000);
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderItem = ({ item }) => (
    <View
      style={{
        flex: 1,
        margin: 8,
        backgroundColor: colors.white,
        elevation: 6,
        flexDirection: 'row',
        borderRadius: 7,
      }}>
      <Image
        source={
          item.item.file
            ? { uri: item.item.file }
            : require('../../../../Assets/image/notfound.png')
        }
        style={{ width: 117, height: 107 }}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.item.title}
        </Text>
        <Text style={styles.produkTitle} numberOfLines={2}>
          Tgl Penukaran :{' '}
          {item.created_at ? moment(item.created_at).format('DD/MM/YYYY') : '-'}
        </Text>
        <View style={styles.wrapPoint}>
          <Image
            source={require('../../../../Assets/icon/poin.png')}
            style={styles.point}
            resizeMode={'contain'}
          />
          <FormatMoney value={item.item.amount}
            style={styles.textPoint} unit={''} />
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        renderItem={renderItem}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{ alignItems: 'center' }}>
              <Text>Load more ....</Text>
            </View>
          ) : (
            <View style={{ height: 130 }} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        ListEmptyComponent={() =>
          refreshing === false ? (
            <View style={{ flex: 1, alignItems: 'center', marginTop: 200 }}>
              <Image
                source={require('../../../../Assets/image/no-data.png')}
                style={{ width: 200, height: 200 }}
                resizeMode={'contain'}
              />
              <Text style={styles.subtitle}>Data tidak ditemukan</Text>
            </View>
          ) : (
            false
          )
        }
      />
    </SafeAreaView>
  );
};

export default ListProduct;

const styles = StyleSheet.create({
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 1,
    margin: 8,
    height: 190,
  },
  imageRender: { width: '100%', height: 107 },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },

  // point
  wrapPoint: {
    flexDirection: 'row',
    padding: 6,
    backgroundColor: colors.yellowShading,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 30,
    width: 100,
  },
  point: { width: 24, height: 24, marginRight: 4 },
  textPoint: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
  subtitle: { fontSize: 12, textAlign: 'center', marginTop: 8 },
});
