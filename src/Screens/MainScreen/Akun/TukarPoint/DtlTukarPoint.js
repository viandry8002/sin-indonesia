import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HTML from 'react-native-render-html';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import colors from '../../../../Styles/Colors';
import { api, key } from '../../../../Variable';
import FormatMoney from '../../../MultipleComponent/FormatMoney';
import TextShimmer from '../../../MultipleComponent/Shimmer/TextShimmer';

const windowWidth = Dimensions.get('window').width;

const DtlTukarPoint = ({ navigation, route }) => {
  const [loading, setLoading] = useState(true);
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState([]);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
      setTimeout(() => {
        getToken();
      }, 100);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    var param;
    route.params.id ? (param = route.params.id) : (param = route.params);

    setTokens(token);
    Axios.get(`${api}/redeem/get/${param}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        res.data.success
          ? setData(res.data.data)
          : navigation.navigate('GoLogout', res.data?.message);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  };

  const postRead = idr => {
    let body = {
      redeem_id: idr,
    };
    Axios.post(`${api}/redeem/create`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? navigation.navigate('Success', {
            type: 3,
            title: 'Poin berhasil di tukar',
            subtitle: 'Menunggu approval dari admin',
            token: null,
          })
          : ToastAndroid.show(res.data.message, ToastAndroid.SHORT);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {loading === true ? (
        <View style={{ flex: 1 }}>
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={{ height: 272, width: windowWidth }}
          />

          <View style={styles.wrapInfo}>
            <TextShimmer type={'normal'} />
            <TextShimmer type={'normal'} />
          </View>
          {/* desc product */}
          <View style={styles.wrapDesc}>
            <TextShimmer type={'normal'} />
            <View style={{ height: 8 }} />
            <TextShimmer type={'large'} />
            <View style={{ height: 8 }} />
            <TextShimmer type={'large'} />
          </View>
        </View>
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image
            source={
              data.file
                ? { uri: data.file }
                : require('../../../../Assets/image/notfound.png')
            }
            style={{ width: windowWidth, height: 272 }}
            resizeMode={'contain'}
          />

          {/* product */}
          <View style={styles.wrapInfo}>
            <Text style={styles.title}>{data.title}</Text>
            <View style={styles.wrapPoint}>
              <Image
                source={require('../../../../Assets/icon/poin.png')}
                style={styles.point}
                resizeMode={'contain'}
              />
              <FormatMoney value={data.amount}
                style={styles.textPoint} unit={''} />
            </View>
          </View>

          {/* desc product */}
          <View style={styles.wrapDesc}>
            <Text style={{ color: colors.primary, fontWeight: 'bold' }}>
              Deskripsi
            </Text>
            <HTML
              source={{ html: data.description }}
              allowedStyles={['color', 'fontFamily']} baseStyle={{ color: colors.gray3 }} ignoredStyles={['color', 'fontFamily']}
            />
          </View>
        </ScrollView>
      )}

      {loading === true ? (
        <View style={styles.wrapFooter}>
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={styles.footerBtn2}
          />
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={styles.footerBtn2}
          />
        </View>
      ) : (
        <View style={styles.wrapFooter}>
          <TouchableOpacity
            style={styles.footerBtn1}
            onPress={() => navigation.navigate('Bantuan')}>
            <Text style={{ color: colors.primary }}>Hubungi CS</Text>
          </TouchableOpacity>
          <View style={{ width: 16 }} />
          <TouchableOpacity
            style={styles.footerBtn2}
            onPress={() => postRead(data.id)}>
            <Text style={{ color: colors.white }}>Tukarkan Poin</Text>
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

export default DtlTukarPoint;

const styles = StyleSheet.create({
  // slider
  dotSlider: { width: 6, height: 6, marginHorizontal: -8 },

  // product
  wrapInfo: {
    backgroundColor: colors.white,
    padding: 16,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: { color: colors.black, fontWeight: 'bold', width: 250 },
  wrapPoint: {
    flexDirection: 'row',
    padding: 6,
    backgroundColor: colors.yellowShading,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  point: { width: 24, height: 24, marginRight: 4 },
  textPoint: { fontSize: 12, color: colors.black, fontWeight: 'bold' },

  // desc product
  wrapDesc: { backgroundColor: colors.white, padding: 16, marginTop: 8, flex: 1 },

  // footer
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerBtn1: {
    flex: 1,
    // width: 168,
    height: 38,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: colors.primary,
  },
  footerBtn2: {
    flex: 1,
    backgroundColor: colors.primary,
    // width: 168,
    height: 38,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  point: { width: 18, height: 18, marginRight: 4 },
});
