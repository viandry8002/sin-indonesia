import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../../Styles/Colors';
import { api, key } from '../../../../Variable';
import FormatExpired from '../../../MultipleComponent/FormatExpired';
import FormatMoney from '../../../MultipleComponent/FormatMoney';
import Gradient from '../../../MultipleComponent/Gradient';

const windowWidth = Dimensions.get('window').width;

const Index = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  const [dataPoin, setDataPoin] = useState([]);
  const [dataRedem, setDataRedem] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    setRefreshing(true);
    setLoading(true);
    getToken();
    let timer = setInterval(() => {
      FormatExpired(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
        ? clearInterval(timer)
        : setdatess(moment(endDate).diff(moment(), 'seconds'));
    }, 1000);
    // });
    // return unsubscribe;
  }, [endDate]);

  const getData = token => {
    setTokens(token);
    token === null
      ? false
      : //point
      setTimeout(() => {
        Axios.get(`${api}/poins/amount`, {
          headers: {
            key: key,
            Authorization: 'Bearer ' + token,
          },
        })
          .then(res => {
            res.data.success
              ? (setDataPoin(res.data), setendDate(res.data.expired_at))
              : navigation.navigate('GoLogout', res.data?.message);
          })
          .catch(err => {
            console.log(err);
          });

        Axios.get(`${api}/redeem`, {
          headers: {
            key: key,
          },
        })
          .then(res => {
            // setData(res.data.data)
            setDataRedem(res.data.data);
            res.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.next_page_url);
            setLoading(false);
            setRefreshing(false);
          })
          .catch(err => {
            console.log(err);
            setLoading(false);
            setRefreshing(false);
          });
      }, 100);
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 2000);
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setDataRedem([...dataRedem, ...res.data.data]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderProduct = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapRender}
      onPress={() => navigation.navigate('DtlTukarPoint', item.id)}>
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <ImageBackground
          source={require('../../../../Assets/image/redem.png')}
          style={{
            width: 160,
            height: 36,
            justifyContent: 'center',
            paddingLeft: 28,
          }}
          resizeMode={'contain'}>
          <FormatMoney value={item.amount}
            unit={''}
            style={{ color: colors.white, fontWeight: 'bold' }} />
        </ImageBackground>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <View>
        <View style={{ height: 44 }}>
          <Gradient />
        </View>
        {/* elevation1 */}
        {loading === true ? (
          <View style={styles.wrapPoint}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 16,
              }}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.point}
              />
              <View style={{ flex: 1 }}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={{ height: 34, width: 94, borderRadius: 7 }}
                />
              </View>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={{ height: 34, width: 94, borderRadius: 7 }}
              />
            </View>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={styles.wrapExpired}
            />
          </View>
        ) : (
          <View style={styles.wrapPoint}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 16,
              }}>
              <Image
                source={require('../../../../Assets/icon/poin.png')}
                style={styles.point}
              />
              <View style={{ flex: 1 }}>
                <FormatMoney value={dataPoin.data}
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: colors.black,
                  }} unit={''} />
                <Text style={{ fontSize: 10, color: colors.gray3 }}>
                  Poin anda
                </Text>
              </View>
              <View>
                <Text style={styles.textExpired}>Berlaku sampai :</Text>
                <Text
                  style={[
                    styles.textExpired,
                    { fontWeight: 'bold', textAlign: 'right' },
                  ]}>
                  {moment(dataPoin.expired_at).format('DD/MM/YYYY')}
                </Text>
              </View>
            </View>
            <View style={styles.wrapExpired}>
              <Text style={{ fontSize: 12, color: colors.primary }}>
                Berakhir Dalam
              </Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <MaterialCommunityIcons
                  name="clock-time-three-outline"
                  size={16}
                  color={colors.primary}
                />
                <Text style={styles.timeExpired}>
                  {FormatExpired(moment(endDate).diff(moment(), 'seconds'))}
                </Text>
              </View>
            </View>
          </View>
        )}

        <FlatList
          data={dataRedem}
          renderItem={renderProduct}
          style={{ margin: 4, marginTop: 90 }}
          // style={{ paddingHorizontal: 8, marginTop: 55 }}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          ListFooterComponent={() =>
            hasScrolled === true ? (
              <View style={{ alignItems: 'center' }}>
                <Text>Load more ....</Text>
              </View>
            ) : (
              <View style={{ height: 130 }} />
            )
          }
          onScroll={() => (pass === false ? onScroll() : false)}
          onEndReached={() => (nextLink === null ? false : handleLoadMore())}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  // point
  wrapPoint: {
    marginHorizontal: 8,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    position: 'absolute',
    width: windowWidth - 16,
    marginTop: 16,
  },
  textExpired: { fontSize: 12, color: colors.black },
  wrapExpired: {
    backgroundColor: colors.blueShading,
    height: 32,
    borderRadius: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    width: '100%',
  },
  timeExpired: { fontSize: 11, color: colors.primary, marginLeft: 8 },
  point: { width: 24, height: 24, marginRight: 10, borderRadius: 24 },
  tukarPoint: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.primary,
    height: 34,
    width: 94,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 0.5,
    margin: 4,
    height: 190,
  },
  imageRender: { width: '100%', height: 107 },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },
});
