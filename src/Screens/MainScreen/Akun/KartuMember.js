import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Image,
  ImageBackground,
  PermissionsAndroid,
  SafeAreaView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
  Alert
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import RNFetchBlob from 'rn-fetch-blob';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import Gradient from '../../MultipleComponent/Gradient';
import SpinnerLoad from '../../MultipleComponent/SpinnerLoad';

const KartuMember = ({ navigation }) => {
  const [data, setData] = useState([]);
  const [linkCard, setLinkCard] = useState('');
  const [load, setload] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getToken();
    });
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('api_token');
        return getData(token);
      } catch (err) {
        console.log(err);
      }
    }
    return unsubscribe;
  }, []);

  const requestStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        actualDownload();
      } else {
        if (granted === 'never_ask_again') {
          actualDownload();
        } else {
          Alert.alert('Peringatan', 'Izinkan File dan media terlebih dahulu', [
            {
              text: 'ok',
              onPress: () => console.log('Tidak'),
            },
          ]);
          // console.log('External storage permission denied');
        }
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const getData = token => {
    setload(true);
    Axios.get(`${api}/user`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('asssss', res.data);
        res.data.success
          ? setData(res.data.data)
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });

    Axios.get(`${api}/user/card`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('asssss', res.data);
        setLinkCard(res.data.url);
        setload(false);
      })
      .catch(err => {
        console.log(err);
        setload(false);
      });
  };

  const actualDownload = () => {
    const { dirs } = RNFetchBlob.fs;
    RNFetchBlob.config({
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: `member.pdf`,
        path: `${dirs.DownloadDir}/member.pdf`,
      },
    })
      .fetch('GET', linkCard, {})
      .then(res => {
        // console.log('The file saved to ', res.path());
        ToastAndroid.show('Kartu Member berhasil di simpan', ToastAndroid.LONG);
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <SpinnerLoad loads={load} />
      <View>
        <View style={{ height: 44 }}>
          <Gradient />
        </View>
        <View style={styles.wrapMember}>
          <ImageBackground
            source={require('../../../Assets/image/card.png')}
            style={styles.backgroundMember}
            imageStyle={{ borderRadius: 15 }}
            resizeMode={'contain'}>
            <View style={{ marginTop: 47, marginLeft: 22 }}>
              <Image
                source={
                  data.file
                    ? { uri: data.file }
                    : require('../../../Assets/image/person1.png')
                }
                style={{ width: 56, height: 56, borderRadius: 100 }}
              />
              <Text style={styles.name} numberOfLines={1}>
                {data.name}
              </Text>
              <Text style={styles.registDate}>
                Terdaftar Sejak : {moment(data.is_verified_at).format('DD/MM/YYYY')}
              </Text>
              <Text style={styles.number} numberOfLines={1}>
                {data.member_id}
              </Text>
            </View>
          </ImageBackground>
        </View>

        <TouchableOpacity
          style={styles.TouchableStyle}
          onPress={() => requestStoragePermission()}>
          <AntDesign name="download" color={colors.white} size={20} />
          <Text style={{ color: colors.white, fontSize: 12, marginLeft: 10 }}>
            Download Kartu Member
          </Text>
        </TouchableOpacity>

        <View style={{ padding: 16, marginTop: 8 }}>
          <Text style={styles.title}>Keuntungan Member</Text>
          <View style={styles.wrapDesc}>
            <Image
              source={require('../../../Assets/icon/member.png')}
              style={styles.icon}
            />
            <Text style={styles.textDesc}>
              Pembelian produk dengan harga{' '}
              <Text style={{ fontWeight: 'bold' }}>Murah</Text>
            </Text>
          </View>
          <View style={styles.wrapDesc}>
            <Image
              source={require('../../../Assets/icon/member.png')}
              style={styles.icon}
            />
            <Text style={styles.textDesc}>
              target <Text style={{ fontWeight: 'bold' }}>point</Text> dapat
              ditukar dengan berbagai hadiah
            </Text>
          </View>
          <View style={styles.wrapDesc}>
            <Image
              source={require('../../../Assets/icon/member.png')}
              style={styles.icon}
            />
            <Text style={styles.textDesc}>
              <Text style={{ fontWeight: 'bold' }}>Doorpize</Text> tahunan yang di
              undi tiap tahun
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default KartuMember;

const styles = StyleSheet.create({
  // cart member
  wrapMember: { position: 'absolute', alignSelf: 'center' },
  backgroundMember: { width: 343, height: 209, elevation: 6 },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.white,
    marginTop: 8,
    marginRight: 22,
  },
  registDate: { fontSize: 10, color: colors.white },
  number: { fontSize: 28, color: colors.white, marginTop: 8, marginRight: 22 },
  // btn
  TouchableStyle: {
    backgroundColor: colors.primary,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginTop: 185,
    marginHorizontal: 16,
    flexDirection: 'row',
  },
  //  profit
  title: { fontSize: 16, color: colors.black, fontWeight: 'bold' },
  wrapDesc: { marginVertical: 16, flexDirection: 'row', alignItems: 'center' },
  icon: { width: 24, height: 24, marginRight: 4 },
  textDesc: { fontSize: 12, color: colors.black },
});
