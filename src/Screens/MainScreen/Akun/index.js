import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Alert,
  Dimensions,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatExpired from '../../MultipleComponent/FormatExpired';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import TextShimmer from '../../MultipleComponent/Shimmer/TextShimmer';

const windowWidth = Dimensions.get('window').width;

const Index = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [dataPoin, setDataPoin] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    setLoading(true);
    getToken();
    let timer = setInterval(() => {
      FormatExpired(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
        ? clearInterval(timer)
        : setdatess(moment(endDate).diff(moment(), 'seconds'));
    }, 1000);
    // });
    // return unsubscribe;
  }, [endDate]);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getToken();
      setRefreshing(false);
    }, 2000);
  };

  const getPoin = token => {
    Axios.get(`${api}/poins/amount`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        setDataPoin(res.data);
        setendDate(res.data.expired_at);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getData = token => {
    setTimeout(() => {
      Axios.get(`${api}/user`, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => {
          res.data.success
            ? (setData(res.data.data), getPoin(token))
            : navigation.navigate('GoLogout', res.data?.message);
          setLoading(false);
        })
        .catch(err => {
          console.log('errormya', err);
          setLoading(false);
        });
    }, 100);
  };

  const getAlert = () => {
    Axios.get(`${api}/configuration`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        onAlertLogout(res.data.data.deskripsi_logout);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onAlertLogout = msg => {
    Alert.alert('Peringatan', msg, [
      {
        text: 'Tidak',
        onPress: () => console.log('Tidak'),
      },
      {
        text: 'Ya',
        onPress: () => navigation.navigate('GoLogout', null),
      },
    ]);
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={{ height: 100 }}>
          <LinearGradient
            colors={['#3BA172', '#0B326D']}
            start={{ x: -1, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}>
            {loading === true ? (
              <View style={styles.wrapProfile}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={{ width: 80, height: 80, borderRadius: 100 }}
                />
                <View style={{ marginLeft: 16 }}>
                  <TextShimmer type={'normal'} />
                  <View style={{ height: 8 }} />
                  <TextShimmer type={'normal'} />
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    style={styles.member}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.wrapProfile}>
                <Image
                  source={
                    data?.file
                      ? { uri: data.file }
                      : require('../../../Assets/image/person1.png')
                  }
                  style={{ width: 80, height: 80, borderRadius: 100 }}
                />
                <View style={{ marginHorizontal: 16, flex: 1 }}>
                  <Text style={styles.name} numberOfLines={1}>
                    {data?.name}
                  </Text>
                  <Text style={styles.number}>{data?.member_id}</Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('KartuMember')}>
                    <Image
                      source={require('../../../Assets/image/member.png')}
                      style={styles.member}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </LinearGradient>
        </View>

        {/* elevation1 */}
        {loading === true ? (
          <View style={styles.wrapPoint}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.point}
              />
              <View style={{ flex: 1 }}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={[styles.tukarPoint, { borderWidth: 0 }]}
                />
              </View>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={[styles.tukarPoint, { borderWidth: 0 }]}
              />
            </View>
            <View style={{ height: 16 }} />
            <TextShimmer type={'large'} />
            <View style={{ height: 16 }} />
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={styles.wrapExpired}
            />
          </View>
        ) : (
          <View style={styles.wrapPoint}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                source={require('../../../Assets/icon/poin.png')}
                style={styles.point}
              />
              <View style={{ flex: 1 }}>
                <FormatMoney value={dataPoin.data}
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: colors.black,
                  }} unit={''} />
                <Text style={{ fontSize: 10, color: colors.gray3 }}>
                  Poin anda
                </Text>
              </View>
              <TouchableOpacity
                style={styles.tukarPoint}
                onPress={() => navigation.navigate('TukarPoint')}>
                <Text style={{ color: colors.primary }}>Tukar Poin</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.textExpired}>
              Berlaku sampai :{' '}
              <Text style={{ fontWeight: 'bold' }}>
                {moment(dataPoin.expired_at).format('DD/MM/YYYY')}
              </Text>
            </Text>
            <View style={styles.wrapExpired}>
              <Text style={{ fontSize: 12, color: colors.primary }}>
                Berakhir Dalam
              </Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <MaterialCommunityIcons
                  name="clock-time-three-outline"
                  size={16}
                  color={colors.primary}
                />
                <Text style={styles.timeExpired}>
                  {FormatExpired(moment(endDate).diff(moment(), 'seconds'))}
                </Text>
              </View>
            </View>
          </View>
        )}

        {/* elevation2 */}
        <View style={styles.wrapElevation2}>
          <TouchableOpacity
            style={styles.btnMenu}
            onPress={() => navigation.navigate('EditAkun', { type: 'user' })}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/pen.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Edit Akun</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnMenu}
            onPress={() => navigation.navigate('KartuMember')}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/card.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Kartu Member</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnMenu}
            onPress={() => navigation.navigate('Alamat', { from: 'profile' })}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/pin.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Alamat Saya</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnMenu}
            onPress={() => navigation.navigate('WishList')}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/favorite.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Wishlist</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnMenu}
            onPress={() => navigation.navigate('Bantuan')}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/chat.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Bantuan</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnMenu} onPress={() => getAlert()}>
            <View style={styles.wrapMenu}>
              <Image
                source={require('../../../Assets/icon/signOut.png')}
                style={styles.iconMenu}
              />
              <Text style={styles.titleMenu}>Sign Out</Text>
              <AntDesign name="right" size={16} color={colors.gray4} />
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  // profile
  wrapProfile: {
    flexDirection: 'row',
    marginHorizontal: 16,
    alignItems: 'center',
  },
  name: { fontSize: 18, color: colors.white, fontWeight: 'bold', width: 220 },
  number: { fontSize: 12, color: colors.white },
  member: { width: 160, height: 26, marginTop: 8, borderRadius: 7 },

  // point
  wrapPoint: {
    marginHorizontal: 16,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    position: 'absolute',
    width: windowWidth - 32,
    marginTop: 90,
  },
  textExpired: { fontSize: 12, color: colors.black, marginVertical: 16 },
  wrapExpired: {
    backgroundColor: colors.blueShading,
    height: 32,
    borderRadius: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    width: '100%',
  },
  timeExpired: { fontSize: 11, color: colors.primary, marginLeft: 8 },
  point: { width: 24, height: 24, marginRight: 10, borderRadius: 24 },
  tukarPoint: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.primary,
    height: 34,
    width: 94,
    justifyContent: 'center',
    alignItems: 'center',
  },

  // menu
  wrapElevation2: {
    marginHorizontal: 16,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    position: 'relative',
    width: windowWidth - 32,
    marginTop: 140,
    // marginBottom: 130,
    marginBottom: 30,
  },
  // wrapElevation2: { marginHorizontal: 16, backgroundColor: colors.white, elevation: 6, borderRadius: 7, padding: 12, position: 'relative', width: windowWidth - 32, marginTop: 110, marginBottom: 130 },
  btnMenu: { height: 40, marginVertical: 4 },
  wrapMenu: { flexDirection: 'row', alignItems: 'center' },
  iconMenu: { width: 24, height: 24, marginRight: 8 },
  titleMenu: { fontSize: 12, color: colors.black, flex: 1 },
});
