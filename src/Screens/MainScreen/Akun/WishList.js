import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatMoney from '../../MultipleComponent/FormatMoney';

const WishList = ({ navigation, route }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      setTimeout(() => {
        getToken();
      }, 1000);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/wishlist`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success
          ? (setData(res.data.data),
            res.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.next_page_url))
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 2000);
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const showToast = msg => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  };

  const addWhislist = idproduct => {
    let body = {
      product_id: idproduct,
    };

    Axios.post(`${api}/wishlist/do`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (getData(tokens), showToast(res.data.message))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapRender}
      onPress={() => navigation.navigate('DtlProduct', item.id)}>
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <FormatMoney value={item.price}
            style={styles.produkPrice} />
          <MaterialCommunityIcons
            name="heart"
            size={24}
            color={colors.red}
            onPress={() => addWhislist(item.id)}
          />
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <FlatList
        data={data}
        renderItem={renderItem}
        style={{ margin: 4 }}
        numColumns={2}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{ alignItems: 'center' }}>
              <Text>Load more ....</Text>
            </View>
          ) : (
            <View style={{ height: 130 }} />
          )
        }
        showsVerticalScrollIndicator={false}
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        ListEmptyComponent={() =>
          refreshing === false ? (
            <View style={{ flex: 1, alignItems: 'center', marginTop: 200 }}>
              <Image
                source={require('../../../Assets/image/no-data.png')}
                style={{ width: 200, height: 200 }}
                resizeMode={'contain'}
              />
              <Text style={styles.subtitle}>Data tidak ditemukan</Text>
            </View>
          ) : (
            false
          )
        }
      />
    </SafeAreaView>
  );
};

export default WishList;

const styles = StyleSheet.create({
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 0.5,
    margin: 4,
    height: 190,
  },
  imageRender: { width: '100%', height: 107 },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },
  subtitle: { fontSize: 12, textAlign: 'center', marginTop: 8 },
});
