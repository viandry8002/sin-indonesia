import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import moment from 'moment';

const windowHeight = Dimensions.get('window').height;

const Notifikasi = ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  const [chose, setChose] = useState(0);
  const [data, setData] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState(null);
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      setTimeout(() => {
        getToken();
      }, 100);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/notification`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        console.log('aaazzzzzzkkkk', res.data);
        // console.log(
        //   'aaazzzzzzkkkk',
        //   res.data.data.filter(x => x.id != null),
        // );
        res.data.success
          ? (setData(res.data.data.filter(x => x.id != null)),
            res.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.next_page_url))
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 2000);
  };

  const postRead = (idn, invoice, status) => {
    let body = {
      id: idn,
    };
    Axios.post(`${api}/notification/read`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        console.log('res', res.data);
        status?.status
          ? navigation.navigate('DtlPesanan', { invoice: invoice, type: status })
          : false;

        if (status?.type === 'new redeem') {
          navigation.navigate('DtlTukarPoint', status.id);
        } else if (status?.type === 'new product') {
          navigation.navigate('DtlProduct', status.id);
        } else {
          getData(tokens);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('reeeeeeee', res.data);
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data.filter(x => x.id != null)]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      style={[
        { padding: 16 },
        item.is_read === 0 ? { backgroundColor: colors.gray6 } : false,
      ]}
      onPress={() => postRead(item.id, item.invoice_id, item.detail)}>
      {(item.type === 'promotions' && item.detail?.type === 'new redeem') ||
        item.type === 'vouchers' ? (
        <View style={{ flexDirection: 'row' }}>
          <Image
            source={require('../../../Assets/icon/speaker.png')}
            style={styles.renderIcon}
          />
          <View style={styles.wrapContent}>
            <View style={styles.wrapTitle}>
              <Text style={styles.type}>{item.type}</Text>
              <Text style={{ fontSize: 12 }}>
                {moment(item.created_at).format('DD MMM YYYY')}
              </Text>
            </View>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={{ marginTop: 8, fontSize: 11, color: colors.gray3 }}>
              {item.description}
            </Text>
            <Image
              source={
                item.title_img
                  ? { uri: item.title_img }
                  : require('../../../Assets/image/notfound.png')
              }
              style={styles.imgPromosi}
              resizeMode="contain"
            />
          </View>
        </View>
      ) : (
        <View style={{ flexDirection: 'row' }}>
          <Image
            source={require('../../../Assets/icon/cart.png')}
            style={styles.renderIcon}
          />
          <View style={styles.wrapContent}>
            <View style={styles.wrapTitle}>
              <Text style={styles.type}>{item.type}</Text>
              <Text style={{ fontSize: 12 }}>
                {moment(item.created_at).format('DD MMM YYYY')}
              </Text>
            </View>
            <Text style={styles.title}>{item.title}</Text>
            {item.detail?.type === 'announcement' ? (
              false
            ) : (
              <View
                style={{
                  paddingHorizontal: 8,
                  marginTop: 8,
                  flexDirection: 'row',
                  backgroundColor: colors.gray6,
                  borderRadius: 7,
                  alignItems: 'center',
                }}>
                <Image
                  source={
                    item.title_img
                      ? { uri: item.title_img }
                      : require('../../../Assets/image/notfound.png')
                  }
                  style={styles.imgBelanja}
                  resizeMode="contain"
                />
                <Text style={{ fontSize: 12, color: colors.black, flex: 1 }}>
                  {item.title_item}
                </Text>
              </View>
            )}
          </View>
        </View>
      )}
    </TouchableOpacity>
  );

  // const choseKategori = (pilihan) => {
  //     console.log(filter(flashsale, { type: 'promotions' }));
  // }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <FlatList
        data={data}
        renderItem={renderItem}
        // horizontal
        // style={{ paddingHorizontal: 8 }}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={
          <View style={{ alignItems: 'center', marginTop: windowHeight / 4 }}>
            {/* <Text style={Font.regular12}>Gulir kebawah untuk menyegarkan halaman</Text> */}
            <Image source={require('../../../Assets/image/no-data.png')} style={{ width: 130, height: 130, resizeMode: 'contain' }} />
            <Text style={Font.regular12}>Data tidak ditemukan</Text>
          </View>
        }
        // numColumns={2}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{ alignItems: 'center' }}>
              <Text style={{ color: colors.black }}>Load more ....</Text>
            </View>
          ) : (
            <View style={{ height: 130 }} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
        refreshing={refreshing}
        onRefresh={onRefresh}
      />
    </SafeAreaView>
  );
};

export default Notifikasi;

const styles = StyleSheet.create({
  wrapHeader: {
    flexDirection: 'row',
    height: 54,
    width: '100%',
    backgroundColor: colors.white,
    alignItems: 'center',
    elevation: 6,
    paddingHorizontal: 8,
  },
  btnHeader: {
    borderRadius: 100,
    borderWidth: 1,
    borderColor: colors.gray6,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  btnHeaderAct: {
    borderRadius: 100,
    backgroundColor: colors.gray6,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  btnText: { fontSize: 12, color: colors.black },
  btnTextAct: { fontSize: 12, color: colors.primary },

  // render
  renderIcon: { width: 36, height: 36 },
  wrapContent: { marginLeft: 8, flex: 1 },
  wrapTitle: { flexDirection: 'row', justifyContent: 'space-between' },
  type: { color: colors.primary, fontSize: 12 },
  title: { marginTop: 8, color: colors.black, fontWeight: 'bold', fontSize: 12 },
  imgPromosi: { width: 299, height: 148, borderRadius: 15, marginTop: 8 },
  imgBelanja: {
    width: 42,
    height: 42,
    borderRadius: 7,
    marginVertical: 8,
    marginRight: 8,
  },
});
