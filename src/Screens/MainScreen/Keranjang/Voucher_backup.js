import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import colors from '../../../Styles/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import {api, key} from '../../../Variable';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import BtnDisable from '../../MultipleComponent/BtnDisable';
import {TextMask} from 'react-native-masked-text';

const Voucher = ({route, navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [data, setData] = useState([]);
  const [idChose, setIdChose] = useState(0);
  const [chose, setChose] = useState('');
  const [tokens, setTokens] = useState(null);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      setTimeout(() => {
        route.params.voucher === null
          ? false
          : (setIdChose(route.params.voucher.id),
            setChose(route.params.voucher));
        getToken();
      }, 100);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    let body = {id: route.params.carts};

    Axios.post(`${api}/voucher`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        console.log('aaa', res.data.data);
        res.data.success
          ? (setData(res.data.data), setTokens(token))
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 1000);
  };

  const renderItem = ({item}) => (
    <View style={{flex: 1}}>
      <TouchableOpacity
        style={{
          flex: 1,
          marginVertical: 4,
          flexDirection: 'row',
        }}
        onPress={() => {
          setChose(item),
            item.require_minimal ? setIdChose(item.id) : setIdChose(0);
        }}>
        {/* {item.require_minimal ? (
            <LinearGradient
              colors={['#3BA172', '#0B326D']}
              start={{x: -1, y: 0}}
              end={{x: 1, y: 0}}
              style={styles.wrapIcon}>
              <Image
                source={require('../../../Assets/icon/voucher.png')}
                style={{width: 26, height: 21}}
              />
            </LinearGradient>
          ) : (
            <View style={[styles.wrapIcon, {backgroundColor: colors.gray3}]}>
              <Image
                source={require('../../../Assets/icon/voucher.png')}
                style={{width: 26, height: 21}}
              />
            </View>
          )} */}
        {item.use ? (
          <LinearGradient
            colors={['#3BA172', '#0B326D']}
            start={{x: -1, y: 0}}
            end={{x: 1, y: 0}}
            style={styles.wrapIcon}>
            <Image
              source={require('../../../Assets/icon/voucher.png')}
              style={{width: 26, height: 21}}
            />
          </LinearGradient>
        ) : (
          <View style={[styles.wrapIcon, {backgroundColor: colors.gray3}]}>
            <Image
              source={require('../../../Assets/icon/voucher.png')}
              style={{width: 26, height: 21}}
            />
          </View>
        )}
        <View
          style={{
            flex: 1,
            backgroundColor: colors.white,
            height: 80,
            borderTopRightRadius: 7,
            borderBottomRightRadius: 7,
            flexDirection: 'row',
            alignItems: 'center',
            padding: 8,
          }}>
          <View style={{flex: 1}}>
            <Text style={[styles.title, {fontSize: 12}]}>{item.title}</Text>
            <View style={styles.rowCenter}>
              <Entypo name="dot-single" size={16} color={colors.gray4} />
              <Text style={styles.fontDesc}>
                Min belanja{' '}
                <TextMask
                  type={'money'}
                  options={{
                    precision: 0,
                    separator: '.',
                    delimiter: '.',
                    unit: 'Rp ',
                    suffixUnit: '',
                  }}
                  value={item.min_nominal}
                  numberOfLines={1}
                />
              </Text>
            </View>
            {item.type_diskon === 'nominal' ? (
              false
            ) : (
              <View style={styles.rowCenter}>
                <Entypo name="dot-single" size={16} color={colors.gray4} />
                <Text style={styles.fontDesc}>
                  Max. Diskon{' '}
                  <TextMask
                    type={'money'}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp ',
                      suffixUnit: '',
                    }}
                    value={item.max_diskon}
                    numberOfLines={1}
                  />
                </Text>
              </View>
            )}
            <View style={styles.rowCenter}>
              <AntDesign name="clockcircle" size={12} />
              <Text style={styles.fontDesc}>
                {' '}
                Berlaku Hingga {moment(item.end_date).format('DD MMM YYYY')}
              </Text>
            </View>
          </View>
          {item.require_minimal ? (
            idChose === item.id ? (
              <MaterialIcons
                name="check-box"
                size={20}
                color={colors.primary}
              />
            ) : (
              <MaterialIcons
                name="check-box-outline-blank"
                size={20}
                color={colors.primary}
              />
            )
          ) : (
            false
          )}
        </View>
      </TouchableOpacity>
      {item.require_minimal ? (
        false
      ) : (
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 4,
            alignItems: 'center',
          }}>
          <MaterialIcons name="info" size={18} color={colors.red} />
          <Text style={[styles.fontDesc, {color: colors.red, marginLeft: 4}]}>
            Min. belanja belum terpenuhi
          </Text>
        </View>
      )}
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* empty */}
      {/* <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 40,
          }}>
          <Image
            source={require('../../../Assets/icon/voucher_empty.png')}
            style={{width: 60, height: 48}}
          />
          <Text style={styles.fontSubTitle}>Anda tidak memiliki voucher</Text>
          <Text style={[styles.fontDesc, {textAlign: 'center', marginTop: 20}]}>
            Tidak ada voucher yang tersedia sekarang, kembali lagi nanti saat ada
            promo menarik
          </Text>
        </View> */}
      <View style={{flex: 1}}>
        <FlatList
          data={data}
          renderItem={renderItem}
          ListHeaderComponent={
            <View style={{marginBottom: 4, marginTop: 16}}>
              <Text style={styles.title}>Voucher Tersedia</Text>
            </View>
          }
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          style={{marginHorizontal: 8}}
        />
      </View>
      <View style={styles.wrapFooter}>
        {console.log('idChose', idChose)}
        {data.length > 0 && idChose > 0 ? (
          <BtnPrimary
            title={'Gunakan Voucher'}
            pressed={() =>
              navigation.navigate('Checkout', {
                carts: route.params.carts,
                payment: route.params.payment,
                voucher: chose,
              })
            }
          />
        ) : (
          <BtnDisable title={'Gunakan Voucher'} />
        )}
      </View>
    </SafeAreaView>
  );
};

export default Voucher;

const styles = StyleSheet.create({
  wrapIcon: {
    width: 72,
    height: 80,
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'center',
  },
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  title: {color: colors.black, fontWeight: '600'},
  fontSubTitle: {
    marginTop: 20,
    fontWeight: '500',
    fontSize: 12,
    color: colors.black,
  },
  fontDesc: {
    fontWeight: '400',
    fontSize: 12,
    color: colors.gray3,
  },
});
