import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';

const MetodePembayaran = ({ navigation, route }) => {
  // console.log('hhhhhh', route.params);
  const [refreshing, setRefreshing] = useState(false);
  const [data, setData] = useState([]);
  const [typeAkun, setTypeAkun] = useState('');

  const getToken = async () => {
    const typeAkun = await AsyncStorage.getItem('type_akun');
    getData(typeAkun);
    setTypeAkun(typeAkun);
  };

  useEffect(() => {
    setRefreshing(true);
    setTimeout(() => {
      getToken();
    }, 100);
  }, []);

  const getData = (typeAkunn) => {
    // Axios.get(typeAkunn === 'agent' ? route.params?.shipment_method === 'ekspedition' ? `${api}/payment?is_agent=1` : `${api}/payment?is_courier_store=1&is_agent=1` : route.params?.shipment_method === 'ekspedition' ? `${api}/payment` : `${api}/payment?is_courier_store=1`, {
    Axios.get(typeAkunn === 'agent' ? route.params?.shipment_method === 'ekspedition' ? `${api}/payment?is_agent=1` : `${api}/payment?is_courier_store=1&is_agent=1` : `${api}/payment`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('reszz', res.data.data);
        setData(res.data.data);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err.response.data);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(typeAkun);
    }, 1000);
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapItem}
      onPress={() =>
        route.params.role === 'agent' ?
          navigation.navigate('Step3', {
            // carts: route.params.carts,
            // voucher: route.params.voucher,
            step1: route.params.step1,
            step2: route.params.step2,
            address: route.params.address,
            courier: route.params.courier,
            payment: item,
            total_all_buy: route.params.total_all_buy
          })
          :
          navigation.navigate('Checkout', {
            carts: route.params.carts,
            payment: item,
            voucher: route.params.voucher,
          })
      }>
      <Image
        source={{ uri: item.file }}
        style={styles.imageItem}
        resizeMode={'contain'}
      />
      <Text style={{ fontSize: 12, color: colors.black }}>{item.title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white, padding: 8 }}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {data.map(dt => (
          <FlatList
            data={dt.items}
            renderItem={renderItem}
            ListHeaderComponent={
              <View style={styles.headerItem}>
                <Text style={styles.headerTitle}>{dt.title}</Text>
              </View>
            }
            showsVerticalScrollIndicator={false}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default MetodePembayaran;

const styles = StyleSheet.create({
  headerItem: {
    padding: 8,
    backgroundColor: colors.blueShading,
    borderRadius: 8,
  },
  headerTitle: { fontSize: 12, color: colors.primary, fontWeight: 'bold' },

  wrapItem: { padding: 8, flexDirection: 'row', marginVertical: 8 },
  imageItem: { width: 48, height: 16, marginRight: 32 },
});
