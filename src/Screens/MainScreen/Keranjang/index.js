import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  ToastAndroid,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import BtnDisable from '../../MultipleComponent/BtnDisable';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import FormatMoney from '../../MultipleComponent/FormatMoney';

const Index = ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  const [chose, setChose] = useState(0);
  const [dataCore1, setDataCore1] = useState([]);
  const [dataCore2, setDataCore2] = useState([]);
  const [data, setData] = useState([]);

  const [carts, setCarts] = useState([]);
  const [checklistAll, setChecklistAll] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      setTimeout(() => {
        getToken();
      }, 100);
    });
    return unsubscribe;
  }, []);

  const showToast = msg => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  };

  const getData = token => {
    setChecklistAll(false);
    setCarts([]);
    setTokens(token);
    Axios.get(`${api}/cart`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success
          ? (setDataCore1({
            price: 0,
            poin: 0,
          }),
            setDataCore2(res.data),
            setData(res.data.data))
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 1000);
  };

  const updateCart = (idc, qty) => {
    const body = {
      id: idc,
      qty: qty,
    };
    Axios.post(`${api}/cart/update`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? getData(tokens)
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const spesificCart = paramCart => {
    let body = { id: paramCart };

    Axios.post(`${api}/cart`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (res.data.price === dataCore2.price
            ? setChecklistAll(true)
            : (setChecklistAll(false),
              setDataCore1({
                price: 0,
                poin: 0,
              })),
            setDataCore1(res.data))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const checkAll = () => {
    setChecklistAll(!checklistAll);
    checklistAll
      ? (setCarts([]),
        setDataCore1({
          price: 0,
          poin: 0,
        }))
      : (setCarts(data.map(dt => dt.id)), spesificCart(data.map(dt => dt.id)));
  };

  const check = idp => {
    setCarts(carts => [...carts, idp]);
    spesificCart([...carts, idp]);
  };

  const uncheck = idp => {
    setCarts(carts.filter(x => x != idp));
    spesificCart(carts.filter(x => x != idp));
  };

  const deleteCart = () => {
    let body = { id: carts };

    Axios.post(`${api}/cart/delete`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (getData(tokens), setChecklistAll(false), setCarts([]))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderProduct = ({ item }) => (
    <View style={styles.wrapRender}>
      {carts.includes(item.id) ? (
        <MaterialIcons
          name="check-box"
          size={20}
          color={colors.primary}
          onPress={() => {
            uncheck(item.id);
          }}
        />
      ) : (
        <MaterialIcons
          name="check-box-outline-blank"
          size={20}
          color={colors.primary}
          onPress={() => {
            check(item.id);
          }}
        />
      )}
      <Image
        source={
          item?.product.file
            ? { uri: item?.product.file }
            : require('../../../Assets/image/notfound.png')
        }
        style={styles.image}
        resizeMode={'contain'}
      />
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('DtlProduct', item?.product.id)}>
          <Text style={styles.title}>{item?.product.title}</Text>
          {/* <Text style={styles.subtitle}>Rp10.000</Text> */}
          <FormatMoney value={item.price}
            style={styles.subtitle} />
        </TouchableOpacity>
        {/* <View style={styles.wrapCategory}>
                    <Text style={styles.TextCategory}>Kecil</Text>
                </View> */}
        <View style={styles.wrapCount}>
          <TouchableOpacity
            onPress={() => updateCart(item.id, Number(item.qty) - 1)}>
            <Image
              source={require('../../../Assets/icon/min.png')}
              style={styles.imageCont}
            />
          </TouchableOpacity>
          <Text style={{ fontWeight: 'bold', color: colors.black }}>
            {item.qty}
            {/* {item?.product.qty} */}
          </Text>
          <TouchableOpacity
            onPress={() =>
              item?.product.qty > item.qty
                ? updateCart(item.id, Number(item.qty) + 1)
                : showToast('Mohon maaf stok terbatas')
            }
            disabled={item?.product.qty + 1 > item.qty ? false : true}>
            {/* <TouchableOpacity onPress={() => console.log('asd',item.qty++)}> */}
            <Image
              source={require('../../../Assets/icon/add.png')}
              style={styles.imageCont}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {/* header */}
      {data.length > 0 ? (
        <View style={styles.wrapHeader}>
          <TouchableOpacity style={styles.wrapPilih} onPress={() => checkAll()}>
            <MaterialIcons
              name={checklistAll ? 'check-box' : 'check-box-outline-blank'}
              size={20}
              color={colors.primary}
              style={{ marginRight: 8 }}
            />
            <Text style={styles.TextHeader}>Pilih Semua</Text>
          </TouchableOpacity>
          {carts.length > 0 ? (
            <Text style={styles.TextHeader} onPress={() => deleteCart()}>
              Hapus
            </Text>
          ) : (
            false
          )}
        </View>
      ) : (
        false
      )}

      <View style={{ flex: 1 }}>
        <FlatList
          style={{ marginTop: 4 }}
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={renderProduct}
          ListFooterComponent={<View style={{ height: 130 }} />}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>

      {/* footer */}
      <View style={styles.wrapFooter}>
        <View>
          <FormatMoney value={dataCore1.price}
            style={styles.price} />
          <View style={styles.wrapPilih}>
            <Image
              source={require('../../../Assets/icon/poin.png')}
              style={styles.poin}
            />
            <Text style={styles.TextHeader}>
              +{dataCore1.poin} Poin Didapat
            </Text>
          </View>
        </View>

        <View style={{ height: 40, width: 137 }}>
          {carts.length > 0 ? (
            <BtnPrimary
              title={'Checkout'}
              pressed={() =>
                navigation.navigate('Checkout', {
                  carts: carts,
                  payment: null,
                  voucher: null,
                  type_akun: 'user',
                  role: null
                })
              }
            />
          ) : (
            <BtnDisable title={'Checkout'} />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  // header
  wrapHeader: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  wrapPilih: { flexDirection: 'row', alignItems: 'center' },
  TextHeader: { fontSize: 12, color: colors.black },

  // render
  wrapRender: {
    flex: 1,
    marginHorizontal: 8,
    marginVertical: 4,
    backgroundColor: colors.white,
    borderRadius: 15,
    flexDirection: 'row',
    padding: 8,
  },
  image: { width: 72, height: 72, borderRadius: 15, marginHorizontal: 8 },
  title: { fontSize: 12, color: colors.black },
  subtitle: { fontWeight: 'bold', color: colors.primary, marginVertical: 8 },
  wrapCategory: {
    flex: 1,
    backgroundColor: colors.blueShading,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 100,
    width: 70,
    alignItems: 'center',
  },
  TextCategory: { color: colors.primary, fontSize: 12 },
  wrapCount: {
    marginTop: 8,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageCont: { width: 32, height: 32, marginHorizontal: 16 },

  // footer
  wrapFooter: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    elevation: 6,
  },
  price: {
    fontSize: 18,
    color: colors.primary,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  poin: { width: 24, height: 24, marginRight: 4 },
});
