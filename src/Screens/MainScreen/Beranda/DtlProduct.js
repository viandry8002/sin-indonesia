import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  Share,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HTML from 'react-native-render-html';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import TextShimmer from '../../MultipleComponent/Shimmer/TextShimmer';

const windowWidth = Dimensions.get('window').width;

const DtlProduct = ({ route, navigation }) => {
  const [loading, setLoading] = useState(true);
  const [tokens, setTokens] = useState('');
  const [data, setData] = useState([]);
  const [image, setImage] = useState([]);
  const [form, setForm] = useState({
    product_id: '',
    qty: 1,
    price: '',
    poin: '',
  });
  const [disable, setDisable] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getDtlProduct(token);
  };

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      getToken();
    }, 100);
  }, []);

  const useInitialURL = () => {
    const [url, setUrl] = useState(null);
    const [processing, setProcessing] = useState(true);

    useMount(() => {
      const getUrlAsync = async () => {
        // Get the deep link used to open the app
        const initialUrl = await Linking.getInitialURL();

        // The setTimeout is just for testing purpose
        setTimeout(() => {
          setUrl(initialUrl);
          setProcessing(false);
        }, 1000);
      };

      getUrlAsync();
    });

    return { url, processing };
  };

  const getDtlProduct = token => {
    var param;
    route.params.id
      ? (param = route.params.id)
      : // (param = base64.decode(route.params.id))
      // // JSON.parse(
      // //   CryptoJS.AES.decrypt(route.params.id + '', key).toString(
      // //     CryptoJS.enc.Utf8,
      // //   ),
      // )
      (param = route.params);

    setTokens(token);
    Axios.get(`${api}/product/get/${param}`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        console.log('ggg', res.data.data);
        setData(res.data.data);
        setImage(res.data.data.files);
        setForm({
          ...form,
          ['product_id']: res.data.data.id,
          ['poin']: res.data.data.poin,
          ['price']: res.data.data.price,
        });
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  };

  const images = image.map((datas, index) => {
    return [{ uri: datas }];
  });

  const showToast = msg => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  };

  const addCart = () => {
    let body = {
      product_id: form.product_id,
      qty: form.qty,
    };

    Axios.post(`${api}/cart/add`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success === true
          ? (showToast('Berhasil menambahkan ke keranjang'),
            navigation.navigate('KeranjangSS'))
          : showToast(res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const addWhislist = () => {
    let body = {
      product_id: form.product_id,
    };

    Axios.post(`${api}/wishlist/do`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (getDtlProduct(tokens), showToast(res.data.message))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        WebGLQuery;
        console.log(err);
      });
  };

  // const onShare = async () => {
  //   try {
  //     // var ciphertext = CryptoJS.AES.encrypt(
  //     //   JSON.stringify(form.product_id + ''),
  //     //   key,
  //     // ).toString();
  //     var ciphertext = base64.encode(form.product_id + '');
  //s     // console.log('gg', ciphertext)
  //     // const result =
  //     await Share.share({
  //       message:
  //         'dapatkan ' +
  //         data.title +
  //         ' hanya dengan Rp.' +
  //         data.price +
  //         ' sekarang juga di SIN Indonesia https://www.sinindonesia.com/DtlProduct/' +
  //         ciphertext,
  //     });
  //   } catch (error) {
  //     alert(error.message);
  //   }
  // };

  const onShare = async () => {
    try {
      // var ciphertext = CryptoJS.AES.encrypt(
      //   JSON.stringify(form.product_id + ''),
      //   key,
      // ).toString();
      // var ciphertext = base64.encode(form.product_id + '');
      // console.log('gg', ciphertext)
      // const result =

      // await Share.share({
      //   message:
      //     'dapatkan ' +
      //     data.title +
      //     ' hanya dengan Rp.' +
      //     data.price +
      //     ' sekarang juga di SIN Indonesia https://www.sinindonesia.com/DtlProduct/' +
      //     form.product_id,
      // });

      await Share.share({
        message:
          'dapatkan ' +
          data.title +
          ' hanya dengan Rp.' +
          data.price +
          ' sekarang juga di SIN Indonesia https://sinindonesia.co.id/DtlProduct/' +
          form.product_id,
      });
    } catch (error) {
      alert(error.message);
    }
  };

  const Flashsale = () => {
    return (
      <View style={styles.wrapFlashSale}>
        <View style={styles.wrapRow}>
          <Ionicons name="flash" size={16} color={colors.white} />
          <Text style={styles.textFlashSale}>Flash Sale</Text>
        </View>
        <View style={styles.wrapRow}>
          <FormatMoney value={data.price}
            style={[styles.price, { color: colors.white, marginRight: 8 }]} />
          <FormatMoney value={data.price_from}
            style={{
              fontSize: 10,
              color: colors.whiteShading,
              textDecorationLine: 'line-through',
            }} />
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {loading === true ? (
        <View style={{ flex: 1 }}>
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={{ height: 272, width: windowWidth }}
          />

          <View style={{ backgroundColor: colors.white, padding: 16 }}>
            {data?.flashsale ? Flashsale() : false}
            <View
              style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
              {/* <Text style={styles.title}>{data.title}</Text> */}
              <View style={{ flex: 1 }}>
                <TextShimmer type={'large'} />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={[
                    styles.imageCont,
                    { marginHorizontal: 0, marginRight: 8 },
                  ]}
                />
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={[styles.imageCont, { marginHorizontal: 0 }]}
                />
              </View>
            </View>
            {data?.flashsale ? false : <TextShimmer type={'normal'} />}
            <View style={styles.wrapCount}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.imageCont}
              />
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.imageCont}
              />
            </View>
          </View>
          {/* desc product */}
          <View style={styles.wrapDesc}>
            <TextShimmer type={'normal'} />
            <View style={{ height: 8 }} />
            <TextShimmer type={'large'} />
            <View style={{ height: 8 }} />
            <TextShimmer type={'large'} />
          </View>
        </View>
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ height: 14 }} />
          {images.length === 0 ? (
            <Image
              source={require('../../../Assets/image/notfound.png')}
              style={{ width: windowWidth, height: 272 }}
              resizeMode={'contain'}
            />
          ) : (
            <View style={{ height: 272 }}>
              <SwiperFlatList
                paginationBoxVerticalPadding={10}
                autoplay
                autoplayDelay={2}
                autoplayLoop
                // index={2}
                showPagination
                data={image}
                renderItem={({ item }) => (
                  <View
                    style={{ flex: 1, width: windowWidth, alignItems: 'center' }}>
                    <Image
                      source={{ uri: item }}
                      style={{ width: windowWidth, height: 272 }}
                    />
                  </View>
                )}
                paginationStyleItem={{ width: 6, height: 6, marginHorizontal: 4 }}
                paginationStyle={{ bottom: 0 }}
                paginationStyleItemActive={{ backgroundColor: colors.primary }}
                paginationStyleItemInactive={{
                  backgroundColor: colors.whiteShading,
                }}
              />
            </View>
            //  <SliderBox
            //   paginationBoxVerticalPadding={10}
            //   autoplay
            //   circleLoop
            //   images={images}
            //   dotColor={colors.primary}
            //   inactiveDotColor={colors.whiteShading}
            //   sliderBoxHeight={272}
            //   dotStyle={styles.dotSlider}
            // />
          )}

          {/* product */}
          <View style={{ backgroundColor: colors.white, padding: 16 }}>
            {data?.flashsale ? Flashsale() : false}
            <View
              style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
              <Text style={styles.title}>{data.title}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Ionicons
                  name="share-social-outline"
                  onPress={() => onShare()}
                  size={24}
                  color={colors.black}
                  style={{ marginRight: 8 }}
                />
                {data.whislist ? (
                  <MaterialCommunityIcons
                    name="heart"
                    size={24}
                    color={colors.red}
                    onPress={() => addWhislist()}
                  />
                ) : (
                  <MaterialCommunityIcons
                    name="heart-outline"
                    size={24}
                    color={colors.black}
                    onPress={
                      tokens === null
                        ? () => navigation.navigate('DirectLogin')
                        : () => addWhislist()
                    }
                  />
                )}
              </View>
            </View>
            {data?.flashsale ? (
              false
            ) : (
              <FormatMoney value={data.price}
                style={styles.price} />
            )}
            <View style={styles.wrapCount}>
              <TouchableOpacity
                onPress={() => {
                  form.qty - 1 > 0
                    ? setForm({
                      ...form,
                      ['qty']: form.qty - 1,
                      ['poin']: Number(form.poin) - Number(data.poin),
                    })
                    : setDisable(true);
                }}
                disabled={disable}>
                <Image
                  source={require('../../../Assets/icon/min.png')}
                  style={styles.imageCont}
                />
              </TouchableOpacity>
              <Text style={{ fontWeight: 'bold', color: colors.black }}>
                {form.qty}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  // console.log('lllll', data.qty);
                  data.qty > form.qty
                    ? (setForm({
                      ...form,
                      ['qty']: form.qty + 1,
                      ['poin']: Number(form.poin) + Number(data.poin),
                    }),
                      setDisable(false))
                    : showToast('Mohon maaf stok terbatas');
                }}
                disabled={data.qty + 1 > form.qty ? false : true}>
                <Image
                  source={require('../../../Assets/icon/add.png')}
                  style={styles.imageCont}
                />
              </TouchableOpacity>
            </View>
          </View>

          {/* desc product */}
          <View style={styles.wrapDesc}>
            <Text style={{ color: colors.primary, fontWeight: 'bold' }}>
              Detail Produk
            </Text>
            <HTML
              source={{ html: data.description }}
              allowedStyles={['color', 'fontFamily']}
              baseStyle={{ color: colors.gray3 }}
              ignoredStyles={['color', 'fontFamily']}
              tagsStyles={{
                p: { color: colors.black, },
                ul: { color: colors.black, },
                ol: { color: colors.black, }
              }}
            />
          </View>
        </ScrollView>
      )}

      {loading === true ? (
        <View style={styles.wrapFooter}>
          <View>
            <TextShimmer type={'normal'} />
            <View style={{ height: 8 }} />
            <TextShimmer type={'normal'} />
          </View>

          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={styles.footerBtn}
          />
        </View>
      ) : (
        <View style={styles.wrapFooter}>
          <View>
            {/* <Text style={styles.footerCost}>{form.price * form.qty}</Text> */}
            <FormatMoney value={form.price * form.qty}
              style={styles.footerCost} />
            <View style={styles.wrapPoint}>
              <Image
                source={require('../../../Assets/icon/poin.png')}
                style={styles.point}
              />
              <Text style={{ fontSize: 12, color: colors.black }}>
                +{form.poin} Poin Didapat
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={styles.footerBtn}
            onPress={() =>
              tokens ? addCart() : navigation.navigate('DirectLogin')
            }>
            <Text style={{ color: colors.white, marginRight: 8 }}>
              Tambah Keranjang{' '}
            </Text>
            <Image
              source={require('../../../Assets/icon/bagCircle.png')}
              style={{ width: 26, height: 26 }}
            />
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

export default DtlProduct;

const styles = StyleSheet.create({
  // slider
  dotSlider: { width: 6, height: 6, marginHorizontal: -8 },

  // product
  wrapFlashSale: {
    backgroundColor: colors.red,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
    borderRadius: 7,
    marginBottom: 8,
  },
  wrapRow: { flexDirection: 'row', alignItems: 'center' },
  textFlashSale: { color: colors.white, fontSize: 12, marginLeft: 8 },
  title: { color: colors.black, fontWeight: 'bold' },
  price: { fontSize: 18, color: colors.primary, fontWeight: 'bold' },
  wrapCount: {
    marginTop: 8,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageCont: { width: 32, height: 32, marginHorizontal: 16, borderRadius: 100 },

  // desc product
  wrapDesc: { backgroundColor: colors.white, padding: 16, marginTop: 8, flex: 1 },

  // footer
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerCost: { fontWeight: 'bold', color: colors.primary, fontSize: 16 },
  footerBtn: {
    flex: 1,
    marginLeft: 16,
    backgroundColor: colors.primary,
    // width: 220,
    height: 42,
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapPoint: { flexDirection: 'row', alignItems: 'center', marginTop: 4 },
  point: { width: 18, height: 18, marginRight: 4 },
});
