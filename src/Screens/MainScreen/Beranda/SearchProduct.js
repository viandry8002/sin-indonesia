import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View
} from 'react-native';
import { Badge } from 'react-native-elements';
import { Input } from 'react-native-elements/dist/input/Input';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';

const SearchProduct = ({ navigation }) => {
  const [focused, setFocused] = useState(true);
  const [inputSearch, setInputSearch] = useState('');
  const [data, setData] = useState(0);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getToken();
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    Axios.get(`${api}/cart/count`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        setData(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onSubmit = () => {
    setInputSearch('');
    navigation.push('ListProduct', {
      idc: '',
      type: '',
      search: inputSearch,
    });
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      {/* header */}
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}>
        <View
          style={{
            width: '100%',
            height: 54,
            justifyContent: 'space-around',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 20,
            marginTop: 40,
          }}>
          <AntDesign
            name="arrowleft"
            size={24}
            color={colors.white}
            onPress={() => navigation.goBack()}
          />
          <Input
            label={''}
            placeholder={'Cari apa yang kamu mau...'}
            leftIcon={
              <Ionicons name="search" size={18} color={colors.primary} />
            }
            style={{ fontSize: 12, color: colors.black }}
            inputContainerStyle={{
              borderBottomWidth: 0,
              borderWidth: 0,
              height: 34,
              marginVertical: 12,
              backgroundColor: colors.white,
              top: 12,
              borderRadius: 100,
              paddingHorizontal: 16,
              alignSelf: 'center',
              marginHorizontal: 16,
            }}
            containerStyle={{ marginHorizontal: 0 }}
            value={inputSearch}
            onChangeText={value => setInputSearch(value)}
            autoFocus={focused}
            returnKeyType={'search'}
            onSubmitEditing={() => onSubmit()}
          />
          <View>
            <MaterialIcons
              name="shopping-bag"
              size={24}
              color={colors.white}
              onPress={() => navigation.navigate('KeranjangSS')}
            />
            {data === 0 ? (
              false
            ) : (
              <Badge
                value={data}
                containerStyle={{ position: 'absolute', top: -5, left: 14 }}
                badgeStyle={{
                  backgroundColor: colors.red,
                  borderColor: colors.white,
                  borderWidth: 1,
                }}
              />
            )}
          </View>
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default SearchProduct;

const styles = StyleSheet.create({
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 1,
    margin: 8,
    height: 190,
  },
  imageRender: { width: '100%', height: 107 },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },
});
