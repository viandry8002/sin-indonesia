import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Progress from 'react-native-progress';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import FormatTime from '../../MultipleComponent/FormatTime';
import TextShimmer from '../../MultipleComponent/Shimmer/TextShimmer';
import SinggleBanner from '../../MultipleComponent/SinggleBanner';

const windowWidth = Dimensions.get('window').width;

const Index = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [tokens, setTokens] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  const [sliders, Sliders] = useState([]);
  const [banner, setBanner] = useState('');
  const [point, setPoint] = useState(0);
  const [category, setCategory] = useState([]);
  const [flashSale, setFlashSale] = useState([]);
  const [newProduct, setNewProduct] = useState([]);
  const [bestSeller, setBestSeller] = useState([]);
  const [recommended, setRecommended] = useState([]);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };
  // useEffect(() => {
  //   setLoading(true);
  //   getToken();
  //   let timer = setInterval(() => {
  //     FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
  //       ? clearInterval(timer)
  //       : setdatess(moment(endDate).diff(moment(), 'seconds'));
  //   }, 1000);
  // }, [endDate]);

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    setLoading(true);
    getToken();
    let timer = setInterval(() => {
      FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
        ? clearInterval(timer)
        : setdatess(moment(endDate).diff(moment(), 'seconds'));
    }, 1000);
    // });
    // return unsubscribe;
  }, [endDate]);

  const getData = token => {
    //point
    setTimeout(() => {
      token === null
        ? false
        : Axios.get(`${api}/poins/amount`, {
          headers: {
            key: key,
            Authorization: 'Bearer ' + token,
          },
        })
          .then(res => {
            res.data.success
              ? (setTokens(token), setPoint(res.data.data))
              : navigation.navigate('GoLogout', res.data?.message);
          })
          .catch(err => {
            console.log(err);
          });

      //sliders
      Axios.get(`${api}/sliders`, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          Sliders(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });

      //categories
      Axios.get(`${api}/product/categories`, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          setCategory(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });

      //baner member
      Axios.get(`${api}/configuration`, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          // console.log('popopop', res.dat.data.banner_user);
          setBanner(res.data.data.banner_user)
        })
        .catch(err => {
          console.log(err);
        });

      // flash sale
      Axios.get(`${api}/flashsale`, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          res.data.data === null
            ? (setFlashSale(null), setendDate(''))
            : (setendDate(res.data.data ? res.data.data.end_date : null),
              setFlashSale(res.data.data ? res.data.data : null));
        })
        .catch(err => {
          console.log(err);
        });

      // new product
      const tbody1 = {
        multi_img: 0,
        type: 'new',
        per_page: 5,
      };
      Axios.post(`${api}/product`, tbody1, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          // console.log('result', res.data.data)
          setNewProduct(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });

      // best seller
      const tbody2 = {
        multi_img: 0,
        type: 'best_seller ',
        per_page: 5,
      };
      Axios.post(`${api}/product`, tbody2, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          setBestSeller(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });

      // product recomend
      const tbody3 = {
        multi_img: 0,
        type: 'recommended',
        per_page: 5,
      };
      Axios.post(`${api}/product`, tbody3, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          setRecommended(res.data.data);
          setRefreshing(false);
          setLoading(false);
        })
        .catch(err => {
          console.log(err);
          setRefreshing(false);
          setLoading(false);
        });
    }, 100);
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 2000);
  };

  const images = sliders.map((datas, index) => {
    return [{ uri: datas.file }];
  });

  const renderKategori = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapKategori}
      onPress={() =>
        navigation.navigate('ListProduct', { idc: item.id, type: '' })
      }>
      <View style={styles.wrapImageCategory}>
        <Image
          source={{ uri: item.file }}
          style={styles.imageKategoti}
          resizeMode={'contain'}
        />
      </View>
      <Text style={styles.textKategori}>{item.title}</Text>
    </TouchableOpacity>
  );

  const renderFlashSale = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapRender}
      onPress={() => navigation.navigate('DtlProduct', item.id)}>
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <FormatMoney value={item.price}
          style={styles.produkPrice} />
        <FormatMoney value={item.price_from}
          style={{
            fontSize: 10,
            textDecorationLine: 'line-through',
            color: colors.gray3,
          }} />
        {/* <Progress.Bar progress={0.3} width={90} color={colors.red} borderColor='transparent' unfilledColor={colors.redShading} /> */}
        <Progress.Bar
          progress={100 - (item.qty - item.sold_flahsale) / 100}
          width={90}
          color={colors.red}
          borderColor="transparent"
          unfilledColor={colors.redShading}
        />
        <Text style={{ fontSize: 10, color: colors.gray3 }}>
          {item.qty - item.sold_flahsale} Tersisa
        </Text>
      </View>
    </TouchableOpacity>
  );

  const renderProduk = ({ item }) => (
    <TouchableOpacity
      style={[styles.wrapRender, { height: 178 }]}
      onPress={() => navigation.navigate('DtlProduct', item.id)}>
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <FormatMoney value={item.price}
          style={styles.produkPrice} />
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      {loading === true ? (
        <View style={{ flex: 1 }}>
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={{ height: 180, width: '100%' }}
          />
          <View style={styles.wrapPoint}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={styles.point}
            />
            <View style={{ flex: 1 }}>
              <TextShimmer type={'normal'} />
            </View>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={[styles.tukarPoint, { borderWidth: 0 }]}
            />
          </View>

          {/* shimmer kategori */}
          <View style={{ marginTop: 69 }}>
            <View style={{ flex: 1, marginLeft: 16, marginBottom: 16 }}>
              <TextShimmer type={'normal'} />
            </View>
            <View style={styles.sWrapKategori1}>
              <View style={styles.sWrapKategori2}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.sKategori}
                />
                <TextShimmer type={'small'} />
              </View>

              <View style={styles.sWrapKategori2}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.sKategori}
                />
                <TextShimmer type={'small'} />
              </View>

              <View style={styles.sWrapKategori2}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.sKategori}
                />
                <TextShimmer type={'small'} />
              </View>

              <View style={styles.sWrapKategori2}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.sKategori}
                />
                <TextShimmer type={'small'} />
              </View>

              <View style={styles.sWrapKategori2}>
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.sKategori}
                />
                <TextShimmer type={'small'} />
              </View>
            </View>
          </View>

          {/* shimer member */}
          <View style={{ marginTop: 16 }}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                width: windowWidth - 32,
                height: 126,
                borderRadius: 16,
                alignSelf: 'center',
              }}
            />
          </View>

          <View style={{ marginTop: 30 }}>
            <View style={styles.wrapFlashSaleHeader}>
              <TextShimmer type={'normal'} />
              <TextShimmer type={'normal'} />
            </View>

            <View style={styles.sWrapKategori1}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.wrapRender}
              />

              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.wrapRender}
              />

              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={styles.wrapRender}
              />
            </View>
          </View>
        </View>
      ) : (
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View style={{ height: 180 }}>
            <SwiperFlatList
              paginationBoxVerticalPadding={10}
              autoplay
              autoplayDelay={2}
              autoplayLoop
              // index={2}
              showPagination
              data={sliders}
              renderItem={({ item }) => (
                <View
                  style={{ flex: 1, width: windowWidth, alignItems: 'center' }}>
                  <Image
                    source={{ uri: item.file }}
                    style={{ width: windowWidth, height: 180 }}
                  />
                </View>
              )}
              paginationStyleItem={{ width: 6, height: 6, marginHorizontal: 4 }}
              paginationStyle={{ bottom: 0 }}
              paginationStyleItemActive={{ backgroundColor: colors.white }}
              paginationStyleItemInactive={{
                backgroundColor: colors.whiteShading,
              }}
            />
          </View>
          {/* <SliderBox
            paginationBoxVerticalPadding={10}
            autoplay
            circleLoop
            images={images}
            dotColor={colors.white}
            inactiveDotColor={colors.whiteShading}
            sliderBoxHeight={180}
            dotStyle={styles.dotSlider}
          /> */}
          {/* Points */}
          {tokens === null ? (
            <View style={styles.wrapPoint}>
              <Ionicons name="person" size={24} color={colors.primary} />
              <View style={{ flex: 1, marginLeft: 10 }}>
                <Text style={{ fontSize: 12, color: colors.gray3 }}>
                  Mau berbelanja? Ayo{' '}
                  <Text
                    style={{ color: colors.primary }}
                    onPress={() => navigation.navigate('Login')}>
                    Login
                  </Text>{' '}
                  atau{' '}
                  <Text
                    style={{ color: colors.primary }}
                    onPress={() => navigation.navigate('Register')}>
                    Daftar Sebagai Member
                  </Text>
                </Text>
              </View>
            </View>
          ) : (
            <View style={styles.wrapPoint}>
              <Image
                source={require('../../../Assets/icon/poin.png')}
                style={styles.point}
              />
              <View style={{ flex: 1 }}>
                {/* <Text style={{ fontSize: 18, fontWeight: 'bold', color: colors.black }}>{point}</Text> */}
                <FormatMoney value={point}
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: colors.black,
                  }} unit={''} />
                <Text style={{ fontSize: 10, color: colors.gray3 }}>
                  Poin anda
                </Text>
              </View>
              <TouchableOpacity
                style={styles.tukarPoint}
                onPress={() => navigation.navigate('TukarPoint')}>
                <Text style={{ color: colors.primary }}>Tukar Poin</Text>
              </TouchableOpacity>
            </View>
          )}
          {/* Points ofline */}

          {/* Kategori */}
          <View style={{ marginTop: 69 }}>
            <Text style={[styles.title, { marginLeft: 16 }]}>Kategori</Text>
            <FlatList
              data={category}
              renderItem={renderKategori}
              horizontal
              style={{ marginTop: 8, paddingHorizontal: 8 }}
              ListFooterComponent={<View style={{ margin: 8 }} />}
              showsHorizontalScrollIndicator={false}
            />
          </View>

          {/* banner member */}
          {/* <SinggleBanner pressed={() => navigation.navigate('Step1')} image={require('../../../Assets/image/banner-agen.png')} /> */}
          <SinggleBanner pressed={() => navigation.navigate('Step1')} image={{ uri: 'https://sinindonesia.co.id/backend/' + banner }} />

          {flashSale === null ? (
            false
          ) : (
            <View style={{ marginTop: 30 }}>
              <LinearGradient
                colors={['#3BA172', '#0B326D']}
                start={{ x: -1, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={{ flex: 1 }}>
                <View style={[styles.wrapFlashSaleHeader, { marginTop: 16 }]}>
                  <View style={styles.wrapLainnya}>
                    <Text style={[styles.title, { color: colors.white }]}>
                      Flash Sale
                    </Text>
                    <View style={styles.time}>
                      <MaterialCommunityIcons
                        name="clock-time-three-outline"
                        size={16}
                        color={colors.white}
                      />
                      <Text style={{ color: colors.white, marginLeft: 8 }}>
                        {FormatTime(moment(endDate).diff(moment(), 'seconds'))}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapLainnya}>
                    <Text
                      style={[styles.lihatLainnya, { color: colors.white }]}
                      onPress={() => navigation.navigate('ListFlashSale')}>
                      Lihat Lainnya
                    </Text>
                    <Image
                      source={require('../../../Assets/icon/right.png')}
                      style={styles.iconRight}
                    />
                  </View>
                </View>

                <FlatList
                  data={flashSale.items}
                  renderItem={renderFlashSale}
                  horizontal
                  style={{ marginTop: 8, paddingHorizontal: 8, marginBottom: 8 }}
                  ListFooterComponent={<View style={{ margin: 8 }} />}
                  showsHorizontalScrollIndicator={false}
                />
              </LinearGradient>
            </View>
          )}

          {/* produk terbaru */}
          <View style={styles.wrapContent}>
            <View style={styles.wrapFlashSaleHeader}>
              <Text style={styles.title}>Produk Terbaru</Text>
              <View style={styles.wrapLainnya}>
                <Text
                  style={styles.lihatLainnya}
                  onPress={() =>
                    navigation.navigate('ListProduct', { idc: '', type: 'new' })
                  }>
                  Lihat Lainnya
                </Text>
                <Image
                  source={require('../../../Assets/icon/right.png')}
                  style={styles.iconRight2}
                />
              </View>
            </View>

            <FlatList
              data={newProduct}
              renderItem={renderProduk}
              horizontal
              style={{ marginTop: 8, paddingHorizontal: 8 }}
              ListFooterComponent={<View style={{ margin: 8 }} />}
              showsHorizontalScrollIndicator={false}
            />
          </View>

          {/* produk terlaris */}
          <View style={styles.wrapContent}>
            <View style={styles.wrapFlashSaleHeader}>
              <Text style={styles.title}>Produk Terlaris</Text>
              <View style={styles.wrapLainnya}>
                <Text
                  style={styles.lihatLainnya}
                  onPress={() =>
                    navigation.navigate('ListProduct', {
                      idc: '',
                      type: 'best_seller',
                    })
                  }>
                  Lihat Lainnya
                </Text>
                <Image
                  source={require('../../../Assets/icon/right.png')}
                  style={styles.iconRight2}
                />
              </View>
            </View>

            <FlatList
              data={bestSeller}
              renderItem={renderProduk}
              horizontal
              style={{ marginTop: 8, paddingHorizontal: 8 }}
              ListFooterComponent={<View style={{ margin: 8 }} />}
              showsHorizontalScrollIndicator={false}
            />
          </View>

          {/* produk rekomendasi */}
          <View style={[styles.wrapContent, { marginBottom: 75 }]}>
            <View style={styles.wrapFlashSaleHeader}>
              <Text style={styles.title}>Produk Rekomendasi</Text>
              <View style={styles.wrapLainnya}>
                <Text
                  style={styles.lihatLainnya}
                  onPress={() =>
                    navigation.navigate('ListProduct', {
                      idc: '',
                      type: 'recommended',
                    })
                  }>
                  Lihat Lainnya
                </Text>
                <Image
                  source={require('../../../Assets/icon/right.png')}
                  style={styles.iconRight2}
                />
              </View>
            </View>

            <FlatList
              data={recommended}
              renderItem={renderProduk}
              horizontal
              style={{ marginTop: 8, paddingHorizontal: 8 }}
              ListFooterComponent={<View style={{ margin: 8 }} />}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  // slider
  dotSlider: { width: 6, height: 6, marginHorizontal: -8 },
  // points
  wrapPoint: {
    marginHorizontal: 16,
    height: 61,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    flexDirection: 'row',
    padding: 12,
    alignItems: 'center',
    position: 'absolute',
    width: windowWidth - 32,
    marginTop: 175,
  },
  point: { width: 24, height: 24, marginRight: 10, borderRadius: 24 },
  tukarPoint: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.primary,
    height: 34,
    width: 94,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // kategori
  wrapKategori: { marginHorizontal: 8 },
  wrapImageCategory: {
    width: 56,
    height: 56,
    borderRadius: 56,
    backgroundColor: colors.blueShading,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageKategoti: { width: 36, height: 36 },
  textKategori: {
    color: colors.black,
    marginTop: 4,
    fontSize: 10,
    textAlign: 'center',
  },
  //Flash sale
  wrapFlashSaleHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 16,
  },
  time: {
    marginLeft: 8,
    backgroundColor: colors.red,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 4,
    paddingHorizontal: 12,
    borderRadius: 100,
    justifyContent: 'space-around',
  },
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    width: 142,
    height: 206,
    margin: 8,
  },
  imageRender: {
    width: '100%',
    height: 107,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7,
  },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },

  // all style
  wrapContent: { marginTop: 16 },
  title: { color: colors.primary, fontWeight: 'bold' },
  wrapLainnya: { flexDirection: 'row', alignItems: 'center' },
  iconRight: { width: 18, height: 18, marginLeft: 4 },
  iconRight2: { width: 18, height: 18, marginLeft: 4, tintColor: colors.primary },
  lihatLainnya: { fontSize: 12, color: colors.primary },

  //shimmer
  sWrapKategori1: {
    marginHorizontal: 16,
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  sWrapKategori2: { width: 62, alignItems: 'center' },
  sKategori: { borderRadius: 56, width: 56, height: 56, marginBottom: 4 },
  sTitleKategori: { borderRadius: 100, width: 44, marginTop: 4 },
});
