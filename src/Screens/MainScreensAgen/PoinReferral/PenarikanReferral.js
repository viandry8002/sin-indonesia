import React, { useRef } from 'react';
import {
    Dimensions,
    FlatList,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { Input } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { Modalize } from 'react-native-modalize';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import Font from '../../../Styles/Font';
import BtnOutline from '../../MultipleComponent/BtnOutline';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import InputComp from '../../MultipleComponent/InputComp';

const windowWidth = Dimensions.get('window').width;

const PenarikanReferral = ({ navigation }) => {
    const modalRekening = useRef(null);

    const onOpen = modal => {
        modal.current?.open();
    };

    const onClose = modal => {
        modal.current?.close();
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            {/* <ScrollView> */}

            <View style={{ height: 136, width: '100%' }}>
                <LinearGradient
                    colors={['#3BA172', '#0B326D']}
                    start={{ x: -1, y: 0 }}
                    end={{ x: 1, y: 0 }} style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row', marginHorizontal: 16, marginTop: 16, alignItems: 'center' }}>
                        <Image source={require('../../../Assets/icon/poinReferral.png')} style={{ width: 24, height: 24, marginRight: 4 }} />
                        <Text style={[Font.regular12, { color: colors.white }]}>
                            Poin Referral
                        </Text>
                    </View>
                    <FormatMoney value={100000000} style={[Font.bold26, { color: colors.white, marginLeft: 16 }]} />
                </LinearGradient>
            </View>

            <View style={styles.wrapPoint}>
                {/* <View > */}
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={Font.medium14}>Nominal Tarik</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={require('../../../Assets/icon/poinReferral.png')} style={{ width: 16, height: 16, marginRight: 4 }} />
                        <Text style={Font.regular12}>1 Poin = 1 Rupiah</Text>
                    </View>
                </View>
                <Input
                    // onPressIn={press}
                    label={false}
                    // placeholder={placeholder}
                    style={{ fontSize: 12 }}
                    inputContainerStyle={styles.containInput}
                    containerStyle={styles.inputContainer}
                    // value={'Rp'}
                    // onChangeText={onChange}
                    // secureTextEntry={secure}
                    leftIcon={<Text style={[Font.semiBold14, { paddingHorizontal: 8 }]}>Rp</Text>}
                    // rightIcon={<Text>Rp</Text>}
                    // maxLength={mlength}
                    showSoftInputOnFocus={false}
                // keyboardType={tkeyboard}
                />
                <InputComp
                    text={'Pilih Rekening'}
                    placeholder={'Pilih Rekening'}
                    // value={}
                    press={() => onOpen(modalRekening)}
                    inputfocus={false}
                    ricon={
                        <Ionicons
                            name="chevron-down"
                            size={18}
                            color={colors.black}
                        //   onPress={() => openModal('Provinsi')}
                        />
                    }

                />
                {/* <View style={{ height: 16 }} /> */}
                {/* <View style={{ flex: 1, marginTop: 16 }}> */}
                {/* <BtnPrimary title={'Tarik Saldo'} pressed={() => navigation.navigate('SuccessPoin')} /> */}
                {/* </View> */}
                {/* </View> */}
            </View>
            {/* </ScrollView> */}

            <Modalize ref={modalRekening} handlePosition={'inside'} modalHeight={400} modalStyle={{ borderTopRightRadius: 16, borderTopLeftRadius: 16 }}>
                <View style={{ padding: 16, flex: 1 }}>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={Font.semiBold16}>Pilih Rekening</Text>
                        <MaterialCommunityIcons
                            name="close"
                            size={24}
                            color={colors.black}
                            onPress={() => onClose(modalRekening)}
                        />
                    </View>

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={[{}, {}, {}, {}, {}]}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 8, marginHorizontal: 8, marginBottom: 8, flex: 1, borderRadius: 8, backgroundColor: colors.white, borderWidth: 1, borderColor: colors.gray6 }}>
                                <View style={{ flex: 0.5 }}>
                                    <View style={{ width: 40, height: 40, backgroundColor: colors.gray6, borderRadius: 8, alignItems: 'center', justifyContent: 'center' }}>
                                        <Image source={require('../../../Assets/image/bca.png')} style={{ width: 32, height: 10 }} />
                                    </View>
                                </View>
                                <View style={{ width: 16 }} />
                                <View style={{ flex: 3 }}>
                                    <Text style={Font.semiBold14}>Wanteknologi</Text>
                                    <Text style={Font.regular12}>1234567890</Text>
                                </View>
                                <View style={{ width: 8 }} />
                                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
                                    <Image
                                        source={require('../../../Assets/icon/gripHorizontal.png')}
                                        style={{ width: 24, height: 24 }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                            </TouchableOpacity>
                        )}
                        ListEmptyComponent={
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Text style={Font.regular12}>{'tunggu sebentar..'}</Text>
                            </View>
                        }
                        ListFooterComponent={<View style={{ marginTop: 8 }}><BtnOutline title={'Tambah Rekening Baru'} icon={<MaterialCommunityIcons
                            name="plus"
                            size={24}
                            color={colors.primary}

                        />} /></View>}
                    />
                </View>
            </Modalize>
        </SafeAreaView >
    )
}

export default PenarikanReferral

const styles = StyleSheet.create({
    wrapPoint: {
        // flex: 1,
        marginHorizontal: 8,
        backgroundColor: colors.white,
        elevation: 2,
        borderRadius: 8,
        padding: 16,
        width: windowWidth - 16,
        marginTop: -46,
        marginBottom: 16
    },
    //=========== input nominal
    containInput: { borderBottomWidth: 0 },
    labelInput: {
        fontWeight: 'bold',
        color: colors.black,
        fontSize: 12,
        marginTop: 16,
    },
    inputContainer: {
        marginTop: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        paddingHorizontal: 0,
        height: 48,
    },
    // ========= modalize
    wrapDetailOrder: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
    },
})