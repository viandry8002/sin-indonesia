import React, { useEffect, useState } from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import colors from '../../../Styles/Colors'
import Font from '../../../Styles/Font'
import PoinReferralElevate from '../../MultipleComponent/PoinReferralElevate'
import FormatMoney from '../../MultipleComponent/FormatMoney'
import { getPoinReferal, key } from '../../../Variable'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import HTML from 'react-native-render-html';

const PoinReferral = ({ navigation }) => {
    const [referal, setReferal] = useState([]);
    const [UserReferral, setUserReferral] = useState([]);

    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('api_token');
            return getData(token);
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        getToken();
    }, []);


    const getData = token => {
        //referal
        Axios.get(getPoinReferal, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                console.log('popopo', res.data.data.referrals);
                setReferal(res.data.data);
                setUserReferral(res.data.data.referrals)
            })
            .catch(err => {
                console.log(err);
            });
    };

    const textTotalPoin = (text, value) => {
        return (
            <View style={{ flexDirection: 'row', padding: 4, backgroundColor: colors.primaryShading, borderRadius: 100, marginTop: 4 }}>
                <Image source={require('../../../Assets/icon/poinBelanja.png')} style={{ width: 16, height: 16 }} />
                <Text style={[Font.regular11]}> {text} </Text>
                <FormatMoney value={value} unit={''} style={[Font.semiBold12]} />
            </View>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Image source={require('../../../Assets/image/referral.png')} style={{ width: '100%', height: 212 }} />
                <Text style={[Font.semiBold14, { textAlign: 'center', marginTop: 16, marginBottom: 16 }]}>{referal.description_referral}</Text>
                {/* <PoinReferralElevate btnTukar={() => navigation.navigate('PenarikanReferral')} /> */}
                <PoinReferralElevate elevation dataReferal={referal} accumulate btnHistory={() => navigation.navigate('RiwayatReferral')} />
                <View style={{ marginHorizontal: 16 }}>
                    <View style={{ height: 16 }} />
                    <View style={[styles.wrapCircle]}>
                        <Image source={{ uri: referal.banner_referral }} style={{ width: '100%', height: 130, resizeMode: 'contain' }} />
                        <View style={{
                            padding: 16, borderWidth: 1, borderColor: colors.primary, borderRadius: 100, flexDirection: 'row', justifyContent: 'space-between',
                            flex: 1, backgroundColor: colors.primaryShading, marginVertical: 8
                        }}>
                            <Text style={Font.regular14}>Total Belanja mandiri</Text>
                            <Text style={Font.semiBold14}><FormatMoney value={Number(referal.total_order_product)} unit={''} /> Produk</Text>
                        </View>
                        <View style={{
                            padding: 16, borderWidth: 1, borderColor: colors.primary, borderRadius: 100, flexDirection: 'row', justifyContent: 'space-between',
                            flex: 1, backgroundColor: colors.primaryShading, marginVertical: 8
                        }}>
                            <Text style={Font.regular14}>Total Belanja kalkulasi referral</Text>
                            <Text style={Font.semiBold14}><FormatMoney value={Number(referal.total_order_all)} unit={''} /> Produk</Text>
                        </View>
                        <HTML
                            source={{ html: referal.description_referral_stokist }}
                            tagsStyles={{
                                p: { color: colors.black, },
                                ul: { color: colors.black, },
                                ol: { color: colors.black, }
                            }}
                        />
                    </View>
                    <Text style={[Font.semiBold16, { marginVertical: 16 }]}>Syarat dan Ketentuan Keagenan</Text>
                    <View style={[styles.wrapCircle]}>
                        <HTML
                            source={{ html: referal.term_condition_referral }}
                            // source={{ html: `<head><style>p {color:red;}</style></head>${referal.term_condition_referral}` }}
                            // source={{ html: `<p>${referal.term_condition_referral}</p>` }}
                            tagsStyles={{
                                p: { color: colors.black, },
                                ul: { color: colors.black, },
                                ol: { color: colors.black, }
                            }}
                        />
                    </View>
                    <Text style={[Font.semiBold16, { marginVertical: 16 }]}>Agen Yang Sudah Bergabung</Text>
                    {
                        UserReferral.map((data, i) => (
                            <TouchableOpacity style={[styles.wrapCircle, { flexDirection: 'row', marginBottom: 8, alignItems: 'center', padding: 8 }]} key={i}
                                onPress={() => navigation.navigate('HBelanjaGetAgen', { user_id: data.id })}>
                                <Image source={{ uri: data.file }} style={{ width: 56, height: 56 }} />
                                <View style={{ marginLeft: 8 }}>
                                    <Text style={[Font.semiBold14]}>{data.name}</Text>
                                    <View style={{ flexDirection: 'row', padding: 4, backgroundColor: colors.primaryShading, borderRadius: 100, marginTop: 4 }}>
                                        <Image source={require('../../../Assets/icon/Bag_fill.png')} style={{ width: 16, height: 16, tintColor: colors.primary }} />
                                        <Text style={[Font.regular11]}>Belanja </Text>
                                        <FormatMoney value={data.total_order_product} unit={''} style={[Font.semiBold12]} />
                                        <Text style={[Font.regular11]}> Produk </Text>
                                    </View>
                                    {textTotalPoin('Total Poin Belanja saat ini', data.total_point)}
                                    {textTotalPoin('Total Poin Belanja Akumulasi', data.total_akumulasi_order)}
                                    {textTotalPoin('Total Poin Referral saat ini', data.total_point_referral)}
                                    {textTotalPoin('Total Poin Referral Akumulasi', data.total_akumulasi_point)}
                                </View>
                            </TouchableOpacity>
                        ))
                    }
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default PoinReferral

const styles = StyleSheet.create({
    wrapCircle: { padding: 16, borderWidth: 1, borderColor: colors.grayShading, flex: 1, borderRadius: 8 }
})