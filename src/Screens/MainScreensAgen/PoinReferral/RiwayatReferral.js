import { FlatList, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React, { useState, useEffect } from 'react'
import colors from '../../../Styles/Colors'
import BtnPrimary from '../../MultipleComponent/BtnPrimary'
import BtnOutline from '../../MultipleComponent/BtnOutline'
import FormatMoney from '../../MultipleComponent/FormatMoney'
import Font from '../../../Styles/Font'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getHistoryReferal, key, getAgentPoinHistory } from '../../../Variable'
import Axios from 'axios';

const listData = [
    // {
    //     title1: 'Penarikan Poin Referral',
    //     title2: 'Ke BCA',
    //     date: '8 Agustus 2023, 17:00',
    //     nominal: 30000,
    //     status: 'Pending',
    //     type: 'penarikan'
    // },
    // {
    //     title1: 'Penarikan Poin Referral',
    //     title2: 'Ke BCA',
    //     date: '8 Agustus 2023, 17:00',
    //     nominal: 30000,
    //     status: 'Berhasil',
    //     type: 'penarikan'
    // },
    // {
    //     title1: 'Pemasukan Poin Referral',
    //     title2: 'Dari Aditiya Putra',
    //     date: '8 Agustus 2023, 17:00',
    //     nominal: 30000,
    //     status: 'Berhasil',
    //     type: 'pemasukan'
    // },
]

const RiwayatReferral = ({ route, type }) => {
    const [choseBtn, setchoseBtn] = useState('Semua')
    const [data, setData] = useState([])
    const [tokens, setTokens] = useState(null)
    // scrolled
    const [nextLink, setNextLink] = useState('');
    const [hasScrolled, sethasScrolled] = useState(false);
    const [pass, setPass] = useState(false);

    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('api_token');
            setTokens(token)
            return getData(token, '');
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        getToken();
    }, []);

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            getToken();
            setRefreshing(false);
        }, 1000);
    };

    const getData = (token, status) => {
        //penjualan
        type === 'orderPoin' ?
            Axios.get(getAgentPoinHistory + status, {
                headers: {
                    key: key,
                    Authorization: 'Bearer ' + token,
                },
            })
                .then(res => {
                    // setData(res.data);
                    res.data.status
                        ? (setData(res.data.data),
                            res.data.next_page_url === null
                                ? (sethasScrolled(false), setPass(true))
                                : setNextLink(res.data.next_page_url))
                        : false;
                })
                .catch(err => {
                    console.log(err);
                })
            :
            // referal
            Axios.get(getHistoryReferal + status, {
                headers: {
                    key: key,
                    Authorization: 'Bearer ' + token,
                },
            })
                .then(res => {
                    // setData(res.data);
                    res.data.status
                        ? (setData(res.data.data),
                            res.data.next_page_url === null
                                ? (sethasScrolled(false), setPass(true))
                                : setNextLink(res.data.next_page_url))
                        : false;
                })
                .catch(err => {
                    console.log(err);
                });
    };

    // pass scrolled
    const onScroll = () => {
        sethasScrolled(true);
    };

    const handleLoadMore = () => {
        if (!hasScrolled) {
            return null;
        }

        Axios.get(`${nextLink}`, {
            headers: {
                key: key,
            },
        })
            .then(res => {
                sethasScrolled(!hasScrolled),
                    setData([...data, ...res.data.data]),
                    setNextLink(res.data.next_page_url);

                res.data.next_page_url === null
                    ? (sethasScrolled(!hasScrolled), setPass(true))
                    : false;
            })
            .catch(err => {
                console.log(err);
            });
    };

    const BtnTop = (title, pressed) =>
        choseBtn === title ? (<BtnPrimary title={title} pressed={pressed} />) : (<BtnOutline title={title} pressed={pressed} defaultBtn={true} />)

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <View style={{ padding: 8, flexDirection: 'row', elevation: 2, backgroundColor: colors.white }}>
                <View style={{ flex: 1 }}>
                    {
                        BtnTop('Semua', () => (setchoseBtn('Semua'), getData(tokens, '')))
                    }
                </View>
                <View style={{ width: 8 }} />
                <View style={{ flex: 1 }}>
                    {
                        BtnTop('Pemasukan', () => (setchoseBtn('Pemasukan'), getData(tokens, '?status=in')))
                    }
                </View>
                <View style={{ width: 8 }} />
                <View style={{ flex: 1 }}>
                    {
                        BtnTop('Pengeluaran', () => (setchoseBtn('Pengeluaran'), getData(tokens, '?status=out')))
                    }
                </View>
            </View>
            <FlatList
                data={data}
                renderItem={({ item, i }) => (
                    <View style={{ flexDirection: 'row', padding: 8, marginHorizontal: 8, marginTop: 8, flex: 1, borderRadius: 8, elevation: 2, backgroundColor: colors.white }}>
                        <View style={{ flex: 0.5 }}>
                            <Image source={item.status === 'out' ? require('../../../Assets/icon/penarikan.png') : require('../../../Assets/icon/pemasukan.png')} style={{ width: 40, height: 40 }} />
                        </View>
                        <View style={{ width: 8 }} />
                        <View style={{ flex: 3 }}>
                            <Text style={Font.semiBold14}>{item.status === 'out' ? 'Penukaran' : 'Pemasukan'} Poin</Text>
                            <Text style={Font.regular12}>{item.key}</Text>
                            <Text style={Font.regular12}>{item.created}</Text>
                        </View>
                        <View style={{ width: 8 }} />
                        <View style={{ flex: 1.2 }}>
                            <View style={styles.wrapPoint}>
                                <Image
                                    source={require('../../../Assets/icon/poinBelanja.png')}
                                    style={styles.point}
                                    resizeMode={'contain'}
                                />
                                <FormatMoney value={item.amount} style={[Font.regular12, { flex: 1 }]} unit={item.type === 'penarikan' ? '-' : '+'} />
                            </View>
                            {
                                item.active === '0' ?
                                    <Text style={[Font.regular12, { textAlign: 'right', color: colors.red }]}>Pending</Text>
                                    :
                                    <Text style={[Font.regular12, { textAlign: 'right', color: colors.success }]}>Berhasil</Text>
                            }
                        </View>
                    </View>
                )}
                keyExtractor={(item, i) => i}
                // ListFooterComponent={<View style={{ height: 32 }} />}
                ListFooterComponent={() =>
                    hasScrolled === true ? (
                        <View style={{ alignItems: 'center' }}>
                            <Text>Load more ....</Text>
                        </View>
                    ) : (
                        <View style={{ height: 32 }} />
                    )
                }
                showsVerticalScrollIndicator={false}
                onScroll={() => (pass === false ? onScroll() : false)}
                onEndReached={() => (nextLink === null ? false : handleLoadMore())}
                ListEmptyComponent={<View style={{ flex: 1, alignItems: 'center', marginTop: 130 }}>
                    <Image
                        source={require('../../../Assets/image/no-data.png')}
                        style={{ width: 130, height: 130 }}
                        resizeMode={'contain'}
                    />
                    <Text style={[styles.subtitle, { color: colors.black }]}>
                        Data tidak ditemukan
                    </Text>
                </View>}
            />

        </SafeAreaView>
    )
}

export default RiwayatReferral

const styles = StyleSheet.create({
    wrapPoint: {
        flex: 1,
        flexDirection: 'row',
        padding: 6,
        backgroundColor: colors.yellowShading,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    point: { width: 24, height: 24, marginRight: 4 },
})