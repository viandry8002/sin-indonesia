import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
    FlatList,
    Image,
    RefreshControl,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from 'react-native';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import Font from '../../../Styles/Font';

const windowHeight = Dimensions.get('window').height;

const HBelanjaGetAgen = ({ navigation, route }) => {
    const [refreshing, setRefreshing] = useState(false);
    const [chose, setchose] = useState([]);
    const [tokens, setTokens] = useState(null);
    const [data, setData] = useState([]);
    // scrolled
    const [nextLink, setNextLink] = useState('');
    const [hasScrolled, sethasScrolled] = useState(false);
    const [pass, setPass] = useState(false);
    const [typeAkun, setTypeAkun] = useState('');

    const getToken = async () => {
        const token = await AsyncStorage.getItem('api_token');
        const type = await AsyncStorage.getItem('type_akun');
        changeStatus(token);
        setTokens(token);
        setTypeAkun(type);
    };

    useEffect(() => {
        // const unsubscribe = navigation.addListener('focus', () => {
        // setRefreshing(true);
        // setTimeout(() => {
        getToken();
        //   setRefreshing(false);
        // }, 1000);
        // });
        // return unsubscribe;
    }, []);

    const changeStatus = (auth) => {
        // console.log('rararara', chose);
        Axios.get(`${api}/order?status=paid&user_id=${route.params.user_id}`, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + (auth ? auth : tokens),
            },
        })
            .then(res => {
                setData(res.data.data);
                res.data.next_page_url === null
                    ? (sethasScrolled(false), setPass(true))
                    : setNextLink(res.data.next_page_url);
            })
            .catch(err => {
                console.log(err.response.data);
            });
        setRefreshing(false);
    };

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            changeStatus(tokens);
        }, 2000);
    };

    // pass scrolled
    const onScroll = () => {
        sethasScrolled(true);
    };

    const handleLoadMore = () => {
        if (!hasScrolled) {
            return null;
        }

        Axios.get(`${nextLink}`, {
            headers: {
                key: key,
            },
        })
            .then(res => {
                // console.log(res);
                sethasScrolled(!hasScrolled),
                    setData([...data, ...res.data.data]),
                    setNextLink(res.data.next_page_url);

                res.data.next_page_url === null
                    ? (sethasScrolled(!hasScrolled), setPass(true))
                    : false;
            })
            .catch(err => {
                console.log(err);
            });
    };

    const renderItem = ({ item }) => (
        <TouchableOpacity
            style={styles.wrapRenderItem}
            onPress={
                () =>
                    navigation.navigate('DtlHBelanjaGetAgen', {
                        user_id: route.params.user_id,
                        invoice: item.invoice_id,
                        type: item.detail.title,
                    })
            }>
            <View style={styles.wrapItemHeader}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={require('../../../Assets/icon/Bag_fill.png')}
                        style={styles.bagIcon}
                    />
                    <View>
                        <Text style={styles.textBelanja}>Belanja</Text>
                        <Text style={{ fontSize: 11, color: colors.gray3 }}>
                            {moment(item.created_at).format('DD MMM YYYY')}
                        </Text>
                    </View>
                </View>
                <Text style={styles.fontPrimary}>{item.detail.title}</Text>
            </View>
            <View style={styles.horizontal} />
            <View style={styles.wrapItemBody}>
                <Image
                    source={
                        item.item.code === undefined || item.item.code === null ?
                            item.item.product_json.file
                                ? { uri: item.item.product_json.file }
                                : require('../../../Assets/image/notfound.png')
                            : require('../../../Assets/image/logo1.png')
                    }
                    style={styles.product}
                    resizeMode={'contain'}
                />
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 12, color: colors.black }}>
                        {
                            item.item.code === undefined || item.item.code === null ?
                                item.item.product_json.title : item.item.code
                        }
                    </Text>
                    <Text style={{ fontSize: 11, color: colors.black }}>
                        {item.item.qty} x{' '}
                        {
                            typeAkun === 'agent' ? <FormatMoney value={item.item.product_json.price_agent} /> :
                                <FormatMoney value={item.item.product_json.price} />
                        }
                        {' '}
                    </Text>
                </View>
                {
                    typeAkun === 'agent' ?
                        <View>
                            <FormatMoney value={Number(item.item.qty) * Number(item.item.product_json.price_agent)}
                                style={styles.textBelanja} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 8 }}>
                                <Image
                                    source={require('../../../Assets/icon/poin.png')}
                                    style={{ width: 24, height: 24 }}
                                />
                                <Text style={{ fontSize: 12, color: colors.black }}>
                                    {' '}
                                    <FormatMoney unit={'+'} value={item.poin} />
                                </Text>
                            </View>

                        </View> : false
                }
            </View>
            {item.items_count - 1 === 0 ? (
                false
            ) : (
                <Text style={styles.fontPrimary}>
                    + {item.items_count - 1} Produk Lainnya
                </Text>
            )}
            <View style={styles.horizontal} />
            <View style={styles.wrapItemFooter}>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 12, color: colors.black }}>Total Pesanan</Text>
                    {/* <Text style={{ color: colors.black, fontWeight: 'bold' }}>{item.item.subtotal}</Text> */}
                    <FormatMoney value={item.subtotal}
                        style={{ color: colors.black, fontWeight: 'bold' }} />
                </View>
            </View>
        </TouchableOpacity>
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>

            <FlatList
                showsVerticalScrollIndicator={false}
                data={data}
                renderItem={renderItem}
                ListFooterComponent={() =>
                    hasScrolled === true ? (
                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ color: colors.black }}>Load more ....</Text>
                        </View>
                    ) : (
                        <View style={{ height: 130 }} />
                    )
                }
                onScroll={() => (pass === false ? onScroll() : false)}
                onEndReached={() => (nextLink === null ? false : handleLoadMore())}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
                ListEmptyComponent={
                    <View style={{ alignItems: 'center', marginTop: windowHeight / 4 }}>
                        {/* <Text style={Font.regular12}>Gulir kebawah untuk menyegarkan halaman</Text> */}
                        <Image source={require('../../../Assets/image/no-data.png')} style={{ width: 130, height: 130, resizeMode: 'contain' }} />
                        <Text style={Font.regular12}>Data tidak ditemukan</Text>
                    </View>
                }
            />
        </SafeAreaView>
    );
};

export default HBelanjaGetAgen;

const styles = StyleSheet.create({
    fontPrimary: { fontSize: 12, color: colors.primary },
    TouchableStyle: {
        backgroundColor: colors.primary,
        width: 110,
        height: 32,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
    },

    // renderMenu
    wrapRenderMenu: { flex: 1, backgroundColor: colors.white, height: 50 },
    wrapRenderMenu1: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
    },
    wrapcount: {
        width: 18,
        height: 18,
        backgroundColor: colors.primary,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    // renderItem
    wrapRenderItem: {
        flex: 1,
        marginHorizontal: 8,
        marginVertical: 4,
        borderRadius: 7,
        backgroundColor: colors.white,
        padding: 8,
    },
    wrapItemHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    bagIcon: { tintColor: colors.primary, width: 24, height: 24, marginRight: 4 },
    textBelanja: { fontSize: 12, color: colors.black, fontWeight: 'bold' },

    horizontal: {
        borderBottomColor: 'rgba(0, 0, 0, 0.07)',
        borderBottomWidth: 1,
        marginVertical: 8,
    },
    wrapItemBody: { flexDirection: 'row', flex: 1, marginBottom: 10 },
    product: { width: 56, height: 56, borderRadius: 7, marginRight: 8 },
    wrapItemFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});
