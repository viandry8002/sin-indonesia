import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  ToastAndroid,
  View,
  Alert,
} from 'react-native';
import { TextMask } from 'react-native-masked-text';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../Styles/Colors';
import { api, key, postProduct } from '../../../Variable';
import BtnDisable from '../../MultipleComponent/BtnDisable';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import Font from '../../../Styles/Font';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import { Input } from 'react-native-elements';
import SpinnerLoad from '../../MultipleComponent/SpinnerLoad';

const dataDumy = [
  {
    id: 198,
    user_id: '56',
    product_id: '121',
    qty: '1',
    created_at: '2023-06-12T07:16:44.000000Z',
    updated_at: '2023-06-15T04:02:01.000000Z',
    is_use_voucher: false,
    price: 30000,
    poin: 0,
    product: {
      id: 121,
      category_id: {
        id: 21,
        title: 'Minuman',
        file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
        gain_poin: '1',
      },
      title: 'Susu Kesehatan Sin Mega Remeng',
      description: 'Kopi Mega Remeng Kopi Turki',
      weight: '30',
      length: '5',
      width: '5',
      height: '5',
      poin: 0,
      price: '30000',
      price_from: null,
      is_recommended: '0',
      is_use_voucher: '0',
      sold_count: '7',
      // file: 'https://serverwan.com/backend-sin-new/img/product/121/1668052416_WhatsApp Image 2022-11-10 at 10.19.49.jpg',
      file: require('../../../Assets/image/produk2.png'),
      whislist: false,
      qty: 128,
    },
  },
  {
    id: 199,
    user_id: '56',
    product_id: '121',
    qty: '1',
    created_at: '2023-06-12T07:16:44.000000Z',
    updated_at: '2023-06-15T04:02:01.000000Z',
    is_use_voucher: false,
    price: 30000,
    poin: 0,
    product: {
      id: 121,
      category_id: {
        id: 21,
        title: 'Minuman',
        file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
        gain_poin: '1',
      },
      title: 'Mega Remeng Kopi Turki',
      description: 'Kopi Mega Remeng Kopi Turki',
      weight: '30',
      length: '5',
      width: '5',
      height: '5',
      poin: 0,
      price: '30000',
      price_from: null,
      is_recommended: '0',
      is_use_voucher: '0',
      sold_count: '7',
      file: require('../../../Assets/image/produk3.png'),
      whislist: false,
      qty: 128,
    },
  },
  {
    id: 200,
    user_id: '56',
    product_id: '121',
    qty: '1',
    created_at: '2023-06-12T07:16:44.000000Z',
    updated_at: '2023-06-15T04:02:01.000000Z',
    is_use_voucher: false,
    price: 30000,
    poin: 0,
    product: {
      id: 121,
      category_id: {
        id: 21,
        title: 'Minuman',
        file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
        gain_poin: '1',
      },
      title: 'Rokok Sin Kepo',
      description: 'Kopi Mega Remeng Kopi Turki',
      weight: '30',
      length: '5',
      width: '5',
      height: '5',
      poin: 0,
      price: '30000',
      price_from: null,
      is_recommended: '0',
      is_use_voucher: '0',
      sold_count: '7',
      file: require('../../../Assets/image/produk4.png'),
      whislist: false,
      qty: 128,
    },

  },
  {
    id: 201,
    user_id: '56',
    product_id: '121',
    qty: '1',
    created_at: '2023-06-12T07:16:44.000000Z',
    updated_at: '2023-06-15T04:02:01.000000Z',
    is_use_voucher: false,
    price: 30000,
    poin: 0,
    product: {
      id: 121,
      category_id: {
        id: 21,
        title: 'Minuman',
        file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
        gain_poin: '1',
      },
      title: 'Rokok Sin Patriot',
      description: 'Kopi Mega Remeng Kopi Turki',
      weight: '30',
      length: '5',
      width: '5',
      height: '5',
      poin: 0,
      price: '30000',
      price_from: null,
      is_recommended: '0',
      is_use_voucher: '0',
      sold_count: '7',
      file: require('../../../Assets/image/produk5.png'),
      whislist: false,
      qty: 128,
    },

  },
];

const KeranjangAgen = ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [tokens, setTokens] = useState(null);
  // const [chose, setChose] = useState(0);
  const [dataCore1, setDataCore1] = useState([]);
  // const [dataCore2, setDataCore2] = useState([]);
  const [data, setData] = useState([]);
  const [loadCart, setloadCart] = useState(false);

  const [carts, setCarts] = useState([]);
  // const [checklistAll, setChecklistAll] = useState(false);
  const [loadSpiner, setloadSpiner] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
    setTokens(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      // setChecklistAll(false);
      setCarts([]);
      setTimeout(() => {
        getToken();
      }, 100);
    });
    return unsubscribe;
  }, []);

  const showToast = msg => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  };

  const getData = token => {
    // setChecklistAll(false);
    // setCarts([]);

    //produk
    const tbody1 = {
      multi_img: 0,
      type: '',
      per_page: 'all',
    };

    Axios.post(postProduct, tbody1, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('reeeee', res.data);
        res.data.success
          ? (
            carts.length > 0 ?
              spesificCart(carts) :
              setDataCore1({
                price: 0,
                poin: 0,
              }),
            // setDataCore2({ price: 0 }),
            setData(res.data.data))
          : Alert.alert('peringatan', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log('tes', err.response.data);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 1000);
  };

  const updateCart = (idc, qty) => {
    // console.log('wwww', tokens);
    const body = {
      product_id: idc,
      id: idc,
      qty: qty,
    };
    Axios.post(`${api}/agent/cart/add`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        // console.log('hhhhhh', res.data);
        res.data.success
          ? (getData(tokens), onRefresh())
          : Alert.alert('peringatan', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const spesificCart = paramCart => {
    let body = { id: paramCart };
    // console.log('popopopo', body);
    // Axios.post(`${api}/agent/cart`, body, {
    Axios.post(`${api}/agent/cart`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        // setloadSpiner(false)
        // console.log('ggggg', res.data);
        res.data.success
          ?
          (
            // res.data.price === dataCore2.price
            // ? setChecklistAll(true)
            // : 
            // (
            //   setChecklistAll(false),
            // setDataCore1({
            //   price: 0,
            //   poin: 0,
            // }),
            // ),
            setDataCore1(res.data))
          : Alert.alert('peringatan', res.data?.message);
      })
      .catch(err => {
        // setloadSpiner(false)
        console.log(err);
      });
  };

  const check = idp => {
    idp === null ?
      Alert.alert('peringatan', 'isi qty terlebih dahulu')
      :
      (setCarts(carts => [...carts, idp]),
        spesificCart([...carts, idp]))
  };

  const uncheck = idp => {
    idp === null ?
      Alert.alert('peringatan', 'isi qty terlebih dahulu')
      :
      (setCarts(carts.filter(x => x != idp)),
        spesificCart(carts.filter(x => x != idp)))
  };

  const renderProduct = ({ item }) => (
    <View style={styles.wrapRender}>
      {/* {console.log('gggggg', item)} */}
      {carts.includes(item.cart_id) ? (
        <MaterialIcons
          name="check-box"
          size={20}
          color={colors.primary}
          onPress={() => {
            // setloadSpiner(true),
            uncheck(item.cart_id)
          }}
        />
      ) : (
        <MaterialIcons
          name="check-box-outline-blank"
          size={20}
          color={colors.primary}
          onPress={() => {
            // setloadSpiner(true),
            check(item.cart_id)
          }}
        />
      )}
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../../Assets/image/notfound.png')
        }
        style={styles.image}
        resizeMode={'contain'}
      />
      <View style={{ flex: 1 }}>
        <View
          onPress={() => navigation.navigate('DtlProduct', item.id)}>
          <Text style={Font.regular12}>{item.title}</Text>
          <Text style={[Font.bold12, { marginVertical: 8 }]}>
            <FormatMoney value={item.price_agent} />
            /BKS
          </Text>
        </View>
        <View style={styles.wrapCount}>
          <TouchableOpacity
            onPress={() => (updateCart(item.id, Number(item.cart_qty) - 1))}
            disabled={item.cart_qty < 1 ? true : false}>
            <Image
              source={require('../../../Assets/icon/min.png')}
              style={styles.imageCont}
            />
          </TouchableOpacity>
          <Input
            label={false}
            style={{ fontSize: 12 }}
            inputContainerStyle={styles.containInput}
            containerStyle={styles.inputContainer}
            value={item.cart_qty.toString()}
            onChangeText={value => {
              item.cart_qty = value
                , setloadCart(!loadCart)
            }
            }
            onEndEditing={() => (
              updateCart(item.id, item.cart_qty)
            )}
            keyboardType={'numeric'}
          />
          <TouchableOpacity
            onPress={() =>
              item.qty > item.cart_qty
                ? (updateCart(item.id, Number(item.cart_qty) + 1))
                : showToast('Mohon maaf stok terbatas')
            }
            disabled={item.qty + 1 > item.cart_qty ? false : true}>
            <Image
              source={require('../../../Assets/icon/add.png')}
              style={styles.imageCont}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 8,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={Font.regular12}>Total Pembelian: </Text>
          <FormatMoney
            value={item.cart_price}
            style={[Font.bold14, { color: colors.primary }]}
          />
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {/* header */}
      {/* <SpinnerLoad loads={loadSpiner} /> */}
      <View style={{ flex: 1 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={renderProduct}
          ListFooterComponent={<View style={{ height: 130 }} />}
          extraData={loadCart}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>

      {/* footer */}
      {
        dataCore1.price >= 10000000 ?
          <View
            style={{
              padding: 8,
              alignItems: 'center',
              backgroundColor: colors.success,
            }}>
            <Text style={{ fontSize: 12, color: colors.white }}>
              Selamat Anda mendapatkan subsidi ongkir hingga Rp100.000
            </Text>
          </View> : false}
      <View style={styles.wrapFooter}>
        <View>
          <FormatMoney value={dataCore1.price} style={styles.price} />
          <View style={styles.wrapPilih}>
            <Image
              source={require('../../../Assets/icon/poin.png')}
              style={styles.poin}
            />
            <Text style={Font.regular11}>
              <FormatMoney value={dataCore1.poin} unit={'+'} /> Poin Didapat
              {/* +{dataCore1.poin} Poin Didapat */}
            </Text>
          </View>
        </View>

        <View style={{ height: 40, width: 137 }}>
          {carts.length > 0 && dataCore1.price > 0 ? (
            <BtnPrimary
              title={'Checkout'}
              pressed={() =>
                navigation.navigate('Checkout', {
                  carts: carts,
                  payment: null,
                  voucher: null,
                  type_akun: 'agent',
                  role: null
                })
              }
            />
          ) : (
            <BtnDisable title={'Checkout'} />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default KeranjangAgen;

const styles = StyleSheet.create({
  // header
  wrapHeader: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  wrapPilih: { flexDirection: 'row', alignItems: 'center' },

  // render
  wrapRender: {
    flex: 1,
    // marginHorizontal: 8,
    // marginVertical: 4,
    backgroundColor: colors.white,
    // borderRadius: 15,
    flexDirection: 'row',
    padding: 16,
    marginBottom: 4,
  },
  image: { width: 72, height: 72, borderRadius: 15, marginHorizontal: 8 },
  title: { fontSize: 12, color: colors.black },
  subtitle: { fontWeight: 'bold', color: colors.primary, marginVertical: 8 },
  wrapCategory: {
    flex: 1,
    backgroundColor: colors.blueShading,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 100,
    width: 70,
    alignItems: 'center',
  },
  TextCategory: { color: colors.primary, fontSize: 12 },
  wrapCount: {
    marginTop: 8,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageCont: { width: 32, height: 32, marginHorizontal: 16 },

  // footer
  wrapFooter: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    elevation: 6,
  },
  price: {
    fontSize: 18,
    color: colors.primary,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  poin: { width: 24, height: 24, marginRight: 4 },
  // inputan
  containInput: { borderBottomWidth: 0 },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
    width: 50,
  },
});
