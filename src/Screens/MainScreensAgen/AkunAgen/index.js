import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Alert,
  Dimensions,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../../Styles/Colors';
import { api, getPoinReferal, key } from '../../../Variable';
import PoinBelanjaElevate from '../../MultipleComponent/PoinBelanjaElevate';
import PoinReferralElevate from '../../MultipleComponent/PoinReferralElevate';
import TextShimmer from '../../MultipleComponent/Shimmer/TextShimmer';

const windowWidth = Dimensions.get('window').width;

const AkunAgen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [dataPoin, setDataPoin] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');
  const [referal, setReferal] = useState([]);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    // setLoading(true);
    getToken();
    // let timer = setInterval(() => {
    //   FormatExpired(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
    //     ? clearInterval(timer)
    //     : setdatess(moment(endDate).diff(moment(), 'seconds'));
    // }, 1000);
    // });
    // return unsubscribe;
  }, [endDate]);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getToken();
      setRefreshing(false);
    }, 1000);
  };

  const getData = token => {
    setLoading(true)
    setTimeout(() => {
      Axios.get(`${api}/user`, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      }).then(res => {
        // console.log('result', res.data.data
        // );
        res.data.success
          ? (setData(res.data.data), getPoin(token))
          : navigation.navigate('GoLogout', res.data?.message);
        setLoading(false);
      }).catch(err => {
        console.log('errormya', err.response);
        setLoading(false);
      });

      //referal
      Axios.get(getPoinReferal, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => {
          setReferal(res.data.data);
          setLoading(false);
        })
        .catch(err => {
          console.log(err);
          setLoading(false);
        });
    }, 500);
  };

  const getAlert = () => {
    Axios.get(`${api}/configuration`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        onAlertLogout(res.data.data.deskripsi_logout);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const onAlertLogout = msg => {
    Alert.alert('Peringatan', msg, [
      {
        text: 'Tidak',
        onPress: () => console.log('Tidak'),
      },
      {
        text: 'Ya',
        onPress: () => navigation.navigate('GoLogout', null),
      },
    ]);
  };

  const BtnList = (pressed, icon, title) => (
    <TouchableOpacity style={styles.btnMenu} onPress={pressed}>
      <View style={styles.wrapMenu}>
        <Image source={icon} style={styles.iconMenu} />
        <Text style={styles.titleMenu}>{title}</Text>
        <AntDesign name="right" size={16} color={colors.gray4} />
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={{ height: 100, backgroundColor: 'red', width: '100%' }}>
          <LinearGradient colors={['#3BA172', '#0B326D']} start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} style={{ flex: 1 }}>
            <View style={styles.wrapProfile}>
              {
                loading ? <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={{ width: 80, height: 80, borderRadius: 100 }}
                /> :
                  <Image
                    source={
                      data?.file
                        ? { uri: data.file }
                        : require('../../../Assets/image/person1.png')
                    }
                    style={{ width: 80, height: 80, borderRadius: 100 }}
                  />
              }
              <View style={{ marginHorizontal: 16, flex: 1 }}>
                {
                  loading ? <>
                    <TextShimmer type={'normal'} />
                    {/* <View style={{ height: 2 }} /> */}
                    {/* <TextShimmer type={'normal'} /> */}
                  </> :
                    <>
                      <Text style={styles.name} numberOfLines={1}>
                        {data?.name}
                      </Text>
                      {/* <Text style={styles.number}>Akun Aktif : {data?.expired_day}</Text> */}
                    </>
                }
                {/* <TouchableOpacity
                  onPress={() => navigation.navigate('KartuAgen')}>
                  <Image
                    source={require('../../../Assets/image/agen.png')}
                    style={styles.member}
                  />
                </TouchableOpacity> */}
              </View>
            </View>
          </LinearGradient>
        </View>

        {/* Poin Belanja */}
        <View style={{
          marginTop: -8,
          // marginBottom: 16
        }}>
          <PoinBelanjaElevate poin={data?.point_order} pressed={() => navigation.navigate('PoinAgen')} />
        </View>

        {/* Poin Referral */}
        {/* <PoinReferralElevate elevation dataReferal={referal} btnHistory={() => navigation.navigate('RiwayatReferral')} /> */}

        {/* elevation2 */}
        <View style={styles.wrapElevation2}>
          {/* {BtnList(
            () => navigation.navigate('PoinReferral'),
            require('../../../Assets/icon/people.png'),
            'Ajak Teman Sebagai Agen',
          )} */}
          {BtnList(
            () => navigation.navigate('EditAkun', { type: 'agent' }),
            require('../../../Assets/icon/pen.png'),
            'Edit Akun',
          )}
          {BtnList(
            () => navigation.navigate('Rekening'),
            require('../../../Assets/icon/wallet.png'),
            'Rekening Saya',
          )}
          {BtnList(
            () => navigation.navigate('KartuAgen'),
            require('../../../Assets/icon/card.png'),
            'Kartu Member',
          )}
          {BtnList(
            () => navigation.navigate('Alamat', { from: 'profile' }),
            require('../../../Assets/icon/pin.png'),
            'Alamat Saya',
          )}
          {BtnList(
            () => navigation.navigate('Bantuan'),
            require('../../../Assets/icon/chat.png'),
            'Bantuan',
          )}
          {BtnList(
            () => getAlert('EditAkun'),
            require('../../../Assets/icon/signOut.png'),
            'Sign Out',
          )}
        </View>
      </ScrollView>
    </SafeAreaView >
  );
};

export default AkunAgen;

const styles = StyleSheet.create({
  // profile
  wrapProfile: {
    flexDirection: 'row',
    marginHorizontal: 16,
    alignItems: 'center',
  },
  name: { fontSize: 18, color: colors.white, fontWeight: 'bold', width: 220 },
  number: { fontSize: 12, color: colors.white },
  member: { width: 160, height: 26, marginTop: 8, borderRadius: 7 },

  // point
  wrapPoint: {
    marginHorizontal: 16,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    width: windowWidth - 32,
  },
  wrapPoint2: {
    marginHorizontal: 16,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    width: windowWidth - 32,
    marginBottom: 16
  },
  textExpired: { fontSize: 12, color: colors.black, marginVertical: 16 },
  point: { width: 24, height: 24, marginRight: 10, borderRadius: 24 },
  tukarPoint: {
    borderRadius: 100,
    borderWidth: 1,
    borderColor: colors.primary,
    height: 34,
    width: 94,
    justifyContent: 'center',
    alignItems: 'center',
  },

  // menu
  wrapElevation2: {
    marginHorizontal: 16,
    backgroundColor: colors.white,
    elevation: 6,
    borderRadius: 7,
    padding: 12,
    width: windowWidth - 32,
    marginBottom: 30,
    marginTop: 16
  },
  btnMenu: { height: 40, marginVertical: 4 },
  wrapMenu: { flexDirection: 'row', alignItems: 'center' },
  iconMenu: { width: 24, height: 24, marginRight: 8 },
  titleMenu: { fontSize: 12, color: colors.black, flex: 1 },
});
