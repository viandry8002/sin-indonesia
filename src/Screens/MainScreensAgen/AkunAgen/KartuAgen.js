import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
    Image,
    ImageBackground,
    SafeAreaView,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View,
    PermissionsAndroid,
    Alert,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import RNFetchBlob from 'rn-fetch-blob';
import colors from '../../../Styles/Colors';
import { key, getProfile, getCardAgen } from '../../../Variable';
import Gradient from '../../MultipleComponent/Gradient';
import SpinnerLoad from '../../MultipleComponent/SpinnerLoad';
import LinearGradient from 'react-native-linear-gradient';
import Font from '../../../Styles/Font';

const KartuAgen = ({ navigation }) => {
    const [data, setData] = useState([]);
    const [dataAgent, setdataAgent] = useState('');
    const [linkCard, setLinkCard] = useState('');
    const [load, setload] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getToken();
        });
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem('api_token');
                return getData(token);
            } catch (err) {
                console.log(err);
            }
        }
        return unsubscribe;
    }, []);

    const requestStoragePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                actualDownload();
            } else {
                if (granted === 'never_ask_again') {
                    actualDownload();
                } else {
                    Alert.alert('Peringatan', 'Izinkan File dan media terlebih dahulu', [
                        {
                            text: 'ok',
                            onPress: () => console.log('Tidak'),
                        },
                    ]);
                    // console.log('External storage permission denied');
                }
            }
        } catch (err) {
            console.warn(err);
        }
    };

    const getData = token => {
        setload(true);
        Axios.get(getProfile, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                // console.log('ppppp', res.data);
                res.data.success
                    ? (setData(res.data.data), setdataAgent(res.data.data.agent))
                    : navigation.navigate('GoLogout', res.data?.message);
            })
            .catch(err => {
                console.log(err);
            });

        Axios.get(getCardAgen, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                setLinkCard(res.data.url);
                setload(false);
            })
            .catch(err => {
                console.log(err, 'gg');
                setload(false);
            });
    };

    const actualDownload = () => {
        const { dirs } = RNFetchBlob.fs;
        RNFetchBlob.config({
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                mediaScannable: true,
                title: `member.pdf`,
                path: `${dirs.DownloadDir}/member.pdf`,
            },
        })
            .fetch('GET', linkCard, {})
            .then(res => {
                // console.log('The file saved to ', res.path());
                ToastAndroid.show('Kartu Member berhasil di simpan', ToastAndroid.LONG);
            })
            .catch(e => {
                console.log(e);
            });
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <SpinnerLoad loads={load} />
            <View>
                <View style={{ height: 44 }}>
                    <Gradient />
                </View>
                <View style={styles.wrapMember}>
                    <ImageBackground
                        source={require('../../../Assets/image/AgenCard.png')}
                        style={styles.backgroundMember}
                        imageStyle={{ borderRadius: 15 }}
                        resizeMode={'contain'}>
                        <View style={{ marginTop: 47, marginLeft: 8 }}>
                            <Text style={[Font.bold12, {
                                color: colors.white,
                                marginTop: 8,
                                marginRight: 22,
                            }]} numberOfLines={1}>
                                {/* {
                                    dataAgent.is_stockist === '0' ? 'AGEN RESMI' : 'STOKIS RESMI'
                                } */}
                                OFFICIAL
                            </Text>
                            <View>
                                <LinearGradient
                                    colors={['#3BA172', '#0B326D']}
                                    start={{ x: -1, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={{ padding: 4, width: 160, marginVertical: 8 }}>
                                    <Text style={[Font.bold10, { color: colors.white }]}>
                                        {/* REGIONAL {cityAgent.name} */}
                                        PT. SIN Indonesia Cemerlang
                                    </Text>
                                </LinearGradient>
                            </View>
                            <Text style={styles.registDate}>
                                ID Number : {dataAgent.id === null ? " -" : dataAgent.id}
                            </Text>
                            <Text style={styles.registDate}>
                                Nama : {data.name === null ? " -" : data.name}
                            </Text>
                            <Text style={styles.registDate}>
                                No KTP : {dataAgent.no_ktp === null ? " -" : dataAgent.no_ktp}
                            </Text>
                            {/* <Text style={styles.registDate}>
                                Masa Lisensi : {moment(data.expired_at).format('DD MMMM YYYY')}
                            </Text> */}
                        </View>
                    </ImageBackground>
                </View>

                <TouchableOpacity
                    style={styles.TouchableStyle}
                    onPress={() => requestStoragePermission()}
                >
                    <AntDesign name="download" color={colors.white} size={20} />
                    <Text style={{ color: colors.white, fontSize: 12, marginLeft: 10 }}>
                        Download Kartu Member
                    </Text>
                </TouchableOpacity>

            </View>
        </SafeAreaView>
    );
};

export default KartuAgen;

const styles = StyleSheet.create({
    // cart member
    wrapMember: { position: 'absolute', alignSelf: 'center' },
    backgroundMember: { width: 343, height: 209, elevation: 6 },
    name: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
        marginTop: 8,
        marginRight: 22,
    },
    registDate: { fontSize: 8, color: colors.white },
    number: { fontSize: 28, color: colors.white, marginTop: 8, marginRight: 22 },
    // btn
    TouchableStyle: {
        backgroundColor: colors.primary,
        height: 46,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        marginTop: 185,
        marginHorizontal: 16,
        flexDirection: 'row',
    },
});
