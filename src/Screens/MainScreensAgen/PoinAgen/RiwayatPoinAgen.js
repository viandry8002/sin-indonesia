import React from 'react'
import { FlatList, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import colors from '../../../Styles/Colors'
import Font from '../../../Styles/Font'
import BtnPrimary from '../../MultipleComponent/BtnPrimary'
import FormatMoney from '../../MultipleComponent/FormatMoney'

const listData = [
    {
        title1: 'Uang Tunai Rp18.000.000 ',
        title2: 'Tgl Penukaran : 20/02/2022',
        date: 'ID Penukaran : SIN20022002001',
        nominal: 30000,
        status: 'Pending',
        type: 'penarikan'
    },
    {
        title1: 'Uang Tunai Rp18.000.000 ',
        title2: 'Tgl Penukaran : 20/02/2022',
        date: 'ID Penukaran : SIN20022002001',
        nominal: 30000,
        status: 'Berhasil',
        type: 'penarikan'
    },
    {
        title1: 'Uang Tunai Rp18.000.000 ',
        title2: 'Tgl Penukaran : 20/02/2022',
        date: 'ID Penukaran : SIN20022002001',
        nominal: 30000,
        status: 'Berhasil',
        type: 'pemasukan'
    },
]

const RiwayatPoinAgen = () => {

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <FlatList
                data={listData}
                renderItem={({ item }) => (
                    <View style={{ flexDirection: 'row', padding: 8, marginHorizontal: 8, marginTop: 8, flex: 1, borderRadius: 8, elevation: 2, backgroundColor: colors.white }}>
                        <View style={{ flex: 0.5 }}>
                            <Image source={require('../../../Assets/icon/moneyCircle.png')} style={{ width: 40, height: 40 }} />
                        </View>
                        <View style={{ width: 8 }} />
                        <View style={{ flex: 3 }}>
                            <Text style={Font.semiBold14}>{item.title1}</Text>
                            <Text style={[Font.regular12, { marginVertical: 8 }]}>{item.title2}</Text>
                            <Text style={Font.regular12}>{item.date}</Text>
                        </View>
                        <View style={{ width: 8 }} />
                        <View style={{ flex: 1.2 }}>
                            <View style={styles.wrapPoint}>
                                <Image
                                    source={require('../../../Assets/icon/poin.png')}
                                    style={styles.point}
                                    resizeMode={'contain'}
                                />
                                <FormatMoney value={30000} style={[Font.regular12, { flex: 1 }]} unit={item.type === 'penarikan' ? '-' : '+'} />
                            </View>
                        </View>
                    </View>
                )}
                ListFooterComponent={<View style={{ height: 32 }} />}
            />
            <View style={{ bottom: 16, right: 16, width: 141, position: 'absolute' }}>
                <BtnPrimary title={'Hubungi CS'} icon={<Ionicons name='call' size={16} style={{ color: colors.white, marginRight: 10 }} />} />
            </View>
        </SafeAreaView>
    )
}

export default RiwayatPoinAgen

const styles = StyleSheet.create({
    wrapPoint: {
        // flex: 1,
        flexDirection: 'row',
        padding: 6,
        backgroundColor: colors.yellowShading,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    point: { width: 24, height: 24, marginRight: 4 },
})