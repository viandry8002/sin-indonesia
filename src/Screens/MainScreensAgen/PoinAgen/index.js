import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
    Dimensions,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    ToastAndroid,
    View,
    Alert
} from 'react-native';
import colors from '../../../Styles/Colors';
import Font from '../../../Styles/Font';
import { getAgentPoinHistory, getAgentPoinRedem, key } from '../../../Variable';
import BtnDisable from '../../MultipleComponent/BtnDisable';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import Gradient from '../../MultipleComponent/Gradient';
import RiwayatReferral from '../PoinReferral/RiwayatReferral';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';

const windowWidth = Dimensions.get('window').width;

const PoinAgen = ({ navigation, route }) => {
    const [dataRekening, setDataRekening] = useState([]);
    const [dataOrderHistory, setDataOrderHistory] = useState([]);
    const [tokens, setTokens] = useState(null);

    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('api_token');
            setTokens(token)
            return getData(token);
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        getToken();
    }, []);

    const getData = token => {
        Axios.get(getAgentPoinHistory, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                // console.log('regggg', res.data);
                setDataOrderHistory(res.data)
                setDataRekening(res.data.account)
            })
            .catch(err => {
                console.log(err, 'ggggg');
            })
    };

    const onSubmit = (amount, account) => {
        const form = {
            amount: amount,
            account_id: account
        }

        Axios.post(getAgentPoinRedem, form, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + tokens,
            },
        })
            .then(res => {
                res.data.success === true
                    ? (ToastAndroid.show(
                        'Pengajuan Sukses',
                        ToastAndroid.SHORT),
                        navigation.navigate('SuccessPoin'))
                    : Alert.alert('Peringatan', res.data.message)

                // setload(false)
            })
            .catch(err => {
                console.log('aass', err.response.data);
                // Alert.alert('Peringatan', err.response.data.message);
                // setload(false)
            });
    };


    const wrapRadius = (image, text1, text2) => {
        return (
            <View style={{ flexDirection: 'row', padding: 8, borderRadius: 8, borderWidth: 1, borderColor: colors.gray6, marginTop: 8 }} >
                <View style={{ padding: 8, backgroundColor: colors.gray6, borderRadius: 8, marginRight: 8 }}>
                    {image}
                </View>
                <View >
                    {text1}
                    {text2}
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            {/* <ScrollView> */}
            <View style={{ height: 136 }}>
                <Gradient />
            </View>

            <View style={styles.wrapPoint}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={require('../../../Assets/icon/poinBelanja.png')}
                        style={{ width: 24, height: 24 }}
                        resizeMode={'contain'}
                    />
                    <Text style={[Font.regular12, { color: colors.white, marginLeft: 4 }]} >Sisa Poin Belanja</Text>
                </View>
                <FormatMoney value={dataOrderHistory.amount} unit={''} style={[Font.bold26, { color: colors.white }]} />
                <Text style={[Font.regular12, { color: colors.white, marginLeft: 4 }]} >Akumulasi Poin Belanja = <FormatMoney value={dataOrderHistory.akumulasi_poin} unit={''} /></Text>
                {/* elevation1 */}
                <View style={{ padding: 16, marginVertical: 16, backgroundColor: colors.white, elevation: 2, borderRadius: 8 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={[Font.medium14]}>Tukarkan Poin</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={require('../../../Assets/icon/poinBelanja.png')}
                                style={{ width: 16, height: 16 }}
                                resizeMode={'contain'}
                            />
                            <Text style={Font.regular12}> {dataOrderHistory.description_redeem_point}</Text>
                        </View>
                    </View>
                    {
                        wrapRadius(<Image
                            source={require('../../../Assets/icon/poinBelanja.png')}
                            style={{ width: 24, height: 24 }}
                            resizeMode={'contain'}
                        />, <Text style={Font.regular12}>Poin Ditukarkan</Text>, <FormatMoney value={dataOrderHistory.max_redeem} unit={''} style={Font.medium14} />)
                    }
                    <Text style={[Font.medium14, { marginTop: 16 }]}>
                        Rekening
                    </Text>
                    {
                        wrapRadius(<Image
                            source={{ uri: dataRekening?.bank_file }}
                            style={{ width: 24, height: 24 }}
                            resizeMode={'contain'}
                        />, <Text style={Font.medium14}>{dataRekening?.atas_nama}</Text>, <Text style={Font.regular12}>{dataRekening?.no_rekening}</Text>)
                    }
                    <View style={{ height: 16 }} />
                    {
                        dataOrderHistory.max_redeem >= dataOrderHistory.point_redeem_agent ?
                            <BtnPrimary title={'Tukarkan Poin'} pressed={() => onSubmit(dataOrderHistory.max_redeem, dataRekening?.id)} />
                            :
                            <BtnDisable title={'Tukarkan Poin'} />
                    }
                </View>
                <Text style={[Font.bold16, { marginVertical: 8 }]}>History Penukaran</Text>
            </View>
            <RiwayatReferral type={'orderPoin'} />
        </SafeAreaView>
    );
};

export default PoinAgen;

const styles = StyleSheet.create({
    // point
    wrapPoint: {
        marginHorizontal: 16,
        // padding: 16,
        // width: windowWidth - 16,
        marginTop: -120,
    },
    point: { width: 24, height: 24, marginRight: 10, borderRadius: 24 },
});
