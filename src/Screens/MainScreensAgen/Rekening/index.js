import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import {
    Alert,
    FlatList,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import { Modalize } from 'react-native-modalize';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import Font from '../../../Styles/Font';
import { getBanks, key, postBankAccount } from '../../../Variable';
import BtnDisable from '../../MultipleComponent/BtnDisable';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import Horizontal from '../../MultipleComponent/Horizontal';
import InputComp from '../../MultipleComponent/InputComp';

const Rekening = ({ route, navigation }) => {
    const [listBanks, setlistBanks] = useState([]);
    const [tokens, setTokens] = useState('');
    const modalListBank = useRef(null);
    const [form, setForm] = useState({
        bank_id: '',
        bank_name: '',
        bank_file: '',
        no_rekening: '',
        atas_nama: '',
    });

    const onOpen = modal => {
        modal.current?.open();
    };

    const onClose = modal => {
        modal.current?.close();
    };

    const getToken = async () => {
        const token = await AsyncStorage.getItem('api_token');
        getData(token);
    };

    useEffect(() => {
        getToken();
    }, []);

    const getData = token => {
        setTokens(token);

        Axios.get(postBankAccount, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                // console.log('success', res.data);
                setForm({
                    ...form,
                    ['bank_id']: res.data.data.bank_id,
                    ['bank_name']: res.data.data.bank,
                    ['bank_file']: res.data.data.bank_file,
                    ['no_rekening']: res.data.data.no_rekening,
                    ['atas_nama']: res.data.data.atas_nama
                })
            })
            .catch(err => {
                console.log(err, 'ggggg');
            })

        Axios.get(getBanks, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                setlistBanks(res.data.data)
            })
            .catch(err => {
                console.log(err);
            });

    };

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    };

    const onSubmit = () => {
        Axios.post(postBankAccount, form, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + tokens,
            },
        })
            .then(res => {
                console.log('success', res.data);
                res.data.success
                    ? (navigation.navigate('AkunAG'), ToastAndroid.show('Rekening berhasil di simpan', ToastAndroid.SHORT))
                    : (Alert.alert('Peringatan', res.data.message));
            })
            .catch(err => {
                Alert.alert('Peringatan', err.response.data.message);
                console.log(err);
            })
    };

    const renderBank = ({ item }) => (
        <TouchableOpacity
            onPress={() => {
                setForm({
                    ...form,
                    ['bank_id']: item.id,
                    ['bank_name']: item.title,
                    ['bank_file']: item.file,
                }),
                    onClose(modalListBank)
            }} style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 16 }}>
            <Image source={{ uri: item.file }} style={{ width: 48, height: 16, resizeMode: 'contain', marginRight: 8 }} />
            <Text style={Font.regular14}>{item.title}</Text>
            <Horizontal />
        </TouchableOpacity>
    );

    return (
        <View style={{ flex: 1, backgroundColor: colors.white }}>
            <View
                style={{ marginHorizontal: 16, flex: 1 }}>

                <InputComp
                    text={'Bank'}
                    placeholder={'Pilih Bank'}
                    value={form.bank_name}
                    press={() => onOpen(modalListBank)}
                    inputfocus={false}
                    ricon={
                        <Ionicons
                            name="chevron-down"
                            size={18}
                            color={colors.black}
                            onPress={() => onOpen(modalListBank)}
                        />
                    }
                    licon={
                        form.bank_file !== '' ? (
                            <View >
                                <Image source={{ uri: form.bank_file }} style={{ width: 48, height: 16, resizeMode: 'contain', marginRight: 8 }} />
                            </View>
                        )
                            : false
                    }
                />

                < InputComp
                    text={'No Rekening'}
                    placeholder={'No Rekening'}
                    value={form.no_rekening}
                    onChange={value => onInputChange(value, 'no_rekening')}
                // mlength={5}
                />

                <InputComp
                    text={'Nama Pemilik Rekening'}
                    placeholder={'Nama pemilik rekening'}
                    value={form.atas_nama}
                    onChange={value => onInputChange(value, 'atas_nama')}
                />
            </View >
            {
                form.bank_id === '' ||
                    form.bank_name === '' ||
                    form.bank_file === '' ||
                    form.no_rekening === '' ||
                    form.atas_nama === '' ?
                    <View style={styles.footer}>
                        <BtnDisable title={'Simpan Rekening'} />
                    </View>
                    :
                    <View style={styles.footer}>
                        <BtnPrimary title={'Simpan Rekening'} pressed={() => onSubmit()} />
                    </View>
            }

            <Modalize
                ref={modalListBank}
                handlePosition={'inside'}
                modalHeight={400}>
                <View style={{ padding: 16 }}>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.title}>Pilih Bank</Text>
                        <MaterialCommunityIcons
                            name="close"
                            size={24}
                            color={colors.black}
                            onPress={() => onClose(modalListBank)}
                        />
                    </View>

                    <FlatList
                        style={{ marginTop: 8 }}
                        showsVerticalScrollIndicator={false}
                        data={listBanks}
                        renderItem={renderBank}
                        ListEmptyComponent={
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Text style={Font.regular12}>{'tunggu sebentar..'}</Text>
                            </View>
                        }
                    />

                </View>
            </Modalize>
        </View >
    );
};

export default Rekening;
const styles = StyleSheet.create({
    footer: {
        backgroundColor: colors.white,
        elevation: 6,
        height: 70,
        paddingHorizontal: 16,
        justifyContent: 'center',
    },
    title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },
    wrapDetailOrder: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
    },
});
