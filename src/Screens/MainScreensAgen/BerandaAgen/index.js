import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  StatusBar
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { TextMask } from 'react-native-masked-text';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key, getAgentHome, getPoinReferal, postAddCart, postProduct } from '../../../Variable';
import OneSignal from 'react-native-onesignal';
import FormatExpired from '../../MultipleComponent/FormatExpired';
import GoLogout from '../../MultipleComponent/GoLogout';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import TextShimmer from '../../MultipleComponent/Shimmer/TextShimmer';
import Horizontal from '../../MultipleComponent/Horizontal';
import SinggleBanner from '../../MultipleComponent/SinggleBanner';
import Gradient from '../../MultipleComponent/Gradient';
import PoinBelanjaElevate from '../../MultipleComponent/PoinBelanjaElevate';
import PoinReferralElevate from '../../MultipleComponent/PoinReferralElevate';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import Font from '../../../Styles/Font';

const windowWidth = Dimensions.get('window').width;

const BerandaAgen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [dataUser, setdataUser] = useState('');
  const [dataReminder, setDataReminder] = useState([]);
  const [dataOrder, setdataOrder] = useState([]);
  const [referal, setReferal] = useState('');
  const [refreshing, setRefreshing] = useState(false);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getToken();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getToken();
      setRefreshing(false);
    }, 1000);
  };

  const getData = token => {
    //agent home
    Axios.get(getAgentHome, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('popopop', res.data);
        res.data.success ? (
          setData(res.data),
          setdataUser(res.data.user),
          setDataReminder(res.data.reminder),
          setdataOrder(res.data.order))
          :
          Alert.alert('Peringatan', res.data.message, [
            { text: 'OK', onPress: () => navigation.navigate('GoLogout', null) },
          ])
      })
      .catch(err => {
        console.log('errrr', err.response.data);
      });

    //referal
    Axios.get(getPoinReferal, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success ?
          setReferal(res.data.data) : false
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {/* <View style={{ height: 130 }}> */}
        <View style={{ height: 152 }}>
          <Gradient />
        </View>
        {/* {loading === true ? (
          <View style={styles.wrapProfile}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{ width: 50, height: 50, borderRadius: 100 }}
            />
            <View style={{ marginLeft: 16 }}>
              <TextShimmer type={'normal'} />
              <View style={{ height: 8 }} />
              <TextShimmer type={'normal'} />
            </View>
          </View>
        ) : ( */}
        <View style={styles.wrapProfile}>
          <Image
            source={
              dataUser?.file
                ? { uri: dataUser.file }
                : require('../../../Assets/image/person1.png')
            }
            style={{ width: 50, height: 50, borderRadius: 100 }}
          />
          <View style={{ marginHorizontal: 16, flex: 1 }}>
            <Text style={styles.name} numberOfLines={1}>
              Hi{' '}
              {dataUser?.name}
            </Text>
            {/* <TouchableOpacity
              onPress={() => navigation.navigate('KartuAgen')}>
              <Image
                source={require('../../../Assets/image/agen.png')}
                style={styles.member}
              />
            </TouchableOpacity> */}
            {/* <Text style={styles.number}>Akun Aktif :  {dataUser?.expired_day}</Text> */}
          </View>
        </View>
        {/* )} */}

        {/* elevation1 */}
        <View style={{ marginTop: 10 }}>
          <PoinBelanjaElevate poin={data.point} pressed={() => navigation.navigate('PoinAgen')} />
        </View>
        {/* <View style={{ height: 16 }} /> */}
        {/* elevation2 */}
        {/* <PoinReferralElevate elevation dataReferal={referal} btnHistory={() => navigation.navigate('RiwayatReferral')} /> */}

        <View style={styles.wrapElevation2}>
          <SinggleBanner image={{ uri: data.banner }} />
          <Text
            style={styles.title}>
            Reminder
          </Text>
          {
            dataReminder.map((d, i) =>
            (
              <TouchableOpacity style={styles.wrapHeader} key={i}
                onPress={() =>
                  d.invoice_id != null ?
                    navigation.navigate('DtlPesanan', { invoice: d.invoice_id, type: d.detail }) : navigation.navigate('PoinAgen')
                }
              >
                <Image
                  source={require('../../../Assets/icon/speaker.png')}
                  style={{ width: 36, height: 36, marginRight: 8 }}
                />
                <View style={{ flex: 1 }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 8,
                    }}>
                    <Text style={{ fontSize: 12, color: colors.primary }}>
                      Reminder
                    </Text>
                    <Text style={{ fontSize: 10, color: colors.gray3 }}>
                      {d.created}
                    </Text>
                  </View>
                  <Text
                    style={{
                      color: colors.black,
                      fontSize: 12,
                      fontWeight: '700',
                    }}>
                    {d.description}
                  </Text>
                </View>
              </TouchableOpacity>
            )
            )
          }
          <Text
            style={styles.title}>
            Riwayat Pembelian
          </Text>
          <View style={{ height: 8 }} />
          {
            dataOrder.map((d, i) =>
            (
              <TouchableOpacity
                style={styles.wrapRenderItem}
                key={i}
                onPress={() => navigation.navigate('DtlPesanan', { invoice: d.invoice_id, type: d.detail })}
              >
                <View style={styles.wrapItemHeader}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                      source={require('../../../Assets/icon/Bag_fill.png')}
                      style={styles.bagIcon}
                    />
                    <View>
                      <Text style={styles.textBelanja}>Belanja</Text>
                      <Text style={{ fontSize: 11, color: colors.gray3 }}>
                        {d.created_at}
                        {/* {moment(d.created_at).format('DD MMM YYYY')} */}
                        {/* 27 Nov 2021 */}
                      </Text>
                    </View>
                  </View>
                  {/* <Text style={styles.fontPrimary}>{chose.title}</Text> */}
                  <Text style={{ fontSize: 12, color: colors.primary }}>{d.detail.title}</Text>
                </View>
                <View style={styles.horizontal} />
                <View style={styles.wrapItemBody}>
                  <Image
                    source={
                      { uri: d.item.product.file }
                    }
                    style={styles.product}
                    resizeMode={'contain'}
                  />
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 12, color: colors.black }}>
                      {d.item.product.title}
                    </Text>
                    <Text style={{ fontSize: 11, color: colors.black, marginTop: 8 }}>
                      {d.item.qty} x{' '}
                      <FormatMoney value={d.item.product.price_agent} />{' '}
                    </Text>
                  </View>
                  <View>
                    <FormatMoney value={Number(d.item.qty) * Number(d.item.product.price_agent)}
                      style={styles.textBelanja} />
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 8 }}>
                      <Image
                        source={require('../../../Assets/icon/poinBelanja.png')}
                        style={{ width: 24, height: 24 }}
                      />
                      <Text style={{ fontSize: 12, color: colors.black }}>
                        {' '}+
                        <FormatMoney unit={''} value={d.poin} />
                      </Text>
                    </View>

                  </View>
                </View>
                {d.item_other === 0 ? (
                  false
                ) : (
                  <Text style={[Font.regular12, { color: colors.primary }]}>
                    + {d.item_other} Produk Lainnya
                  </Text>
                )}
                <View style={styles.horizontal} />
                <View style={styles.wrapItemFooter}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 12, color: colors.black }}>Total Pesanan</Text>
                    {/* <Text style={{ color: colors.black, fontWeight: 'bold' }}>{item.item.subtotal}</Text> */}
                    <FormatMoney value={d.subtotal} style={{ color: colors.black, fontWeight: 'bold' }} />
                  </View>
                </View>
              </TouchableOpacity>
            )
            )
          }
        </View>
      </ScrollView>
    </SafeAreaView >
  );
};

export default BerandaAgen;

const styles = StyleSheet.create({
  // profile
  wrapProfile: {
    flexDirection: 'row',
    marginHorizontal: 16,
    alignItems: 'center',
    marginTop: -92
  },
  name: { fontSize: 18, color: colors.white, fontWeight: 'bold', width: 220 },
  number: { fontSize: 12, color: colors.white },
  member: { width: 160, height: 26, marginTop: 8, borderRadius: 7 },

  textExpired: { fontSize: 12, color: colors.black, marginVertical: 16 },
  wrapExpired: {
    backgroundColor: colors.blueShading,
    height: 32,
    borderRadius: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    width: '100%',
  },
  timeExpired: { fontSize: 11, color: colors.primary, marginLeft: 8 },

  // menu
  wrapElevation2: {
    marginHorizontal: 16,
    // backgroundColor: colors.white,
    // elevation: 6,
    // borderRadius: 7,
    // padding: 12,
    // position: 'relative',
    width: windowWidth - 32,
    // marginTop: 16,
    // marginBottom: 130,
    marginBottom: 30,
  },
  // wrapElevation2: { marginHorizontal: 16, backgroundColor: colors.white, elevation: 6, borderRadius: 7, padding: 12, position: 'relative', width: windowWidth - 32, marginTop: 110, marginBottom: 130 },
  btnMenu: { height: 40, marginVertical: 4 },
  wrapMenu: { flexDirection: 'row', alignItems: 'center' },
  iconMenu: { width: 24, height: 24, marginRight: 8 },
  titleMenu: { fontSize: 12, color: colors.black, flex: 1 },
  //==============================================================================
  wrapHeader: {
    borderWidth: 1,
    borderColor: colors.grayShading,
    borderRadius: 16,
    padding: 8,
    flexDirection: 'row',
    marginTop: 8,
  },
  title: { color: colors.black, fontWeight: 'bold', marginBottom: 8, marginTop: 16 },
  //riwayat pembelian
  wrapRenderItem: {
    flex: 1,
    // marginHorizontal: 8, 
    // marginVertical: 4,
    marginBottom: 16,
    borderRadius: 7,
    backgroundColor: colors.white,
    padding: 8,
    borderWidth: 1,
    borderColor: colors.grayShading
  },
  wrapItemHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bagIcon: { tintColor: colors.primary, width: 24, height: 24, marginRight: 4 },
  textBelanja: { fontSize: 12, color: colors.black, fontWeight: 'bold' },

  horizontal: {
    borderBottomColor: 'rgba(0, 0, 0, 0.07)',
    borderBottomWidth: 1,
    marginVertical: 8,
  },
  wrapItemBody: { flexDirection: 'row', flex: 1, marginBottom: 10 },
  product: { width: 56, height: 56, borderRadius: 7, marginRight: 8 },
  wrapItemFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
