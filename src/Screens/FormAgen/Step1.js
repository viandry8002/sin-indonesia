import Axios from 'axios';
import React, { useState } from 'react';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../Styles/Colors';
import { key, postValidateStep1 } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import HeaderRegist from '../MultipleComponent/HeaderRegist';
import InputComp from '../MultipleComponent/InputComp';

const Step1 = ({ navigation }) => {
  const [form, setForm] = useState({
    name: '',
    refferal_id: '',
    email: '',
    ahli_waris: '',
    password: '',
    password_confirmation: '',
  });

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const onSubmit = () => {
    // const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    // reg.test(form.email) === false ?
    //   Alert.alert('Peringatan', 'Format Email tidak sesuai')
    //   :
    Axios.post(postValidateStep1, form, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        res.data.success ?
          navigation.navigate('Step2', { step1: form })
          :
          Alert.alert('Peringatan', res.data.message);
        console.log('success', res.data);
        // res.data.success === true
        //   ? navigation.navigate('Verif', {
        //     type: 1,
        //     id: res.data.data.id,
        //     email: res.data.data.email,
        //   })
        //   : (flashMessage(), setMessage(res.data.message));
        // navigation.navigate('Step2', { step1: form })
      })
      .catch(err => {
        console.log(err.response.data);
        Alert.alert('Peringatan', err.response.data.message);
      })
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}>
        <HeaderRegist
          curentp={0}
          // title={'Daftar Sebagai Agen'}
          title={'Daftar Sebagai Distributor'}
          subtitle={
            // 'Dapatkan banyak benefit dengan \nmenjadi Agen SIN Indonesia'
            'Dapatkan banyak benefit dengan \nmenjadi Distributor SIN Indonesia'
          }
        />

        <View style={styles.wrapBody}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text
              style={{ fontWeight: 'bold', fontSize: 16, color: colors.black }}>
              Biodata
            </Text>
            {/* Input */}
            <InputComp
              text={'Nama Lengkap'}
              placeholder={'masukan nama'}
              value={form.name}
              onChange={value => onInputChange(value, 'name')}
            />

            {/* <InputComp
              text={'Kode Referral (Optional)'}
              placeholder={'Masukan kode referral'}
              value={form.refferal_id}
              onChange={value => onInputChange(value, 'refferal_id')}
            /> */}

            <InputComp
              text={'E-Mail'}
              placeholder={'masukan email'}
              value={form.email}
              onChange={value => onInputChange(value, 'email')}
            />

            {/* <InputComp
              text={'Ahli Waris'}
              placeholder={'masukan Ahli Waris'}
              value={form.ahli_waris}
              onChange={value => onInputChange(value, 'ahli_waris')}
            /> */}
            <InputComp
              text={'Area'}
              placeholder={'masukan Area'}
              value={form.ahli_waris}
              onChange={value => onInputChange(value, 'ahli_waris')}
            />

            <InputComp
              text={'Password'}
              placeholder={'masukan password'}
              value={form.password}
              onChange={value => onInputChange(value, 'password')}
              secure={true}
            />

            <InputComp
              text={'Konfirmasi Password'}
              placeholder={'masukan konfirmasi password'}
              value={form.password_confirmation}
              onChange={value => onInputChange(value, 'password_confirmation')}
              secure={true}
            />

            <View style={{ marginVertical: 16, alignSelf: 'flex-start' }}>
              {form.password === '' ? (
                false
              ) : form.password === form.password_confirmation ? (
                <Text style={styles.textFooter}>Password sesuai</Text>
              ) : (
                <Text style={styles.textFooter2}>Password tidak sesuai</Text>
              )}
            </View>

            {/* {
              form.name == '' || form.email == '' || form.ahli_waris == '' || form.password == '' || form.password_confirmation == '' ?
                <BtnDisable title={'Selanjutnya'} />
                : */}
            <BtnPrimary
              title={'Selanjutnya'}
              pressed={() =>
                onSubmit()}
            // pressed={() => navigation.navigate('Step2', { step1: form })}
            />
            {/* } */}
          </ScrollView>
          {/* {status ? (
            <View style={styles.wrapAlert}>
              <Text style={styles.textAlert}>{message}</Text>
            </View>
          ) : (
            false
          )} */}
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Step1;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  containInput: { borderBottomWidth: 0 },
  labelInput: {
    fontWeight: 'bold',
    color: colors.black,
    fontSize: 12,
    marginTop: 16,
  },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
  wrapFooter: { alignSelf: 'center', flexDirection: 'row', marginTop: 200 },
  textFooter: { color: '#12B76A', fontSize: 12 },
  textFooter2: { color: colors.red, fontSize: 12 },
  // message
  wrapAlert: {
    backgroundColor: colors.red,
    height: 30,
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 32,
    borderRadius: 7,
  },
  textAlert: { fontSize: 12, color: colors.white },
});
