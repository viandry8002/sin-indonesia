import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Modalize } from 'react-native-modalize';
import OneSignal from 'react-native-onesignal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import Font from '../../Styles/Font';
import { getConfiguration, getStoreAddress, key, postRegistAgen, postShipdeoPricings } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import Expedition from '../MultipleComponent/Expedition';
import FormatMoney from '../MultipleComponent/FormatMoney';
import HeaderRegist from '../MultipleComponent/HeaderRegist';
import Horizontal from '../MultipleComponent/Horizontal';
import PaymentMethod from '../MultipleComponent/PaymentMethod';
import Shipment from '../MultipleComponent/Shipment';

const windowWidth = Dimensions.get('window').width;

const Step3 = ({ navigation, route }) => {
  const modalPengiriman = useRef(null);
  const modalKurir = useRef(null);
  const [listCourier, setListCourier] = useState([]);
  const [courier, setCourier] = useState(route.params.courier);
  const [typeDelivery, setTypeDelivery] = useState(null);
  const [shipment_method, setShipment_method] = useState(null);
  // const [payment, setPayment] = useState(null);
  const [dataConfig, setdataConfig] = useState('');

  const onOpen = modal => {
    modal.current?.open();
  };

  const onClose = modal => {
    // modal === modalPengiriman ? setListCourier([]) : false;
    modal.current?.close();
  };

  const openModal = (open, close, method, couriers) => {
    onOpen(open), onClose(close), setShipment_method(method);
    getCourier(couriers);
  };

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    //configuration
    Axios.get(getConfiguration, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setdataConfig(res.data.data);
      })
      .catch(err => {
        console.log('tes1', err);
      });
  };

  const saveToken = async (token, type) => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
        await AsyncStorage.setItem('type_akun', type);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const getCourier = (cParam) => {
    cParam === 'shipdeo-pricings'
      ? Axios.post(
        postShipdeoPricings,
        {
          cart: route.params.step2,
          user_address_id: route.params.address,
        },
        {
          headers: {
            key: key
          },
        },
      )
        .then(res => {
          // console.log('ggggp111111', route.params.step2);
          // console.log('ggggp', route.params.address);
          console.log('zzzzzz', res.data);
          setTypeDelivery(cParam), setListCourier(res.data.data)
        })
        .catch(err => {
          // setRefreshing(false);
          console.log(err.response.data);
        })
      :
      Axios.get(
        getStoreAddress + `/${cParam ? cParam + '?province_id=' + route.params.address.province_id + '&city_id=' + route.params.address.city_id : typeDelivery}`,
        {
          headers: {
            key: key
          },
        },
      )
        .then(res => {
          console.log('hjhjhjh', res.data);
          setListCourier(res.data.data), setTypeDelivery(cParam)
        })
        .catch(err => {
          console.log(err);
        })
  };

  const totalPesanan = () => {
    return route.params.total_all_buy >= dataConfig.order_disc_ongkir_agent ?
      Number(courier?.price) > dataConfig.disc_ongkir_agent ? Number(courier?.price) - dataConfig.disc_ongkir_agent : 0 : Number(courier?.price)
  };

  const postPay = () => {
    // console.log('kokokokok', courier);
    let step1 = route.params.step1;
    let address = route.params.address;
    const body = {
      ...step1,
      products: route.params.step2,
      ...address,
      shipment_request: courier,
      payment_request: route.params.payment,
      shipment_method: shipment_method,
      phone: address?.recipient_phone,
      ads_province_id: address?.province_id,
      ads_city_id: address?.city_id,
      ads_subdistrict_id: address?.subdistrict_id,
      ads_postcode: address?.postcode,
      ads_address: address?.address,
    }

    // console.log('hhhh', body);

    address === null ?
      Alert.alert('Peringatan', 'Alamat harus di isi terlebih dahulu') :
      courier === null || courier === 'domisili' || courier === 'shipdeo-pricings' || courier === 'store-address' ?
        Alert.alert('Peringatan', 'Kurir harus di isi terlebih dahulu') :
        route.params.payment === null ?
          Alert.alert('Peringatan', 'Pembayaran harus di isi terlebih dahulu') :
          Axios.post(
            postRegistAgen,
            body,
            {
              headers: {
                key: key
              },
            },
          )
            .then(res => {
              // console.log('bbbbb', res.data);
              res.data.success ?
                (
                  OneSignal.setAppId('8a32cd5b-0a76-4f8b-9e86-72a92c338bb2'), //app id
                  OneSignal.setLogLevel(6, 0),
                  OneSignal.setExternalUserId(res.data.data.id + '', results => {
                    console.log(
                      'Results of setting external user id ' + res.data.data.id,
                    );
                    console.log(results);
                  }),
                  ToastAndroid.show('Akun berhasil di buat', ToastAndroid.LONG),
                  saveToken(res.data.token, res.data.data.type),
                  navigation.navigate('PembayaranAgen', { data: res.data, kurir: totalPesanan() })
                  // navigation.navigate('PembayaranAgen', res.data)
                )
                : Alert.alert('peringatan', res.data.message)
            })
            .catch(err => {
              // console.log('eeeeee', err);
              console.log(err);
            })
  };

  const renderPengiriman = ({ item }) => (
    <TouchableOpacity
      onPress={() => {
        onClose(modalKurir), setCourier(item);
      }}>
      {item.address ? (
        <Text style={styles.itemPrice2}>{item.address}</Text>
      )
        : item.domisili ? (
          <View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.itemPrice2}>{item.domisili}</Text>
              <Text style={[styles.itemPrice2, { marginLeft: 8 }]}>
                ({item.armada})
              </Text>
            </View>
            <Text style={styles.itemTitle}>{item.description}</Text>
            <FormatMoney style={styles.itemTitle} value={item.price} />
          </View>
        )
          : (
            <View>
              <Text style={styles.itemPrice2}>
                {item.courierCode} - {item.service}
              </Text>
              <FormatMoney style={styles.itemTitle}
                value={item.price} />
            </View>
          )}
      <Horizontal />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      {/* <SpinnerLoad loads={load} /> */}
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}>
        <HeaderRegist
          curentp={2}
          title={'Pembayaran'}
          subtitle={
            // 'Dapatkan banyak benefit dengan \nmenjadi Agen SIN Indonesia'
            'Dapatkan banyak benefit dengan \nmenjadi Distributor SIN Indonesia'
          }
        />

        <View style={styles.wrapBody}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* pengiriman */}
            <Text style={styles.title}>Pengiriman</Text>
            <Shipment address={route.params?.address} pressed={() => navigation.navigate('FormAlamat', {
              type: 'registerAgen',
              id: null,
              region: null,
              step1: route.params.step1,
              step2: route.params.step2,
              address: route.params.address,
              courier: route.params.courier,
              payment: route.params.payment,
              total_all_buy: route.params.total_all_buy,
              from: 'step3'
            })} />

            <Expedition pressed1={() => route.params.address != null ? onOpen(modalPengiriman) : Alert.alert('Peringatan', 'isi alamat terlebih dahulu')}
              pressed2={() => onOpen(modalKurir)} courier={courier} typeDelivery={typeDelivery} />

            {/* pembayaran */}
            <Text style={styles.title}>Metode Pembayaran</Text>
            <PaymentMethod payment={route.params.payment} pressed={() =>
              courier === null || courier === 'domisili' || courier === 'shipdeo-pricings' || courier === 'store-address' ?
                Alert.alert('Peringatan', 'isi pengiriman terlebih dahulu')
                : navigation.navigate('MetodePembayaran', {
                  role: 'agent',
                  step1: route.params.step1,
                  step2: route.params.step2,
                  address: route.params.address,
                  courier: route.params.courier,
                  payment: route.params.payment,
                  total_all_buy: route.params.total_all_buy,
                  shipment_method: shipment_method
                })
            } />

            {/* rincian pesanan */}
            <Text style={styles.title}>Rincian Pesanan</Text>
            <View style={styles.wrapDetailOrder}>
              <Text style={styles.textSend}>Harga Paket</Text>
              <FormatMoney value={route.params.total_all_buy} style={styles.itemTitle} />
            </View>
            <View style={styles.wrapDetailOrder}>
              <Text style={styles.textSend}>Ongkos Kirim</Text>
              {
                route.params.total_all_buy >= dataConfig.order_disc_ongkir_agent ?
                  <>< FormatMoney value={courier ? courier.price : 0}
                    style={[styles.itemTitle, { textDecorationLine: 'line-through', marginRight: 4 }]} />
                    < FormatMoney value={courier ? totalPesanan() : 0}
                      style={[styles.itemTitle, { color: colors.success }]} />
                  </> :
                  <FormatMoney value={courier?.price} style={styles.itemTitle} />
              }
            </View>
            <View style={styles.wrapDetailOrder}>
              <Text style={styles.textSend}>Biaya Layanan</Text>
              <FormatMoney value={route.params.payment?.fee}
                style={styles.itemTitle} />
            </View>
            <Horizontal />
            <View style={styles.wrapDetailOrder}>
              <Text style={styles.title}>Total Pesanan</Text>
              <FormatMoney value={route.params.total_all_buy + totalPesanan() + Number(route.params.payment?.fee)} style={styles.title} />
            </View>
            <View style={{ flex: 1 }}>
              <BtnPrimary
                title={'Bayar'}
                // pressed={() => navigation.navigate('PembayaranAgen')}
                pressed={() => postPay()}
              />
            </View>
          </ScrollView>
        </View>
      </LinearGradient>
      {/* modalize pengiriman ======================================================= */}
      <Modalize
        ref={modalPengiriman}
        handlePosition={'inside'}
        modalHeight={400}>
        <View style={{ padding: 16 }}>
          <View style={styles.wrapDetailOrder}>
            <Text style={styles.title}>Pilih Pengiriman</Text>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={colors.black}
              onPress={() => onClose(modalPengiriman)}
            />
          </View>
          {/* kurir toko */}
          <TouchableOpacity
            onPress={() => {
              setCourier('domisili'),
                openModal(
                  modalKurir,
                  modalPengiriman,
                  'courier_store',
                  'domisili',
                );
            }}>
            <Text style={styles.itemTitle}>Kurir Toko</Text>
            <Horizontal />
          </TouchableOpacity>
          {/* ekspedisi */}
          <TouchableOpacity
            onPress={() => {
              setCourier('shipdeo-pricings'),
                openModal(
                  modalKurir,
                  modalPengiriman,
                  'ekspedition',
                  'shipdeo-pricings',
                );
            }}>
            <Text style={styles.itemTitle}>Ekspedisi</Text>
            <Horizontal />
          </TouchableOpacity>
          {/* ambil di toko */}
          <TouchableOpacity
            onPress={() => {
              setCourier('store-address'),
                openModal(
                  modalKurir,
                  modalPengiriman,
                  'pick_up',
                  'store-address',
                );
            }}>
            <Text style={styles.itemTitle}>Ambil Di Toko (Bebas biaya)</Text>
            <Horizontal />
          </TouchableOpacity>
        </View>
      </Modalize>
      {/* modalize kurir ======================================================= */}
      <Modalize ref={modalKurir} handlePosition={'inside'} modalHeight={400}>
        <View style={{ padding: 16 }}>
          <View style={styles.wrapDetailOrder}>
            <Text style={styles.title}>Pilih Kurir</Text>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={colors.black}
              onPress={() => onClose(modalKurir)}
            />
          </View>

          <FlatList
            style={{ marginTop: 8 }}
            showsVerticalScrollIndicator={false}
            data={listCourier}
            renderItem={renderPengiriman}
            ListEmptyComponent={
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={Font.regular12}>{'tunggu sebentar..'}</Text>
              </View>
            }
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default Step3;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  wrapHeader: { backgroundColor: colors.white },
  title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },

  btnSend: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    padding: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    marginVertical: 8,
    flex: 1,
  },
  iconSend: { marginRight: 8 },
  textSend: { flex: 1, fontSize: 12, color: colors.black },
  itemPrice2: { fontSize: 12, color: colors.black, fontWeight: '500' },
  wrapCourir: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    alignItems: 'center',
  },
  flexRight: { flex: 1, marginRight: 10 },
  itemTitle: { fontSize: 12, color: colors.black },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
});
