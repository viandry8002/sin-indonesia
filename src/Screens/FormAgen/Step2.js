import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View
} from 'react-native';
import { Input } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../Styles/Colors';
import Font from '../../Styles/Font';
import { getConfiguration, getDescription, key, postProduct } from '../../Variable';
import BtnDisable from '../MultipleComponent/BtnDisable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import FormatMoney from '../MultipleComponent/FormatMoney';
import HeaderRegist from '../MultipleComponent/HeaderRegist';
import Horizontal from '../MultipleComponent/Horizontal';

const windowWidth = Dimensions.get('window').width;

const Step2 = ({ navigation, route }) => {
  const [desc, setDesc] = useState([]);
  const [produk, setProduk] = useState([]);
  const [loadCart, setloadCart] = useState(false);
  const [dataConfig, setdataConfig] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    //desc
    Axios.get(getDescription, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setDesc(res.data.data);
      })
      .catch(err => {
        console.log('tes1', err);
      });

    //configuration
    Axios.get(getConfiguration, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setdataConfig(res.data.data);
      })
      .catch(err => {
        console.log('tes1', err);
      });

    //produk
    const tbody1 = {
      multi_img: 0,
      type: '',
      per_page: 'all',
    };

    Axios.post(postProduct, tbody1, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        res.data.data.map((d, i) => { d.checked = false, d.qty_buy = '0', d.total_buy = '0', d.total_pointa = '0' })
        res.data.total_all_buy = 0;
        res.data.total_all_poin = 0;

        setProduk(res.data);
      })
      .catch(err => {
        console.log('tes', err.response.data);
      });
  };

  const showToast = msg => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  };

  const textList = (text, i) => {
    return (
      <View style={{ flexDirection: 'row', marginTop: 8 }} key={i}>
        <AntDesign name="checkcircle" size={18} color={colors.success} />
        <Text
          style={{ fontSize: 14, color: colors.black, marginLeft: 8, flex: 1 }}>
          {text}
        </Text>
      </View>
    );
  };

  const totalBayar = () => {
    let sum = 0;
    let sum1 = 0;
    let datafil = produk.data.filter(d => d.checked === true);
    // console.log('pppo', datafil);
    datafil.forEach(num => {
      sum += Number(num.total_buy);
    })

    // datafil.forEach(num => {
    //   sum1 += Number(num.qty_buy);
    // })

    datafil.forEach(num => {
      sum1 += Number(num.total_pointa);
    })

    produk.total_all_buy = sum;
    produk.total_all_poin = sum1;
  }

  const lanjutBayar = () => {
    let datafil = produk.data.filter(d => d.checked === true);

    const products = datafil.map((d, i) => ({
      product_id: d.id,
      qty: d.qty_buy,
      price: d.total_buy,
      // point: d.poin_agent
    }))

    // console.log('wwwww', { step1: route.params.step1, step2: products });
    // let step1 = route.params.step1
    // console.log('popopo', step1);

    const even = (products) => products.qty === '0' || products.price === '0';

    products.some(even) === true ?
      Alert.alert('Peringatan', 'Qty barang tidak boleh 0')
      :
      navigation.navigate('Step3', {
        step1: route.params.step1,
        step2: products,
        address: null,
        courier: null,
        payment: null,
        total_all_buy: produk.total_all_buy
      })
    // navigation.navigate('Step3', { payment: null })
  }

  const renderProduct = ({ item, i }) => (
    <View key={i}>
      <View style={styles.wrapRender}>
        {item.checked ? (
          <MaterialIcons
            name="check-box"
            size={20}
            color={colors.primary}
            onPress={() => {
              item.checked = !item.checked,
                setloadCart(!loadCart), totalBayar()
            }}
          />
        ) : (
          <MaterialIcons
            name="check-box-outline-blank"
            size={20}
            color={colors.primary}
            onPress={() => {
              item.checked = !item.checked,
                setloadCart(!loadCart), totalBayar()
            }}
          />
        )}
        <Image
          source={
            item.file
              ? { uri: item.file }
              : require('../../Assets/image/notfound.png')
          }
          style={styles.image}
          resizeMode={'contain'}
        />
        <View style={{ flex: 1 }}>
          <View onPress={() => navigation.navigate('DtlProduct', item.id)}>
            <Text style={Font.regular12}>{item.title}</Text>
            <Text style={[Font.bold12, { marginVertical: 8 }]}>
              <FormatMoney value={item.price_agent} />
              /BKS
            </Text>
          </View>
          <View style={styles.wrapCount}>
            <TouchableOpacity
              onPress={() => {
                (item.qty_buy = Number(item.qty_buy) - 1,
                  item.total_buy = item.qty_buy * Number(item.price_agent),
                  setloadCart(!loadCart),
                  item.qty_buy = item.qty_buy + '',
                  item.total_pointa = Number(item.total_pointa) - Number(item.poin_agent),
                  totalBayar())
              }}
              disabled={Number(item.qty_buy) > 0 ? false : true}>
              <Image
                source={require('../../Assets/icon/min.png')}
                style={styles.imageCont}
              />
            </TouchableOpacity>
            <Input
              label={false}
              style={{ fontSize: 12 }}
              inputContainerStyle={styles.containInput}
              containerStyle={styles.inputContainer}
              value={item.qty_buy}
              onChangeText={value => {
                item.qty_buy = value, setloadCart(!loadCart)
              }
              }
              onEndEditing={() => (
                item.qty >= item.qty_buy ?
                  (
                    item.total_buy = Number(item.qty_buy) * Number(item.price_agent),
                    item.total_pointa = Number(item.qty_buy) * Number(item.poin_agent),
                    setloadCart(!loadCart), totalBayar()
                  ) : (item.qty_buy = 0, item.total_buy = 0, item.total_pointa = 0, showToast('qty tidak mencukupi'), setloadCart(!loadCart))
              )}
              keyboardType={'numeric'}
            />
            <TouchableOpacity
              onPress={() =>
              (item.qty_buy = Number(item.qty_buy) + 1,
                item.total_buy = Number(item.qty_buy) * Number(item.price_agent),
                setloadCart(!loadCart),
                item.qty_buy = item.qty_buy + '',
                item.total_pointa = Number(item.total_pointa) + Number(item.poin_agent),
                totalBayar())
              }
              disabled={item.qty > item.qty_buy
                ? false : true}>
              <Image
                source={require('../../Assets/icon/add.png')}
                style={styles.imageCont}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 8,
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={Font.regular12}>Total Pembelian:</Text>
            <FormatMoney
              value={item.total_buy}
              style={[Font.bold14, { color: colors.primary }]}
            />
          </View>
        </View>
      </View>
      <Horizontal />
    </View >
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}>
        <HeaderRegist
          curentp={1}
          title={'Pilih Paket'}
          subtitle={
            // 'Dapatkan banyak benefit dengan \nmenjadi Agen SIN Indonesia'
            'Dapatkan banyak benefit dengan \nmenjadi Distributor SIN Indonesia'
          }
        />

        <View style={styles.wrapBody}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {desc.map((data, i) => (
              <View key={i}>
                <Text style={[styles.titleList, { marginTop: 16 }]}>
                  {data.type}
                </Text>
                {data.data.map((dt, i) => textList(dt.text, i))}
              </View>
            ))}
            {/* Pilih Paket */}
            <View
              style={{
                marginTop: 16,
                marginBottom: 8,
              }}>
              <Text style={[styles.titleList]}>Pilih Produk</Text>
            </View>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={produk.data}
              renderItem={renderProduct}
              ListFooterComponent={<View style={{ height: 30 }} />}
              extraData={loadCart}
            />
          </ScrollView>
        </View>
        {
          produk.total_all_buy >= dataConfig.order_disc_ongkir_agent ?
            <View
              style={{
                padding: 8,
                alignItems: 'center',
                backgroundColor: colors.success,
              }}>
              <Text style={{ fontSize: 12, color: colors.white }}>
                Selamat Anda mendapatkan subsidi ongkir hingga <FormatMoney value={dataConfig.disc_ongkir_agent} />
              </Text>
            </View> : false
        }
        <View style={styles.wrapFooter}>
          <View>
            <FormatMoney value={produk.total_all_buy} style={styles.price} />
            <View style={styles.wrapPilih}>
              <Image
                source={require('../../Assets/icon/poin.png')}
                style={styles.poin}
              />
              <Text style={Font.regular11}>
                +
                <FormatMoney value={produk.total_all_poin} unit={''} /> Poin Didapat
              </Text>
            </View>
          </View>

          <View style={{ height: 40, width: 137 }}>
            {
              produk.total_all_buy > dataConfig.first_order_agent ? (
                <BtnPrimary
                  title={'Lanjut Bayar'}
                  pressed={() => lanjutBayar()}
                />
              )
                : (
                  <BtnDisable title={'Lanjut Bayar'} />
                )}
          </View>
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Step2;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 16,
  },
  titleList: { fontWeight: 'bold', fontSize: 16, color: colors.black },
  // render
  wrapRender: {
    flex: 1,
    // marginHorizontal: 8,
    // marginVertical: 4,
    backgroundColor: colors.white,
    // borderRadius: 15,
    flexDirection: 'row',
    marginTop: 4,
    // marginBottom: 4,
  },
  image: { width: 72, height: 72, borderRadius: 15, marginHorizontal: 8 },
  title: { fontSize: 12, color: colors.black },
  subtitle: { fontWeight: 'bold', color: colors.primary, marginVertical: 8 },
  wrapCount: {
    marginTop: 8,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageCont: { width: 32, height: 32, marginHorizontal: 16 },

  // footer
  wrapFooter: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    elevation: 6,
  },
  price: {
    fontSize: 18,
    color: colors.primary,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  poin: { width: 24, height: 24, marginRight: 4 },
  wrapPilih: { flexDirection: 'row', alignItems: 'center' },
  // inputan
  containInput: { borderBottomWidth: 0 },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
    width: 50,
  },
});
