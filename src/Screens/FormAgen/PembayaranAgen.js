import Clipboard from '@react-native-community/clipboard';
import { useBackHandler } from '@react-native-community/hooks';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import HTML from 'react-native-render-html';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import FormatMoney from '../MultipleComponent/FormatMoney';
import Horizontal from '../MultipleComponent/Horizontal';

const windowDimensions = Dimensions.get('window');

const SECTIONS = [
  {
    title: 'Mobile Banking BCA',
    content: `1. Login ke BCA Mobile
        2. Pilih transfer antar rekening BCA
        3. Masukan nomor virtual account BCA anda
        4. Masukan nominal sesuai tagihan
        5. Ikuti instruksi untuk menyelesaikan transaksi`,
  },
  {
    title: 'SMS Banking BCA',
    content: 'Lorem ipsum... 2',
  },
  {
    title: 'ATM BCA',
    content: 'Lorem ipsum... 3',
  },
  {
    title: 'Internet Banking BCA',
    content: 'Lorem ipsum... 3',
  },
];

const PembayaranAgen = ({ route, navigation }) => {
  // console.log('koko', route.params);
  const [activeSections, setActiveSections] = useState([0]);
  // const [endDate, setendDate] = useState(route.params.data.order.payment_response.expiry_time);
  // const [datess, setdatess] = useState('');

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    // getToken();
    // let timer = setInterval(() => {
    //   FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
    //     ? clearInterval(timer)
    //     : setdatess(moment(endDate).diff(moment(), 'seconds'));
    // }, 1000);
    // });
    // return unsubscribe;
    // }, [endDate]);
  }, []);

  const backAction = () => {
    navigation.navigate('Login')
    return true;
  };

  useBackHandler(backAction)

  const copyToClipboard = (text, type) => {
    Clipboard.setString(text.toString());
    ToastAndroid.show(type + ' telah di salin', ToastAndroid.SHORT);
  };

  const _renderHeader = section => {
    return (
      <View style={styles.wrapAccorHeader}>
        <Text style={{ fontSize: 12, color: colors.black }}>{section.title}</Text>
        <Ionicons name="chevron-down" size={18} color={colors.black} />
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {
          section.steps.map((x) =>
            <HTML
              contentWidth={windowDimensions.width}
              source={{ html: x }}
              allowedStyles={['color', 'fontFamily']}
              baseStyle={{ color: colors.fullGray }}
              ignoredStyles={['color', 'fontFamily']}
              tagsStyles={{
                p: { color: colors.black, },
                ul: { color: colors.black, },
                ol: { color: colors.black, }
              }}
            />)
        }

      </View>
    );
  };

  const Copy = ({ text, type }) => (
    <TouchableOpacity
      style={styles.wrapCopy}
      onPress={() => copyToClipboard(text, type)}>
      <MaterialCommunityIcons
        name="content-copy"
        size={16}
        color={colors.primary}
      />
      <Text style={{ fontSize: 11, color: colors.primary }}>Salin</Text>
    </TouchableOpacity>
  );

  const rowText = (text1, text2) => (
    <View style={styles.wrapDetailOrder}>
      <Text style={styles.textSend}>{text1}</Text>
      <Text style={[styles.textSend, { fontWeight: '500', textAlign: 'right' }]}>
        {text2}
      </Text>
    </View>
  );

  const rowTextMoney = (text1, text2, bold) => (
    <View style={styles.wrapDetailOrder}>
      <Text
        style={[
          styles.textSend,
          bold ? { fontWeight: 'bold', fontSize: 14 } : false,
        ]}>
        {text1}
      </Text>
      <FormatMoney value={text2}
        style={[
          styles.textSend,
          bold
            ? { textAlign: 'right', fontWeight: 'bold', fontSize: 14 }
            : { textAlign: 'right' },
        ]} />
    </View>
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* header */}
        <View style={{ backgroundColor: colors.white, padding: 16 }}>
          {/* Data Informasi Anda */}
          <Text style={styles.title}>Data Informasi Anda</Text>
          <Text style={{ color: colors.black, fontSize: 12, marginBottom: 8 }}>
            Kami akan mengirimkan Invoice ini ke email anda dan informasi
            aktivasi akun anda.
          </Text>
          {/* circle info data diri */}
          <View style={[styles.wrapHeader, { marginBottom: 8 }]}>
            <Text style={styles.title}>Rincian Pesanan</Text>
            {rowText('Nama Lengkap', route.params.data.data.name)}
            {rowText('Email', route.params.data.data.email)}
            {rowText('No. Telpon', route.params.data.address.recipient_phone)}
            {rowText(
              'Alamat',
              route.params.data.address.address,
            )}
          </View>
          {/* Rincian Pesanan */}
          <View style={[styles.wrapHeader, { marginBottom: 8 }]}>
            <Text style={styles.title}>Rincian Pesanan</Text>
            {rowTextMoney('Harga Paket', route.params.data.order.total_order)}
            {rowTextMoney('Ongkos Kirim', route.params.kurir)}
            {rowTextMoney('Biaya Layanan', route.params.data.order.fee)}
            <Horizontal />
            {rowTextMoney('Total Pesanan', route.params.data.order.subtotal, true)}
          </View>
          {/* Cara Pembayaran */}
          {
            route.params.data.order.payment_method === 'online' ?
              <>
                <Text style={styles.title}>Cara Pembayaran</Text>
                <Text style={styles.font12margin}>
                  Transfer sesuai nominal di bawah ini:
                </Text>
                <View style={styles.wrapHeader}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                      source={{ uri: route.params.data.order.payment_request.file }}
                      style={{ width: 48, height: 20, marginRight: 8 }}
                      resizeMode={'contain'}
                    />
                    <Text style={{ color: colors.black }}>{route.params.data.order.payment_request.title}</Text>
                  </View>
                  <Horizontal />
                  <View style={styles.rowSpace}>
                    <View>
                      <Text style={styles.font10}>Nominal</Text>
                      <FormatMoney style={styles.font16Primary}
                        value={parseInt(route.params.data.order.payment_response.data.amount)} />
                    </View>
                    <Copy text={parseInt(route.params.data.order.payment_response.data.amount)} type={'Nominal'} />
                  </View>
                  <Horizontal />
                  <View style={styles.rowSpace}>
                    <View>
                      <Text style={styles.font10}>No Rekening</Text>
                      <Text style={styles.font16Primary}>
                        {route.params.data.order.payment_response.data.pay_code}
                      </Text>
                    </View>
                    <Copy text={route.params.data.order.payment_response.data.pay_code} type={'No Rekening'} />
                  </View>
                </View>
              </> : false
          }
        </View>
        {/* Transfer sebelum */}
        {
          route.params.data.order.payment_method === 'online' ?
            <View style={styles.wrapBody}>
              <Text style={styles.font12margin}>
                Transfer sebelum{' '}
                <Text style={{ fontWeight: 'bold' }}>
                  {moment(route.params.data.order.payment_response.transaction_expired_at).format(
                    'DD MMM YYYY hh:mm',
                  )}{' '}
                  WIB
                </Text> atau
                pesanan kamu akan batal otomatis oleh sistem
              </Text>
              <Text style={styles.font12margin}>
                Transfer akan diverifikasi 1x24 jam
              </Text>

              <Accordion
                sections={route.params.data.order.payment_response.data.instructions}
                activeSections={activeSections}
                renderHeader={_renderHeader}
                renderContent={_renderContent}
                sectionContainerStyle={{ borderWidth: 1, borderColor: colors.grayShading, marginBottom: 8, borderRadius: 7, padding: 8 }}
                onChange={(activeSections) => setActiveSections(activeSections)}
              />
            </View>
            : false
        }
        <View style={{ padding: 8, backgroundColor: colors.white }}>
          <BtnPrimary
            title={'Lanjut Ke Beranda'}
            pressed={() => navigation.navigate('BerandaAgen')}
          />
        </View>
      </ScrollView>
    </SafeAreaView >
  );
};

export default PembayaranAgen;

const styles = StyleSheet.create({
  title: { fontWeight: 'bold', color: colors.black, marginBottom: 8 },
  wrapHeader: {
    borderWidth: 1,
    borderColor: colors.grayShading,
    borderRadius: 7,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  font12margin: { fontSize: 12, color: colors.black, marginBottom: 8 },
  wrapCopy: {
    flexDirection: 'row',
    padding: 8,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 7,
    width: 72,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowSpace: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  font10: { fontSize: 10 },
  font11: { fontSize: 11 },
  font16Primary: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },

  wrapBody: { backgroundColor: colors.white, padding: 16, marginTop: 8 },
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'center',
  },

  wrapAccorHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    paddingVertical: 8,
  },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  textSend: { flex: 1, fontSize: 12, color: colors.black },
});
