import React, { useState } from 'react';
import {
    Dimensions,
    FlatList,
    Image,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../Styles/Colors';
import Font from '../../Styles/Font';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import FormatMoney from '../MultipleComponent/FormatMoney';
import HeaderRegist from '../MultipleComponent/HeaderRegist';
import Horizontal from '../MultipleComponent/Horizontal';
import SpinnerLoad from '../MultipleComponent/SpinnerLoad';

const windowWidth = Dimensions.get('window').width;

const dataDumy = [
    {
        id: 198,
        user_id: '56',
        product_id: '121',
        qty: '1',
        created_at: '2023-06-12T07:16:44.000000Z',
        updated_at: '2023-06-15T04:02:01.000000Z',
        is_use_voucher: false,
        price: 30000,
        poin: 0,
        product: {
            id: 121,
            category_id: {
                id: 21,
                title: 'Minuman',
                file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
                gain_poin: '1',
            },
            title: 'Susu Kesehatan Sin Mega Remeng',
            description: 'Kopi Mega Remeng Kopi Turki',
            weight: '30',
            length: '5',
            width: '5',
            height: '5',
            poin: 0,
            price: '30000',
            price_from: null,
            is_recommended: '0',
            is_use_voucher: '0',
            sold_count: '7',
            // file: 'https://serverwan.com/backend-sin-new/img/product/121/1668052416_WhatsApp Image 2022-11-10 at 10.19.49.jpg',
            file: require('../../Assets/image/produk2.png'),
            whislist: false,
            qty: 128,
        },
    },
    {
        id: 199,
        user_id: '56',
        product_id: '121',
        qty: '1',
        created_at: '2023-06-12T07:16:44.000000Z',
        updated_at: '2023-06-15T04:02:01.000000Z',
        is_use_voucher: false,
        price: 30000,
        poin: 0,
        product: {
            id: 121,
            category_id: {
                id: 21,
                title: 'Minuman',
                file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
                gain_poin: '1',
            },
            title: 'Mega Remeng Kopi Turki',
            description: 'Kopi Mega Remeng Kopi Turki',
            weight: '30',
            length: '5',
            width: '5',
            height: '5',
            poin: 0,
            price: '30000',
            price_from: null,
            is_recommended: '0',
            is_use_voucher: '0',
            sold_count: '7',
            file: require('../../Assets/image/produk3.png'),
            whislist: false,
            qty: 128,
        },
    },
    {
        id: 200,
        user_id: '56',
        product_id: '121',
        qty: '1',
        created_at: '2023-06-12T07:16:44.000000Z',
        updated_at: '2023-06-15T04:02:01.000000Z',
        is_use_voucher: false,
        price: 30000,
        poin: 0,
        product: {
            id: 121,
            category_id: {
                id: 21,
                title: 'Minuman',
                file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
                gain_poin: '1',
            },
            title: 'Rokok Sin Kepo',
            description: 'Kopi Mega Remeng Kopi Turki',
            weight: '30',
            length: '5',
            width: '5',
            height: '5',
            poin: 0,
            price: '30000',
            price_from: null,
            is_recommended: '0',
            is_use_voucher: '0',
            sold_count: '7',
            file: require('../../Assets/image/produk4.png'),
            whislist: false,
            qty: 128,
        },

    },
    {
        id: 201,
        user_id: '56',
        product_id: '121',
        qty: '1',
        created_at: '2023-06-12T07:16:44.000000Z',
        updated_at: '2023-06-15T04:02:01.000000Z',
        is_use_voucher: false,
        price: 30000,
        poin: 0,
        product: {
            id: 121,
            category_id: {
                id: 21,
                title: 'Minuman',
                file: 'https://serverwan.com/backend-sin-new/img/category/1667200314_soft-drink.png',
                gain_poin: '1',
            },
            title: 'Rokok Sin Patriot',
            description: 'Kopi Mega Remeng Kopi Turki',
            weight: '30',
            length: '5',
            width: '5',
            height: '5',
            poin: 0,
            price: '30000',
            price_from: null,
            is_recommended: '0',
            is_use_voucher: '0',
            sold_count: '7',
            file: require('../../Assets/image/produk5.png'),
            whislist: false,
            qty: 128,
        },

    },
];

const Step2 = ({ navigation }) => {
    const [load, setload] = useState(false);
    const [form, setForm] = useState({
        name: '',
        email: '',
        phone: '',
        password: '',
        password_confirmation: '',
    });
    const [status, setStatus] = useState(false);
    const [message, setMessage] = useState('');
    const [carts, setCarts] = useState([]);

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    };

    const flashMessage = () => {
        setStatus(true);
        setTimeout(() => {
            setStatus(false);
        }, 2000);
    };

    const textList = text => {
        return (
            <View style={{ flexDirection: 'row', marginTop: 8 }}>
                <AntDesign name="checkcircle" size={18} color={colors.success} />
                <Text style={{ fontSize: 14, color: colors.black, marginLeft: 8 }}>
                    {text}
                </Text>
            </View>
        );
    };

    const renderProduct = ({ item }) => (
        <>
            <View style={styles.wrapRender}>
                {carts.includes(item.id) ? (
                    <MaterialIcons
                        name="check-box"
                        size={20}
                        color={colors.primary}
                        onPress={() => {
                            uncheck(item.id);
                        }}
                    />
                ) : (
                    <MaterialIcons
                        name="check-box-outline-blank"
                        size={20}
                        color={colors.primary}
                        onPress={() => {
                            check(item.id);
                        }}
                    />
                )}
                <Image
                    // source={
                    //   item.product.file
                    //     ? { uri: item.product.file }
                    //     : require('../../Assets/image/notfound.png')
                    // }
                    source={
                        item.product.file
                    }
                    style={styles.image}
                    resizeMode={'contain'}
                />
                <View style={{ flex: 1 }}>
                    <View
                        onPress={() => navigation.navigate('DtlProduct', item.product.id)}>
                        <Text style={Font.regular12}>{item.product.title}</Text>
                        <Text style={[Font.bold12, { marginVertical: 8 }]}>
                            <FormatMoney value={item.price} />
                            /BKS
                        </Text>
                    </View>
                    <View style={styles.wrapCount}>
                        <TouchableOpacity
                            onPress={() => updateCart(item.id, Number(item.qty) - 1)}>
                            <Image
                                source={require('../../Assets/icon/min.png')}
                                style={styles.imageCont}
                            />
                        </TouchableOpacity>
                        <Text style={Font.semiBold14}>
                            {item.qty}
                            {/* {item.product.qty} */}
                        </Text>
                        <TouchableOpacity
                            onPress={() =>
                                item.product.qty > item.qty
                                    ? updateCart(item.id, Number(item.qty) + 1)
                                    : showToast('Mohon maaf stok terbatas')
                            }
                            disabled={item.product.qty + 1 > item.qty ? false : true}>
                            {/* <TouchableOpacity onPress={() => console.log('asd',item.qty++)}> */}
                            <Image
                                source={require('../../Assets/icon/add.png')}
                                style={styles.imageCont}
                            />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginTop: 8,
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                        <Text style={Font.regular12}>Total Pembelian:</Text>
                        <FormatMoney
                            value={2500000}
                            style={[Font.bold14, { color: colors.primary }]}
                        />
                    </View>
                </View>
            </View>
            <Horizontal />
        </>
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={'transparent'}
                translucent
                barStyle="light-content"
            />
            <SpinnerLoad loads={load} />
            <LinearGradient
                colors={['#3BA172', '#0B326D']}
                start={{ x: -1, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={{ flex: 1 }}>
                <HeaderRegist
                    curentp={1}
                    title={'Pilih Paket'}
                    subtitle={
                        'Dapatkan banyak benefit dengan \nmenjadi Agen SIN Indonesia'
                    }
                />

                <View style={styles.wrapBody}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {/* Syarat Keagenan */}
                        <Text style={styles.titleList}>Syarat Keagenan</Text>
                        {textList('Minim Pembelian 500 Bungkus = 4,5 JT')}
                        {textList('Sudah menjadi Agen Resmi Sin Kepo ')}
                        {/* Keuntungan */}
                        <Text style={[styles.titleList, { marginTop: 16 }]}>Keuntungan</Text>
                        {textList('Modal Rp9.000 Jual Rp12.000 \nUntung 3.000 berbungkus')}
                        {textList('Mendapatkan Sertifikat \nKeagenan dan ID Card E-money')}
                        {textList('Agen hanya ada satu Perkecamatan')}
                        {textList(
                            'Diikut sertakan Program Cashback \n- Minimal pembelian 30.000 bungkus selamat 6 bulan Cashback 18 Juta \n- Point sisa penukaran tidak hangus akan di ikut sertakan ke periode selanjutnya',
                        )}
                        {textList(
                            'Penjanjian keagenan 6 bulan berlaku Saat \npembelian pertama',
                        )}
                        {/* Pilih Paket */}
                        <Text style={[styles.titleList, { marginTop: 16, marginBottom: 8 }]}>
                            Pilih Paket
                        </Text>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={dataDumy}
                            renderItem={renderProduct}
                            ListFooterComponent={<View style={{ height: 130 }} />}
                        // refreshControl={
                        //   <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        // }
                        />
                    </ScrollView>
                </View>
                <View
                    style={{
                        padding: 8,
                        alignItems: 'center',
                        backgroundColor: colors.success,
                    }}>
                    <Text style={{ fontSize: 12, color: colors.white }}>
                        Selamat Anda mendapatkan subsidi ongkir hingga Rp100.000
                    </Text>
                </View>
                <View style={styles.wrapFooter}>
                    <View>
                        <FormatMoney value={10000000} style={styles.price} />
                        <View style={styles.wrapPilih}>
                            <Image
                                source={require('../../Assets/icon/poin.png')}
                                style={styles.poin}
                            />
                            <Text style={Font.regular11}>
                                +
                                <FormatMoney value={10000} unit={''} /> Poin Didapat
                                {/* +{dataCore1.poin} Poin Didapat */}
                            </Text>
                        </View>
                    </View>

                    <View style={{ height: 40, width: 137 }}>
                        {/* {carts.length > 0 ? ( */}
                        <BtnPrimary
                            title={'Lanjut Bayar'}
                            pressed={() =>
                                navigation.navigate('Step3')
                            }
                        />
                        {/* ) : (
              <BtnDisable title={'Checkout'} />
            )} */}
                    </View>
                </View>
            </LinearGradient>
        </SafeAreaView>
    );
};

export default Step2;

const styles = StyleSheet.create({
    wrapBody: {
        flex: 1,
        backgroundColor: colors.white,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 16,
    },
    titleList: { fontWeight: 'bold', fontSize: 16, color: colors.black },
    // render
    wrapRender: {
        flex: 1,
        // marginHorizontal: 8,
        // marginVertical: 4,
        backgroundColor: colors.white,
        // borderRadius: 15,
        flexDirection: 'row',
        padding: 16,
        marginBottom: 4,
    },
    image: { width: 72, height: 72, borderRadius: 15, marginHorizontal: 8 },
    title: { fontSize: 12, color: colors.black },
    subtitle: { fontWeight: 'bold', color: colors.primary, marginVertical: 8 },
    wrapCategory: {
        flex: 1,
        backgroundColor: colors.blueShading,
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderRadius: 100,
        width: 70,
        alignItems: 'center',
    },
    TextCategory: { color: colors.primary, fontSize: 12 },
    wrapCount: {
        marginTop: 8,
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center',
    },
    imageCont: { width: 32, height: 32, marginHorizontal: 16 },

    // footer
    wrapFooter: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        elevation: 6,
    },
    price: {
        fontSize: 18,
        color: colors.primary,
        fontWeight: 'bold',
        marginBottom: 4,
    },
    poin: { width: 24, height: 24, marginRight: 4 },
    wrapPilih: { flexDirection: 'row', alignItems: 'center' },
});
