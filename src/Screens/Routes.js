import React, { useState, useEffect, useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import { api, key } from '../Variable';

// component
import colors from '../Styles/Colors';
import Gradient from './MultipleComponent/Gradient';
import Verif from './MultipleComponent/Verif';
import Success from './MultipleComponent/Success';
import { Badge } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
// Intro
import SplashScreen from './Intro/SplashScreen';
import Onboard from './Intro/Onboard';
import Login from './FormIntro/Login';
import Register from './FormIntro/Register';
import ForgotPass from './FormIntro/ForgotPass';
import CreatePass from './FormIntro/CreatePass';

// Main Screen
import { Input } from 'react-native-elements/dist/input/Input';
import ListProduct from './MultipleComponent/ListProduct';
import ListFlashSale from './MultipleComponent/ListFlashSale';
// Beranda
import Beranda from './MainScreen/Beranda';
import DtlProduct from './MainScreen/Beranda/DtlProduct';
import SearchProduct from './MainScreen/Beranda/SearchProduct';
// Keranjang
import Keranjang from './MainScreen/Keranjang';
import Checkout from './MultipleScreen/Checkout';
import MetodePembayaran from './MainScreen/Keranjang/MetodePembayaran';
import Voucher from './MainScreen/Keranjang/Voucher';
// Pesanan
import Pesanan from './MultipleScreen/Pesanan'
import DtlPesanan from './MultipleScreen/Pesanan/DtlPesanan';
import Pembayaran from './MultipleScreen/Pesanan/Pembayaran';
import BtlPesanan from './MultipleScreen/Pesanan/BtlPesanan';
import PaymentWeb from './MultipleScreen/Pesanan/PaymentWeb';
import TrackingPesanan from './MultipleScreen/Pesanan/TrackingPesanan';

// Akun
import Akun from './MainScreen/Akun';
import EditAkun from './MultipleScreen/EditAkun';
import KartuMember from './MainScreen/Akun/KartuMember';
import TukarPoint from './MainScreen/Akun/TukarPoint';
import DtlTukarPoint from './MainScreen/Akun/TukarPoint/DtlTukarPoint';
import HistoryPenukaran from './MainScreen/Akun/TukarPoint/HistoryPenukaran';
import Bantuan from './MultipleScreen/Bantuan';
import WishList from './MainScreen/Akun/WishList';
import Alamat from './MultipleScreen/Alamat';
import FormAlamat from './MultipleScreen/Alamat/FormAlamat'
import Maps from './MultipleScreen/Alamat/Maps';
import GoLogout from './MultipleComponent/GoLogout';

// header
import Notifikasi from './MainScreen/Header/Notifikasi';
import DirectLogin from './MultipleComponent/DirectLogin';

// new user Agent ========================================
import Step1 from './FormAgen/Step1';
import Step2 from './FormAgen/Step2';
import Step3 from './FormAgen/Step3';
import PembayaranAgen from './FormAgen/PembayaranAgen';
import BerandaAgen from './MainScreensAgen/BerandaAgen';
// keranjang
import KeranjangAgen from './MainScreensAgen/KeranjangAgen';
// pesanan
// akun
import AkunAgen from './MainScreensAgen/AkunAgen';
import KartuAgen from './MainScreensAgen/AkunAgen/KartuAgen';
//referal
import PoinReferral from './MainScreensAgen/PoinReferral';
import RiwayatReferral from './MainScreensAgen/PoinReferral/RiwayatReferral';
import PenarikanReferral from './MainScreensAgen/PoinReferral/PenarikanReferral';
import HBelanjaGetAgen from './MainScreensAgen/PoinReferral/HBelanjaGetAgen';
import DtlHBelanjaGetAgen from './MainScreensAgen/PoinReferral/DtlHBelanjaGetAgen';
import SuccessPoin from './MultipleComponent/SuccessPoin';
import PoinAgen from './MainScreensAgen/PoinAgen';
import RiwayatPoinAgen from './MainScreensAgen/PoinAgen/RiwayatPoinAgen';
// rekening
import Rekening from './MainScreensAgen/Rekening';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const windowWidth = Dimensions.get('window').width;

const styleHead = title => ({
  title: title,
  headerTitleStyle: { fontSize: 14, color: colors.white },
  headerTintColor: colors.white,
  headerBackground: () => <Gradient />,
});

const styleHeadNoBack = title => ({
  title: title,
  headerTitleStyle: { fontSize: 14, color: colors.white },
  headerTintColor: colors.white,
  headerLeft: false,
  headerBackground: () => <Gradient />,
});

// sin reguler ===================================

var theTokens = null;
var countCart = 0;

function BerandaSS({ navigation }) {
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState(0);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    theTokens = token;
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getToken();
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/notification/count`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('result count', res.data.data);
        res.data.success
          ? (setData(res.data.data), cartCount(token))
          : Alert.alert('Peringatan', res.data.message, [
            {
              text: 'ok',
              onPress: navigation.navigate('GoLogout'),
            },
          ]);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const cartCount = tokenp => {
    Axios.get(`${api}/cart/count`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokenp,
      },
    })
      .then(res => {
        countCart = res.data.data;
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Beranda"
        component={Beranda}
        options={({ navigation }) => ({
          headerLeft: '',
          headerTitle: () => (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                width: tokens === null
                  ? windowWidth - 30 : windowWidth - 70,
                backgroundColor: colors.white,
                height: 34,
                alignItems: 'center',
                paddingHorizontal: 16,
                borderRadius: 100,
              }}
              onPress={() => navigation.navigate('SearchProduct')}>
              <Ionicons
                name="search"
                size={18}
                color={colors.primary}
                style={{ marginRight: 8 }}
              />
              <Text style={{ color: colors.gray3, fontSize: 12 }}>
                Cari apa yang kamu mau...
              </Text>
            </TouchableOpacity>
          ),
          headerBackground: () => <Gradient />,
          headerRight: () => (
            <View style={{ marginRight: 16 }}>
              {
                tokens === null
                  ?
                  <View /> :
                  <MaterialCommunityIcons
                    name="bell"
                    size={24}
                    color={colors.white}
                    onPress={
                      tokens === null
                        ? () => navigation.navigate('DirectLogin')
                        : () => navigation.navigate('Notifikasi')
                    }
                  />
              }
              {data === 0 ? (
                false
              ) : (
                <Badge
                  value={data}
                  containerStyle={{ position: 'absolute', top: -5, left: 10 }}
                  badgeStyle={{
                    backgroundColor: colors.red,
                    borderColor: colors.white,
                    borderWidth: 1,
                  }}
                />
              )}
            </View>
          ),
        })}
      />
    </Stack.Navigator>
  );
}

function KeranjangSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Keranjang"
        component={theTokens === null ? DirectLogin : Keranjang}
        options={styleHeadNoBack('Keranjang')}
      />
    </Stack.Navigator>
  );
}

function PesananSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Pesanan"
        component={theTokens === null ? DirectLogin : Pesanan}
        options={styleHeadNoBack('Pesanan')}
      />
    </Stack.Navigator>
  );
}

function AkunSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Akun"
        component={theTokens === null ? DirectLogin : Akun}
        options={styleHeadNoBack('Akun')}
      />
    </Stack.Navigator>
  );
}

const MainTabNavigation = () => {
  return (
    <Tab.Navigator
      // initialRouteName={'Akun'}
      initialRouteName={'BerandaSS'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconColor;
          // let ukuran;

          if (route.name === 'BerandaSS') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Home_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'KeranjangSS') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Bag_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'PesananSS') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Order_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'AkunSS') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/User_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          }
        },
      })}
      activeColor={colors.primary}
      inactiveColor={colors.gray4}
      barStyle={{
        backgroundColor: colors.white,
      }}
      shifting={false}>
      <Tab.Screen
        name="BerandaSS"
        component={BerandaSS}
        options={{ title: 'Beranda' }}
      />
      <Tab.Screen
        name="KeranjangSS"
        component={KeranjangSS}
        options={{ title: 'Keranjang' }}
      />
      <Tab.Screen
        name="PesananSS"
        component={PesananSS}
        options={{ title: 'Pesanan' }}
      />
      <Tab.Screen name="AkunSS" component={AkunSS} options={{ title: 'Akun' }} />
    </Tab.Navigator>
  );
};

// sin agen ===================================

function BerandaAG({ navigation }) {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="BerandaAgen"
        component={BerandaAgen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function KeranjangAG() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Keranjang"
        component={KeranjangAgen}
        options={styleHeadNoBack('Pilih Produk')}
      />
    </Stack.Navigator>
  );
}

function PesananAG() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Pesanan"
        component={Pesanan}
        options={styleHeadNoBack('Pesanan')}
      />
    </Stack.Navigator>
  );
}

function AkunAG() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Akun"
        component={AkunAgen}
        options={styleHeadNoBack('Akun')}
      />
    </Stack.Navigator>
  );
}

const MainAgenTabNavigation = () => {
  return (
    <Tab.Navigator
      // initialRouteName={'Akun'}
      initialRouteName={'BerandaAG'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconColor;
          // let ukuran;

          if (route.name === 'BerandaAG') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Home_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'KeranjangAG') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Bag_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'PesananAG') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/Order_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          } else if (route.name === 'AkunAG') {
            iconColor = focused ? colors.primary : colors.gray4;
            return (
              <Image
                source={require('../Assets/icon/User_fill.png')}
                style={[styles.colorBottomIcon, { tintColor: iconColor }]}
                resizeMode={'contain'}
              />
            );
          }
        },
      })}
      activeColor={colors.primary}
      inactiveColor={colors.gray4}
      barStyle={{
        backgroundColor: colors.white,
      }}
      shifting={false}>
      <Tab.Screen
        name="BerandaAG"
        component={BerandaAG}
        options={{ title: 'Beranda' }}
      />
      <Tab.Screen
        name="KeranjangAG"
        component={KeranjangAG}
        options={{ title: 'Produk' }}
      />
      <Tab.Screen
        name="PesananAG"
        component={PesananAG}
        options={{ title: 'Pesanan' }}
      />
      <Tab.Screen name="AkunAG" component={AkunAG} options={{ title: 'Akun' }} />
    </Tab.Navigator>
  );
};

const MainNavigation = props => {
  return (
    <Stack.Navigator
      initialRouteName={props.token !== null ? (props.typeAkun === 'agent' ? 'BerandaAgen' : 'BerandaS') : 'Onboard'}>
      {/* // initialRouteName={'Onboard'}> */}
      {/* RiwayatReferral */}
      <Stack.Screen
        name="Onboard"
        component={Onboard}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="BerandaS"
        component={MainTabNavigation}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="Login" component={Login} options={styleHead('')} />
      <Stack.Screen
        name="Register"
        component={Register}
        options={styleHead('')}
      />
      <Stack.Screen
        name="ForgotPass"
        component={ForgotPass}
        options={styleHead('')}
      />
      <Stack.Screen
        name="CreatePass"
        component={CreatePass}
        options={styleHead('')}
      />
      <Stack.Screen
        name="Verif"
        component={Verif}
        options={styleHeadNoBack('Verifikasi')}
      />
      <Stack.Screen
        name="Success"
        component={Success}
        options={styleHead('Verifikasi')}
      />
      <Stack.Screen
        name="ListFlashSale"
        component={ListFlashSale}
        options={({ navigation }) => ({
          title: 'Flash Sale',
          headerTitleStyle: { fontSize: 14, color: colors.white },
          headerTintColor: colors.white,
          headerBackground: () => <Gradient />,
          headerRight: () => (
            <View style={{ marginRight: 16 }}>
              <MaterialIcons
                name="shopping-bag"
                size={24}
                color={colors.white}
                onPress={() => navigation.navigate('KeranjangSS')}
              />
              {countCart === 0 ? (
                false
              ) : (
                <Badge
                  value={countCart}
                  containerStyle={{ position: 'absolute', top: -5, left: 10 }}
                  badgeStyle={{
                    backgroundColor: colors.red,
                    borderColor: colors.white,
                    borderWidth: 1,
                  }}
                />
              )}
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="ListProduct"
        component={ListProduct}
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerLeft: () => (
            <View style={{ marginLeft: 16 }}>
              <AntDesign
                name="arrowleft"
                size={24}
                color={colors.white}
                onPress={() => navigation.navigate('BerandaSS')}
              />
            </View>
          ),
          headerTitle: () => (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                width: windowWidth - 110,
                backgroundColor: colors.white,
                height: 34,
                alignItems: 'center',
                paddingHorizontal: 16,
                borderRadius: 100,
              }}
              onPress={() => navigation.navigate('SearchProduct')}>
              <Ionicons
                name="search"
                size={18}
                color={colors.primary}
                style={{ marginRight: 8 }}
              />
              <Text style={{ color: colors.gray3, fontSize: 12 }}>
                Cari apa yang kamu mau...
              </Text>
            </TouchableOpacity>
          ),
          headerTintColor: colors.white,
          headerBackground: () => <Gradient />,
          headerRight: () => (
            <View style={{ marginRight: 16 }}>
              <MaterialIcons
                name="shopping-bag"
                size={24}
                color={colors.white}
                onPress={() => navigation.navigate('KeranjangSS')}
              />
              {countCart === 0 ? (
                false
              ) : (
                <Badge
                  value={countCart}
                  containerStyle={{ position: 'absolute', top: -5, left: 10 }}
                  badgeStyle={{
                    backgroundColor: colors.red,
                    borderColor: colors.white,
                    borderWidth: 1,
                  }}
                />
              )}
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Notifikasi"
        component={Notifikasi}
        options={styleHead('Notifikasi')}
      />
      <Stack.Screen
        name="DirectLogin"
        component={DirectLogin}
        options={styleHead('')}
      />
      <Stack.Screen
        name="SearchProduct"
        component={SearchProduct}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DtlProduct"
        component={DtlProduct}
        options={styleHead('Produk Detail')}
      />
      {/* pesanan */}
      <Stack.Screen
        name="DtlPesanan"
        component={DtlPesanan}
        options={styleHead('Detail Pesanan')}
      />
      <Stack.Screen
        name="Pembayaran"
        component={Pembayaran}
        options={styleHead('Pembayaran')}
      />
      <Stack.Screen
        name="BtlPesanan"
        component={BtlPesanan}
        options={styleHead('Batalkan Pesanan')}
      />
      <Stack.Screen
        name="PaymentWeb"
        component={PaymentWeb}
        options={styleHead('Internet Banking')}
      />
      <Stack.Screen
        name="TrackingPesanan"
        component={TrackingPesanan}
        options={styleHead('Informasi Pengiriman')}
      />
      {/* profile */}
      <Stack.Screen
        name="EditAkun"
        component={EditAkun}
        options={styleHead('Edit akun')}
      />
      <Stack.Screen
        name="KartuMember"
        component={KartuMember}
        options={styleHead('Kartu Member')}
      />
      {/* Tukar Point */}
      <Stack.Screen
        name="TukarPoint"
        component={TukarPoint}
        options={({ navigation }) => ({
          title: 'Tukar Poin',
          headerTitleStyle: { fontSize: 14 },
          headerTintColor: colors.white,
          headerBackground: () => <Gradient />,
          headerRight: () => (
            <View style={{ marginRight: 16 }}>
              <MaterialIcons
                name="history"
                size={24}
                color={colors.white}
                onPress={() => navigation.navigate('HistoryPenukaran')}
              />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="DtlTukarPoint"
        component={DtlTukarPoint}
        options={styleHead('Tukar Poin')}
      />
      <Stack.Screen
        name="HistoryPenukaran"
        component={HistoryPenukaran}
        options={styleHead('Histori Penukaran')}
      />
      <Stack.Screen
        name="Bantuan"
        component={Bantuan}
        options={styleHead('Bantuan')}
      />
      <Stack.Screen
        name="WishList"
        component={WishList}
        options={styleHead('Wishlist')}
      />
      <Stack.Screen
        name="Alamat"
        component={Alamat}
        options={styleHead('Alamat Saya')}
      />
      <Stack.Screen
        name="FormAlamat"
        component={FormAlamat}
        options={styleHead('Tambah Alamat')}
      // options={({navigation}) => ({
      //   headerLeft: () => (
      //     <View style={{marginLeft: 16}}>
      //       <AntDesign
      //         name="arrowleft"
      //         size={24}
      //         color={colors.white}
      //         onPress={() => navigation.navigate('Alamat')}
      //       />
      //     </View>
      //   ),
      //   title: 'Tambah Alamat',
      //   headerTitleStyle: {fontSize: 14, color: colors.white},
      //   headerTintColor: colors.white,
      //   headerBackground: () => <Gradient />,
      // })}
      />
      <Stack.Screen
        name="Maps"
        component={Maps}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="GoLogout"
        component={GoLogout}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkout}
        options={styleHead('Checkout')}
      />
      <Stack.Screen
        name="MetodePembayaran"
        component={MetodePembayaran}
        options={styleHead('Pilih Metode Pembayaran')}
      />
      <Stack.Screen
        name="Voucher"
        component={Voucher}
        options={styleHead('Pilih Voucher')}
      />
      {/* // new user Agent ========================================  */}
      <Stack.Screen name="Step1" component={Step1} options={styleHead('')} />
      <Stack.Screen name="Step2" component={Step2} options={styleHead('')} />
      <Stack.Screen name="Step3" component={Step3} options={styleHead('')} />
      <Stack.Screen
        name="PembayaranAgen"
        component={PembayaranAgen}
        options={styleHead('Pembayaran')}
      />
      <Stack.Screen
        name="BerandaAgen"
        component={MainAgenTabNavigation}
        options={{ headerShown: false }}
      />
      {/* keranjang agen */}
      {/* pesanan agen */}
      {/* profile agen */}
      <Stack.Screen
        name="KartuAgen"
        component={KartuAgen}
        options={styleHead('Kartu Member')}
      />
      {/* referal */}
      <Stack.Screen
        name="PoinReferral"
        component={PoinReferral}
        options={styleHead('Tukar Poin Referral')}
      />
      <Stack.Screen
        name="RiwayatReferral"
        component={RiwayatReferral}
        options={styleHead('Riwayat Tukar Poin Referral')}
      />
      <Stack.Screen
        name="PenarikanReferral"
        component={PenarikanReferral}
        options={styleHead('Penarikan Poin')}
      />
      <Stack.Screen
        name="HBelanjaGetAgen"
        component={HBelanjaGetAgen}
        options={styleHead('History Belanja Get Agen')}
      />
      <Stack.Screen
        name="DtlHBelanjaGetAgen"
        component={DtlHBelanjaGetAgen}
        options={styleHead('Detail History Belanja Get Agen')}
      />
      <Stack.Screen
        name="SuccessPoin"
        component={SuccessPoin}
        options={styleHead('Sukses')}
      />
      {/* poin agen */}
      <Stack.Screen
        name="PoinAgen"
        component={PoinAgen}
        options={styleHead('Tukar Poin')}
      />
      <Stack.Screen
        name="RiwayatPoinAgen"
        component={RiwayatPoinAgen}
        options={styleHead('History Penukaran')}
      />
      <Stack.Screen
        name="Rekening"
        component={Rekening}
        options={styleHead('Rekening Saya')}
      />
    </Stack.Navigator>
  );
};

const Routes = () => {
  // deep link
  const config = {
    screens: {
      Notifikasi: {
        path: 'Notifikasi',
      },
      DtlProduct: {
        path: 'DtlProduct/:id',
      },
      DtlTukarPoint: {
        path: 'DtlTukarPoint/:id',
      },
      DtlPesanan: {
        path: 'DtlPesanan/:data',
      },
      GoLogout: {
        path: 'GoLogout/:msg',
      },
    },
  };

  const linking = {
    // prefixes: ['https://www.sinindonesia.com', 'app://sinindo'],
    // prefixes: ['https://sinindonesia.co.id', 'app://sinparnter'],
    prefixes: ['https://sinindonesia.co.id', 'app://sinparnter'],
    config,
  };
  // deep link

  const [isLoading, setIsLoading] = useState(true);
  const [token, setToken] = useState(null);
  const [typeAkun, setTypeAkun] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);

    async function getToken() {
      const token = await AsyncStorage.getItem('api_token');
      const typeAkun = await AsyncStorage.getItem('type_akun');
      setToken(token);
      setTypeAkun(typeAkun);
    }
    getToken();
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer linking={linking}>
      <MainNavigation token={token} typeAkun={typeAkun} />
    </NavigationContainer>
  );
};

export default Routes;

const styles = StyleSheet.create({
  colorBottomIcon: { width: 24, height: 24 },
});
