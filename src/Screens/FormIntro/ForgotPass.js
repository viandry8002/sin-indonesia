import Axios from 'axios';
import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Alert,
} from 'react-native';
import {Input} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import {api, key} from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import SpinnerLoad from '../MultipleComponent/SpinnerLoad';

const ForgotPass = ({navigation}) => {
  const [load, setload] = useState(false);
  const [email, setEmail] = useState('');

  const onSubmit = () => {
    // setload(true);
    const form = {
      email: email,
    };
    Axios.post(`${api}/password/forgot`, form, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('success', res.data.message);
        res.data.success === true
          ? navigation.navigate('Verif', {
              type: 2,
              id: res.data.data.id,
              email: res.data.data.email,
            })
          : Alert.alert('Peringatan', res.data.message);
        // setload(false);
      })
      .catch(err => {
        // setMessage(err.response.data.message);
        Alert.alert('Peringatan', err.response.data.message);
        console.log(err);
        // setload(false);
      });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <SpinnerLoad loads={load} />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{x: -1, y: 0}}
        end={{x: 1, y: 0}}
        style={{flex: 1}}>
        <View style={styles.wrapHeader}>
          <Text style={styles.title}>Lupa Password</Text>
          <Text style={styles.subtitle}>
            Reset Sandi untuk membuat sandi baru
          </Text>
        </View>

        <View style={styles.wrapBody}>
          {/* Email */}
          <Input
            label={false}
            placeholder={'masukan email'}
            leftIcon={
              <View style={styles.leftIcon}>
                <MaterialCommunityIcons
                  name="email"
                  size={24}
                  color={colors.primary}
                />
              </View>
            }
            style={{fontSize: 12}}
            inputContainerStyle={{borderBottomWidth: 0}}
            containerStyle={[styles.inputContainer, {marginBottom: 16}]}
            value={email}
            onChangeText={value => setEmail(value)}
          />

          <BtnPrimary title={'Berikutnya'} pressed={() => onSubmit()} />
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default ForgotPass;

const styles = StyleSheet.create({
  wrapHeader: {marginHorizontal: 16, width: 160, height: 140},
  title: {fontWeight: 'bold', fontSize: 28, color: colors.white},
  subtitle: {color: colors.white, marginTop: 8, fontSize: 12},
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  leftIcon: {
    width: 48,
    height: 48,
    backgroundColor: colors.gray6,
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    marginTop: 16,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
});
