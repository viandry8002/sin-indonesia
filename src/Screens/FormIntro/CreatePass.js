import Axios from 'axios';
import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import {Input} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../Styles/Colors';
import {key, api} from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';

const CreatePass = ({navigation, route}) => {
  //   console.log('aaaz', route.params);
  const [form, setForm] = useState({
    user_id: route.params.id,
    password: '',
    password_confirmation: '',
  });
  const [show, setShow] = useState(true);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const onSubmit = () => {
    Axios.post(`${api}/password/reset`, form, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('success', res.data);
        res.data.success === true
          ? navigation.navigate('Success', {
              type: 2,
              title: 'Password Berhasil Direset',
              subtitle:
                'Password baru berhasil dibuat, anda bisa langsung login dengan password baru anda',
              token: null,
              id: null,
            })
          : Alert.alert('Peringatan', res.data.message);
      })
      .catch(err => {
        // setMessage(err.response.data.message);
        Alert.alert('Peringatan', err.response.data.message);
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{x: -1, y: 0}}
        end={{x: 1, y: 0}}
        style={{flex: 1}}>
        <View style={styles.wrapHeader}>
          <Text style={styles.title}>Buat </Text>
          <Text style={styles.title}>Password Baru</Text>
          <Text style={styles.subtitle}>Reset Sandi untuk membuat</Text>
          <Text style={[styles.subtitle, {marginTop: 0}]}>sandi baru</Text>
        </View>

        <View style={styles.wrapBody}>
          {/* Input */}
          <Text style={[styles.labelInput, {marginTop: 0}]}>Password</Text>
          <Input
            label={false}
            placeholder={'masukan password'}
            style={{fontSize: 12}}
            inputContainerStyle={styles.containInput}
            containerStyle={styles.inputContainer}
            value={form.password}
            onChangeText={value => onInputChange(value, 'password')}
            secureTextEntry={true}
          />

          <Text style={styles.labelInput}>Konfirmasi Password</Text>
          <Input
            label={false}
            placeholder={'masukan konfirmasi password'}
            style={{fontSize: 12}}
            inputContainerStyle={styles.containInput}
            containerStyle={styles.inputContainer}
            value={form.password_confirmation}
            onChangeText={value =>
              onInputChange(value, 'password_confirmation')
            }
            secureTextEntry={true}
          />

          {form.password === form.password_confirmation &&
          form.password.length > 0 &&
          form.password_confirmation.length > 0 ? (
            <TouchableOpacity
              style={{marginVertical: 16, alignSelf: 'flex-start'}}>
              <Text style={styles.textFooter}>Password sesuai</Text>
            </TouchableOpacity>
          ) : (
            <View style={{height: 16}} />
          )}

          <BtnPrimary title={'Berikutnya'} pressed={() => onSubmit()} />
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default CreatePass;

const styles = StyleSheet.create({
  wrapHeader: {marginHorizontal: 16, width: 200, height: 140},
  title: {fontWeight: 'bold', fontSize: 28, color: colors.white},
  subtitle: {color: colors.white, marginTop: 8, fontSize: 12},
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  containInput: {borderBottomWidth: 0},
  labelInput: {
    fontWeight: 'bold',
    color: colors.black,
    fontSize: 12,
    marginTop: 16,
  },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
  wrapFooter: {alignSelf: 'center', flexDirection: 'row', marginTop: 200},
  textFooter: {color: '#12B76A', fontSize: 12},
});
