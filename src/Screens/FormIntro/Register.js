import Axios from 'axios';
import React, { useState } from 'react';
import {
  Alert,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../Styles/Colors';
import { api, key } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import InputComp from '../MultipleComponent/InputComp';
import SpinnerLoad from '../MultipleComponent/SpinnerLoad';

const Register = ({ navigation }) => {
  const [load, setload] = useState(false);
  const [form, setForm] = useState({
    name: '',
    email: '',
    phone: '',
    password: '',
    password_confirmation: '',
  });
  const [status, setStatus] = useState(false);
  const [message, setMessage] = useState('');

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const flashMessage = () => {
    setStatus(true);
    setTimeout(() => {
      setStatus(false);
    }, 2000);
  };

  const onSubmit = () => {
    setload(true),
      Axios.post(`${api}/register`, form, {
        headers: {
          key: key,
        },
      })
        .then(res => {
          console.log('success', res.data);
          res.data.success
            ? navigation.navigate('Verif', {
              type: 1,
              id: res.data.data.id,
              email: res.data.data.email,
            })
            : (flashMessage(), setMessage(res.data.message));
          setload(false);
        })
        .catch(err => {
          flashMessage();
          Alert.alert('Peringatan', err.response.data.message);
          console.log(err);
          setload(false);
        })
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <SpinnerLoad loads={load} />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}>
        <View style={styles.wrapHeader}>
          <Text style={styles.title}>Buat Akun</Text>
          <Text style={styles.title}>Member Baru</Text>
          <Text style={styles.subtitle}>
            Buat akun Member baru untuk login ke aplikasi
          </Text>
        </View>

        <View style={styles.wrapBody}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* Input */}
            <InputComp
              text={'Nama Lengkap'}
              placeholder={'masukan nama'}
              value={form.name}
              onChange={value => onInputChange(value, 'name')}
            />

            <InputComp
              text={'E-Mail'}
              placeholder={'masukan email'}
              value={form.email}
              onChange={value => onInputChange(value, 'email')}
            />

            <InputComp
              text={'No. Telepon'}
              placeholder={'masukan telepon'}
              value={form.phone}
              tkeyboard={'numeric'}
              onChange={value => onInputChange(value, 'phone')}
            />

            <InputComp
              text={'Password'}
              placeholder={'masukan password'}
              value={form.password}
              onChange={value => onInputChange(value, 'password')}
              secure={true}
            />

            <InputComp
              text={'Konfirmasi Password'}
              placeholder={'masukan konfirmasi password'}
              value={form.password_confirmation}
              onChange={value => onInputChange(value, 'password_confirmation')}
              secure={true}
            />

            <View style={{ marginVertical: 16, alignSelf: 'flex-start' }}>
              {form.password === '' ? (
                false
              ) : form.password === form.password_confirmation ? (
                <Text style={styles.textFooter}>Password sesuai</Text>
              ) : (
                <Text style={styles.textFooter2}>Password tidak sesuai</Text>
              )}
            </View>

            {/* <BtnPrimary title={'Daftar'} pressed={() => navigation.navigate('Verif', 'Regist')} /> */}
            <BtnPrimary title={'Daftar'} pressed={() => onSubmit()} />
          </ScrollView>
          {status ? (
            <View style={styles.wrapAlert}>
              <Text style={styles.textAlert}>{message}</Text>
            </View>
          ) : (
            false
          )}
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  wrapHeader: { marginHorizontal: 16, width: 200, height: 140 },
  title: { fontWeight: 'bold', fontSize: 28, color: colors.white },
  subtitle: { color: colors.white, marginTop: 8, fontSize: 12 },
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  containInput: { borderBottomWidth: 0 },
  labelInput: {
    fontWeight: 'bold',
    color: colors.black,
    fontSize: 12,
    marginTop: 16,
  },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
  wrapFooter: { alignSelf: 'center', flexDirection: 'row', marginTop: 200 },
  textFooter: { color: '#12B76A', fontSize: 12 },
  textFooter2: { color: colors.red, fontSize: 12 },
  // message
  wrapAlert: {
    backgroundColor: colors.red,
    height: 30,
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 32,
    borderRadius: 7,
  },
  textAlert: { fontSize: 12, color: colors.white },
});
