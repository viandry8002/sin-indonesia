import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState, useRef } from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Input } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import { api, key } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import SpinnerLoad from '../MultipleComponent/SpinnerLoad';
import OneSignal from 'react-native-onesignal';
import BtnOutline from '../MultipleComponent/BtnOutline';
import Font from '../../Styles/Font';

const Login = ({ navigation }) => {
  const [load, setload] = useState(false);
  const [form, setForm] = useState({
    email: '',
    password: '',
  });
  const [status, setStatus] = useState(false);
  const [show, setShow] = useState(true);
  const [message, setMessage] = useState('');

  useEffect(() => {
    navigation.setOptions({ headerLeft: false });
  }, []);

  const saveToken = async (token, type) => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
        await AsyncStorage.setItem('type_akun', type);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const flashMessage = () => {
    setStatus(true);
    setTimeout(() => {
      setStatus(false);
    }, 2000);
  };

  const onSubmit = () => {
    setload(true);
    Axios.post(`${api}/login`, form, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        res.data.success === true
          ? (
            OneSignal.setAppId('8a32cd5b-0a76-4f8b-9e86-72a92c338bb2'),
            //app id
            OneSignal.setLogLevel(6, 0),
            OneSignal.setExternalUserId(res.data.data.id + '', results => {
              console.log(
                'Results of setting external user id ' + res.data.data.id,
              );
              console.log(results);
            }),
            saveToken(res.data.token, res.data.data.type),
            res.data.data.type === 'agent' ?
              navigation.reset({
                index: 0,
                routes: [{ name: 'BerandaAgen' }]
              }) : navigation.reset({
                index: 0,
                routes: [{ name: 'BerandaS' }]
              })

          )
          : (flashMessage(),
            setMessage(res.data.message),
            // console.log('aaazz', res.data),
            res.data.message === 'Akun belum diverifikasi'
              ? navigation.navigate('Verif', {
                type: 1,
                id: res.data.data.id,
                email: res.data.data.email,
              })
              : false);

        setload(false);
      })
      .catch(err => {
        flashMessage();
        setMessage(err.response.data.message);
        // Alert.alert('Peringatan', err.response.data.message)
        // console.log(err.response.data.message);
        setload(false);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'transparent'}
        translucent
        barStyle="light-content"
      />
      <SpinnerLoad loads={load} />
      <LinearGradient
        colors={['#3BA172', '#0B326D']}
        start={{ x: -1, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}>
        <View style={styles.wrapHeader}>
          <Text style={[Font.bold28, { color: colors.white }]}>Selamat Datang</Text>
          <Text style={[Font.regular12, { color: colors.white, marginTop: 8 }]}>
            Silahkan login ke aplikasi untuk melanjutkan
          </Text>
        </View>

        <View style={styles.wrapBody}>
          <Text style={Font.bold14}>Login</Text>

          {/* Email */}
          <Input
            label={false}
            placeholder={'masukan email'}
            leftIcon={
              <View style={styles.leftIcon}>
                <MaterialCommunityIcons
                  name="email"
                  size={24}
                  color={colors.primary}
                />
              </View>
            }
            style={{ fontSize: 12 }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={[
              styles.inputContainer,
              status ? { borderColor: colors.red } : false,
            ]}
            value={form.email}
            onChangeText={value => onInputChange(value, 'email')}
          />

          {/* Pass */}
          <Input
            label={false}
            placeholder={'masukan password'}
            leftIcon={
              <View style={styles.leftIcon}>
                <MaterialCommunityIcons
                  name="lock"
                  size={24}
                  color={colors.primary}
                />
              </View>
            }
            style={{ fontSize: 12 }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={[
              styles.inputContainer,
              status ? { borderColor: colors.red } : false,
            ]}
            rightIcon={
              <MaterialCommunityIcons
                onPress={() => setShow(!show)}
                name={show === true ? 'eye-off-outline' : 'eye-outline'}
                size={20}
                color={colors.black}
              />
            }
            secureTextEntry={show}
            value={form.password}
            onChangeText={value => onInputChange(value, 'password')}
          />

          <Text
            style={[
              styles.textFooter,
              { marginVertical: 16, alignSelf: 'flex-end' },
            ]}
            onPress={() => navigation.navigate('ForgotPass')}>
            Lupa Password?
          </Text>

          <BtnPrimary title={'Login'} pressed={() => onSubmit()} />
          {/* <BtnPrimary title={'Login'} pressed={() => navigation.navigate('BerandaAgen')} /> */}
          <Text style={[Font.regular12, {
            marginVertical: 16,
            alignSelf: 'center'
          }]}>Belum punya akun? </Text>
          <TouchableOpacity
            style={styles.TouchableStyle}
            onPress={() => navigation.navigate('Register')}>
            <Text style={[Font.semiBold14, { color: colors.primary }]}>Daftar Sebagai Member</Text>
          </TouchableOpacity>
          <View style={{ height: 16 }} />
          <TouchableOpacity
            style={[styles.TouchableStyle, { backgroundColor: colors.greenPrimary }]}
            onPress={() => navigation.navigate('Step1')}>
            <Text style={[Font.semiBold14, { color: colors.white }]}>Daftar Sebagai Agen</Text>
          </TouchableOpacity>
          {/* <BtnOutline
            title={'Daftar Sebagai Agen'}
            pressed={() => navigation.navigate('Step1')}
          /> */}

          {status ? (
            <View style={styles.wrapAlert}>
              <Text style={[Font.regular12, { color: colors.white }]}>{message}</Text>
            </View>
          ) : (
            false
          )}
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  wrapHeader: { marginHorizontal: 16, width: 160, height: 140 },
  wrapBody: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  leftIcon: {
    width: 48,
    height: 45,
    backgroundColor: colors.gray6,
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -3,
  },
  inputContainer: {
    marginTop: 16,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
  wrapFooter: { alignSelf: 'center', flexDirection: 'row', marginTop: 200 },
  textFooter: { color: colors.primary, fontWeight: 'bold', fontSize: 12 },
  TouchableStyle: {
    backgroundColor: colors.blueShading,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
  // message
  wrapAlert: {
    backgroundColor: colors.red,
    height: 30,
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 32,
    borderRadius: 7,
  },
});
