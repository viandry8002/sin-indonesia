import Axios from 'axios';
import React from 'react';
import {
    Image,
    Linking,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import colors from '../../Styles/Colors';
import { api, key } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';

const Bantuan = () => {
    const getContact = () => {
        Axios.get(`${api}/configuration`, {
            headers: {
                key: key,
            },
        })
            .then(res => {
                Linking.openURL('https://wa.me/' + (res.data.data.no_cs.includes("62") ? res.data.data.no_cs : res.data.data.no_cs.replace("08", "628")));
            })
            .catch(err => {
                console.log(err);
            });
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <View
                style={{ alignItems: 'center', justifyContent: 'center', marginTop: 80 }}>
                <Image
                    source={require('../../Assets/image/cs.png')}
                    style={{ width: 225, height: 200 }}
                />
                <Text
                    style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        color: colors.black,
                        marginVertical: 16,
                    }}>
                    Butuh Bantuan ? Hubungi Kami
                </Text>
                <Text
                    style={{
                        fontSize: 12,
                        color: colors.black,
                        width: 300,
                        marginBottom: 32,
                    }}>
                    Jika anda punya pertanyaan yang ingin di tanyakan, anda bisa tanyak ke
                    customer services SIN Indoensia
                </Text>
            </View>
            <View style={{ marginHorizontal: 62 }}>
                <BtnPrimary
                    title={'Hubungi Kami'}
                    pressed={() => {
                        getContact();
                    }}
                />
            </View>
        </SafeAreaView>
    );
};

export default Bantuan;

const styles = StyleSheet.create({});
