import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import {TextMask} from 'react-native-masked-text';
import colors from '../../../Styles/Colors';
import {api, key} from '../../../Variable';

const Index = ({navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [chose, setchose] = useState([]);
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState([]);
  const [menuCategory, setMenuCategory] = useState([]);
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // setRefreshing(true);
      // setTimeout(() => {
      getToken();
      //   setRefreshing(false);
      // }, 1000);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/order/count`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success
          ? (setMenuCategory(res.data.count), setchose(res.data.count[0]))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });

    changeStatus(token, 'unpaid');
  };

  const changeStatus = (auth, status) => {
    Axios.get(`${api}/order?status=${status ? status : chose.status}`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + (auth ? auth : tokens),
      },
    })
      .then(res => {
        // console.log(res)
        // getData(auth ? auth : tokens);
        setData(res.data.data);
        res.data.next_page_url === null
          ? (sethasScrolled(false), setPass(true))
          : setNextLink(res.data.next_page_url);
      })
      .catch(err => {
        console.log(err.response.data);
      });
    setRefreshing(false);
  };

  const onRefresh = () => {
    getData(tokens);
    setRefreshing(true);
    setTimeout(() => {
      changeStatus(tokens, chose.status);
    }, 2000);
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        // console.log(res);
        sethasScrolled(!hasScrolled),
          setData([...data, ...res.data.data]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const switchMenu = (status, items) => {
    setRefreshing(true);
    setTimeout(() => {
      changeStatus(null, status), setchose(items);
    }, 500);
  };

  const renderMenu = ({item}) => (
    <TouchableOpacity
      style={styles.wrapRenderMenu}
      onPress={() => {
        switchMenu(item.status, item);
      }}>
      <View style={styles.wrapRenderMenu1}>
        <Text
          style={[
            {fontSize: 12, marginRight: 8},
            chose.id === item.id
              ? {color: colors.primary}
              : {color: colors.black},
          ]}>
          {item.title}
        </Text>
        {item.count === 0 ? (
          <View style={[styles.wrapcount, {backgroundColor: colors.white}]} />
        ) : (
          <View style={styles.wrapcount}>
            <Text style={{fontSize: 11, color: colors.white}}>
              {item.count}
            </Text>
          </View>
        )}
      </View>
      {chose.id === item.id ? (
        <View
          style={{
            borderBottomColor: colors.primary,
            borderBottomWidth: 2,
          }}
        />
      ) : (
        <View
          style={{
            borderBottomColor: colors.blueShading,
            borderBottomWidth: 2,
          }}
        />
      )}
    </TouchableOpacity>
  );

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.wrapRenderItem}
      onPress={
        () =>
          navigation.navigate('DtlPesanan', {
            invoice: item.invoice_id,
            type: chose,
          })
        // {
        //   console.log('aaaaaa', {
        //     invoice: item.invoice_id,
        //     type: chose,
        //   });
        // }
      }>
      <View style={styles.wrapItemHeader}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/icon/Bag_fill.png')}
            style={styles.bagIcon}
          />
          <View>
            <Text style={styles.textBelanja}>Belanja</Text>
            <Text style={{fontSize: 11, color: colors.gray3}}>
              {moment(item.created_at).format('DD MMM YYYY')}
            </Text>
          </View>
        </View>
        <Text style={styles.fontPrimary}>{chose.title}</Text>
      </View>
      <View style={styles.horizontal} />
      <View style={styles.wrapItemBody}>
        <Image
          source={
            item.item.product_json.file
              ? {uri: item.item.product_json.file}
              : require('../../../Assets/image/notfound.png')
          }
          style={styles.product}
          resizeMode={'contain'}
        />
        <View style={{flex: 1}}>
          <Text style={{fontSize: 12, color: colors.black}}>
            {item.item.product_json.title}
          </Text>
          <Text style={{fontSize: 11, color: colors.black}}>
            {item.item.qty} x{' '}
            <TextMask
              type={'money'}
              options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp ',
                suffixUnit: '',
              }}
              value={item.item.product_json.price}
              numberOfLines={1}
            />{' '}
          </Text>
        </View>
      </View>
      {item.items_count - 1 === 0 ? (
        false
      ) : (
        <Text style={styles.fontPrimary}>
          + {item.items_count - 1} Produk Lainnya
        </Text>
      )}
      <View style={styles.horizontal} />
      <View style={styles.wrapItemFooter}>
        <View style={{flex: 1}}>
          <Text style={{fontSize: 12, color: colors.black}}>Total Pesanan</Text>
          {/* <Text style={{ color: colors.black, fontWeight: 'bold' }}>{item.item.subtotal}</Text> */}
          <TextMask
            type={'money'}
            options={{
              precision: 0,
              separator: '.',
              delimiter: '.',
              unit: 'Rp ',
              suffixUnit: '',
            }}
            value={item.subtotal}
            style={{color: colors.black, fontWeight: 'bold'}}
            numberOfLines={1}
          />
        </View>
        {item.status === 'waiting' && item.status_payment === 'unpaid' ? (
          <TouchableOpacity
            style={styles.TouchableStyle}
            onPress={() =>
              item.payment_request.type === 'internet_banking'
                ? navigation.navigate(
                    'PaymentWeb',
                    item.payment_response.redirect_url,
                  )
                : navigation.navigate('Pembayaran', item.invoice_id)
            }>
            <Text style={{color: colors.white, fontWeight: 'bold'}}>
              Bayar Sekarang
            </Text>
          </TouchableOpacity>
        ) : (
          false
        )}
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{height: 58}}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={menuCategory}
          renderItem={renderMenu}
          refreshing={refreshing}
        />
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        renderItem={renderItem}
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{alignItems: 'center'}}>
              <Text style={{color: colors.black}}>Load more ....</Text>
            </View>
          ) : (
            <View style={{height: 130}} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  fontPrimary: {fontSize: 12, color: colors.primary},
  TouchableStyle: {
    backgroundColor: colors.primary,
    width: 110,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },

  // renderMenu
  wrapRenderMenu: {flex: 1, backgroundColor: colors.white, height: 50},
  wrapRenderMenu1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  wrapcount: {
    width: 18,
    height: 18,
    backgroundColor: colors.primary,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // renderItem
  wrapRenderItem: {
    flex: 1,
    marginHorizontal: 8,
    marginVertical: 4,
    borderRadius: 7,
    backgroundColor: colors.white,
    padding: 8,
  },
  wrapItemHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bagIcon: {tintColor: colors.primary, width: 24, height: 24, marginRight: 4},
  textBelanja: {fontSize: 12, color: colors.black, fontWeight: 'bold'},

  horizontal: {
    borderBottomColor: 'rgba(0, 0, 0, 0.07)',
    borderBottomWidth: 1,
    marginVertical: 8,
  },
  wrapItemBody: {flexDirection: 'row', flex: 1, marginBottom: 10},
  product: {width: 56, height: 56, borderRadius: 7, marginRight: 8},
  wrapItemFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
