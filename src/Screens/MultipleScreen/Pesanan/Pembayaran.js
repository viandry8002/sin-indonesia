import AsyncStorage from '@react-native-async-storage/async-storage';
import Clipboard from '@react-native-community/clipboard';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import FormatTime from '../../MultipleComponent/FormatTime';
import Horizontal from '../../MultipleComponent/Horizontal';
import Font from '../../../Styles/Font';
import Accordion from 'react-native-collapsible/Accordion';
import HTML from 'react-native-render-html';
import CountDown from 'react-native-countdown-component';

const windowDimensions = Dimensions.get('window');

const SECTIONS = [
  {
    title: 'Mobile Banking BCA',
    content: `1. Login ke BCA Mobile
        2. Pilih transfer antar rekening BCA
        3. Masukan nomor virtual account BCA anda
        4. Masukan nominal sesuai tagihan
        5. Ikuti instruksi untuk menyelesaikan transaksi`,
  },
  {
    title: 'SMS Banking BCA',
    content: 'Lorem ipsum... 2',
  },
  {
    title: 'ATM BCA',
    content: 'Lorem ipsum... 3',
  },
  {
    title: 'Internet Banking BCA',
    content: 'Lorem ipsum... 3',
  },
];

const Pembayaran = ({ route, navigation }) => {
  const [activeSections, setActiveSections] = useState([0]);
  const [tokens, setTokens] = useState(null);
  // const [data, setData] = useState([]);
  const [dtPRequest, setdtPRequest] = useState([]);
  const [dtPResponse, setdtPResponse] = useState([]);
  const [dtPResponseData, setdtPResponseData] = useState([]);
  const [instructions, setInstructions] = useState([]);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', () => {
    getToken();
    // let timer = setInterval(() => {
    //   FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
    //     ? clearInterval(timer)
    //     : setdatess(moment(endDate).diff(moment(), 'seconds'));
    // }, 1000);
    // });
    // return unsubscribe;
    // }, [endDate]);
  }, []);

  let param;
  route.params.data
    ? (param = JSON.parse(base64.decode(route.params.data)))
    : (param = route.params);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/order/get/${param}`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        console.log(
          'aaaa',
          res.data.data.payment_response
        );
        res.data.success
          ? // setData(res.data.data),
          (setdtPRequest(res.data.data.payment_request),
            setdtPResponse(res.data.data.payment_response),
            setdtPResponseData(res.data.data.payment_response.data),
            setInstructions(res.data.data.payment_response.data.instructions),
            setendDate(res.data.data.payment_response.transaction_expired_at))
          // res.data.data.payment_response.payment_type === 'bank_transfer' ?
          //   setdtRek(res.data.data.payment_response.va_numbers[0]) : setdtRek(res.data.data.payment_response))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const copyToClipboard = (text, type) => {
    // console.log('kokook', text);
    Clipboard.setString(text.toString());
    ToastAndroid.show(type + ' telah di salin', ToastAndroid.SHORT);
  };

  const _renderHeader = section => {
    return (
      <View style={styles.wrapAccorHeader}>
        <Text style={{ fontSize: 12, color: colors.black }}>{section.title}</Text>
        <Ionicons name="chevron-down" size={18} color={colors.black} />
      </View>
    );
  };

  const _renderContent = section => {
    return (
      <View>
        {
          section.steps.map((x) =>
            <HTML
              contentWidth={windowDimensions.width}
              source={{ html: x }}
              allowedStyles={['color', 'fontFamily']}
              baseStyle={{ color: colors.fullGray }}
              ignoredStyles={['color', 'fontFamily']}
              tagsStyles={{
                p: { color: colors.black, },
                ul: { color: colors.black, },
                ol: { color: colors.black, }
              }}
            />)
        }

      </View>
    );
  };

  const Copy = ({ text, type }) => (
    <TouchableOpacity
      style={styles.wrapCopy}
      onPress={() => copyToClipboard(text, type)}>
      <MaterialCommunityIcons
        name="content-copy"
        size={16}
        color={colors.primary}
      />
      <Text style={{ fontSize: 11, color: colors.primary }}>Salin</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* header */}
        <View style={{ backgroundColor: colors.white, padding: 16 }}>
          <Text
            style={{ fontWeight: 'bold', color: colors.black, marginBottom: 8 }}>
            Cara Pembayaran
          </Text>
          <Text style={styles.font12margin}>
            Transfer sesuai nominal di bawah ini:
          </Text>
          <View style={styles.wrapHeader}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                source={{ uri: dtPRequest.file }}
                style={{ width: 48, height: 20, marginRight: 8 }}
                resizeMode={'contain'}
              />
              <Text style={Font.regular14} >{dtPRequest.title}</Text>
            </View>
            <Horizontal />
            <View style={styles.rowSpace}>
              <View>
                <Text style={styles.font10}>Nominal</Text>
                {/* <Text style={styles.font16Primary}>Rp295.023</Text> */}
                <FormatMoney style={styles.font16Primary}
                  value={parseInt(dtPResponseData.amount)} />
                {/* value={dtPResponse.gross_amount.replace('.00', '')} /> */}
                {/* <Text style={styles.font11}>3 angka dibelakang adalah angka unik</Text> */}
              </View>
              <Copy text={parseInt(dtPResponseData.amount)} type={'Nominal'} />
            </View>
            <Horizontal />
            <View style={styles.rowSpace}>
              <View>
                <Text style={styles.font10}>No Rekening</Text>
                {/* {dtPResponse.payment_type === 'cstore' ? (
                  <Text style={styles.font16Primary}>
                    {dtPResponse.payment_code}
                  </Text>
                ) : dtPResponse.payment_type === 'bank_transfer' ? (
                  <Text style={styles.font16Primary}>{dtRek.va_number}</Text>
                ) : <Text style={styles.font16Primary}>{dtRek.permata_va_number}</Text>} */}
                <Text style={styles.font16Primary}>
                  {dtPResponseData.pay_code}
                </Text>
              </View>
              {/* <Copy text={dtPResponse.payment_type === 'cstore' ? dtPResponse.payment_code : dtPResponse.payment_type === 'bank_transfer' ? dtRek.va_number : dtRek.permata_va_number} type={'No Rekening'} /> */}
              <Copy text={dtPResponseData.pay_code} type={'No Rekening'} />
            </View>
          </View>
        </View>

        {/* body */}
        <View style={styles.wrapBody}>
          <Text style={styles.font12margin}>
            Transfer sebelum{' '}
            <Text style={{ fontWeight: 'bold' }}>
              {moment(dtPResponse.transaction_expired_at).format(
                'DD MMM YYYY hh:mm',
              )}{' '}
              WIB
            </Text>{' '}
            atau pesanan kamu akan batal otomatis oleh sistem
          </Text>
          {/* <Text
            style={{
              fontSize: 24,
              fontWeight: 'bold',
              color: colors.red,
              textAlign: 'center',
              marginBottom: 8,
            }}>
            {FormatTime(moment(endDate).diff(moment(), 'seconds'))}
          </Text> */}
          {/* <CountDown
            size={24}
            until={86400}
            onFinish={() => alert('Finished')}
            digitStyle={{ backgroundColor: '#FFF', width: 30, top: 3 }}
            digitTxtStyle={{
              fontWeight: 'bold',
              color: colors.red,
            }}
            timeLabelStyle={{ color: 'red', fontWeight: 'bold' }}
            separatorStyle={{ color: colors.red }}
            timeToShow={['H', 'M', 'S']}
            timeLabels={{ m: null, s: null }}
            showSeparator
          /> */}
          <Text style={styles.font12margin}>
            Transfer akan diverifikasi 1x24 jam
          </Text>

          {/* <Text style={styles.font12margin}>{dtPRequest?.description}</Text> */}
          {/* accordion */}
          <Accordion
            sections={instructions}
            activeSections={activeSections}
            renderHeader={_renderHeader}
            renderContent={_renderContent}
            sectionContainerStyle={{ borderWidth: 1, borderColor: colors.grayShading, marginBottom: 8, borderRadius: 7, padding: 8 }}
            onChange={(activeSections) => setActiveSections(activeSections)}
          />
        </View>
      </ScrollView>

      {/* <View style={styles.wrapFooter}>
        <BtnPrimary title={'Upload Bukti Transfer'} />
      </View> */}
    </SafeAreaView>
  );
};

export default Pembayaran;

const styles = StyleSheet.create({
  wrapHeader: {
    borderWidth: 1,
    borderColor: colors.grayShading,
    borderRadius: 7,
    paddingHorizontal: 8,
    paddingVertical: 16,
  },
  font12margin: { fontSize: 12, color: colors.black, marginBottom: 8 },
  wrapCopy: {
    flexDirection: 'row',
    padding: 8,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 7,
    width: 72,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowSpace: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  font10: { fontSize: 10 },
  font11: { fontSize: 11 },
  font16Primary: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },

  wrapBody: { backgroundColor: colors.white, padding: 16, marginTop: 8 },
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'center',
  },

  wrapAccorHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    paddingVertical: 8,
  },
});
