import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import colors from '../../../Styles/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Input} from 'react-native-elements';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import moment from 'moment';
import {api, key} from '../../../Variable';

const Pilihan = [
  {
    id: 1,
    title: 'Ingin mengubah pesanan',
    check: false,
  },
  {
    id: 2,
    title: 'Berubah pikiran',
    check: false,
  },
  {
    id: 3,
    title: 'Salah pesan',
    check: false,
  },
  {
    id: 4,
    title: 'Isi Sendiri',
    check: false,
  },
];
const BtlPesanan = ({route, navigation}) => {
  const [check, setCheck] = useState({
    id: 0,
    title: '',
    check: false,
  });
  const [message, setMessage] = useState('');
  const [tokens, setTokens] = useState(null);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    setTokens(token);
  };

  useEffect(() => {
    getToken();
  }, []);

  const postCancel = () => {
    const body = {
      invoice_id: route.params,
      alasan: check.id === 4 ? message : check.title,
    };

    // console.log('gg', body)
    Axios.post(`${api}/order/cancel`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (ToastAndroid.show(
              'Berhasil membatalkan pesanan',
              ToastAndroid.SHORT,
            ),
            navigation.navigate('DtlPesanan', {
              invoice: route.params,
              type: {
                id: 5,
                title: 'Dibatalkan',
                status: 'cancel',
                count: 1,
              },
            }))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity style={styles.wrapRender} onPress={() => setCheck(item)}>
      {item.id === check.id ? (
        <MaterialCommunityIcons
          name="radiobox-marked"
          size={24}
          color={colors.primary}
        />
      ) : (
        <MaterialCommunityIcons
          name="radiobox-blank"
          size={24}
          color={colors.gray3}
        />
      )}
      <Text style={styles.titleRender}>{item.title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={{backgroundColor: colors.white, padding: 16}}>
          <FlatList data={Pilihan} renderItem={renderItem} />
          {check.id === 4 ? (
            <Input
              label={false}
              style={{fontSize: 12}}
              inputContainerStyle={{borderBottomColor: colors.grayShading}}
              value={message}
              onChangeText={value => setMessage(value)}
            />
          ) : (
            false
          )}
        </View>
      </View>
      <View style={styles.wrapFooter}>
        <BtnPrimary title={'Lanjut'} pressed={() => postCancel()} />
      </View>
    </SafeAreaView>
  );
};

export default BtlPesanan;

const styles = StyleSheet.create({
  wrapRender: {flexDirection: 'row', alignItems: 'center', marginBottom: 8},
  titleRender: {marginLeft: 10, fontSize: 12, color: colors.black},
  wrapFooter: {
    height: 66,
    backgroundColor: colors.white,
    elevation: 6,
    padding: 16,
    justifyContent: 'center',
  },
});
