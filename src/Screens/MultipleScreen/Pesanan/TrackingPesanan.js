import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Alert,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import colors from '../../../Styles/Colors';
import StepIndicator from 'react-native-step-indicator';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import { api, key } from '../../../Variable';

const windowWidth = Dimensions.get('window').width;

const dumyTracking = {
  errors: [],
  data: {
    outbounds: [
      {
        city_name: 'Terima permintaan pick up dari [Tokopedia]',
        outbound_date: '2022-10-25 09:03',
        outbound_code: 'PICKREQ',
        outbound_description: 'Terima permintaan pick up dari [Tokopedia]',
      },
      {
        city_name: 'Paket telah di pick up oleh [SIGESIT - ]',
        outbound_date: '2022-10-25 11:39',
        outbound_code: 'PICK',
        outbound_description: 'Paket telah di pick up oleh [SIGESIT - ]',
      },
      {
        city_name:
          'Paket telah di input (manifested) di Jakarta Selatan [Jaksel Pancoran]',
        outbound_date: '2022-10-25 15:04',
        outbound_code: 'IN',
        outbound_description:
          'Paket telah di input (manifested) di Jakarta Selatan [Jaksel Pancoran]',
      },
    ],
    summary: {
      courier_code: 'sicepat',
      courier_name: 'SiCepat Ekspres',
      tracking_number: '003098284542',
      service_code: 'BEST',
      service_name: 'BEST',
      service_description: '',
      ship_date: '2022-10-25 22:04',
      delivery_date: '',
      shipper_name: 'NutriMart',
      receiver_name: 'Ferdi',
      origin: 'CGK10000',
      destination: 'BOO20120',
      status: 'IN',
    },
    details: {
      tracking_number: '003098284542',
      ship_date: '2022-10-25 22:04',
      ship_time: '',
      weight: 1,
      origin: 'CGK10000',
      destination: 'BOO20120',
      shipper_name: 'NutriMart',
      shipper_address1: 'DKI Jakarta',
      shipper_address2: '',
      shipper_address3: '',
      shipper_city: '',
      receiver_name: 'Ferdi',
      receiver_address1: 'Caringin, Kab. Bogor',
      receiver_address2: '',
      receiver_address3: '',
      receiver_city: '',
    },
    delivery_status: {
      status: 'IN',
      pod_receiver: '',
      pod_date: '',
      pod_time: '',
    },
    mobile_status: {
      status: 5,
      name: 'Pesanan Diproses',
    },
  },
};
const TrackingPesanan = ({ route, navigation }) => {
  const [currentPosition, setcurrentPosition] = useState(0);
  const [data, setData] = useState([]);
  const [dataSummary, setdataSummary] = useState([]);
  const [dataTracking, setDataTracking] = useState([{}]);

  // console.log('param', route.params);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('api_token');
    getData(token);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getToken();
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    Axios.get(`${api}/order/tracking/?id=${route.params}`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        // console.log('resss', res.data);
        res.data.status && res.data.data != null ? (
          (setData(res.data.data),
            setdataSummary(res.data.data.summary),
            setDataTracking(res.data.data.outbounds))
        ) : (
          Alert.alert('Peringatan', 'Data tidak ditemukan')
        );
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <ScrollView>
        <View
          style={{
            padding: 16,
            backgroundColor: colors.white,
          }}>
          <View style={styles.marbot}>
            <Text style={styles.font12}>Nomor Resi</Text>
            <Text style={styles.font12Bold}>{data.tracking_number}</Text>
          </View>
          <View style={styles.wrap2row}>
            <View style={{ flex: 1 }}>
              <Text style={styles.font12}>Tanggal Pengiriman</Text>
              <Text style={styles.font12Bold}>
                {moment(data.ship_date).format('DD MMM YYYY')}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={styles.font12}>Service Code</Text>
              <Text style={styles.font12Bold}>{dataSummary.courier_code}</Text>
            </View>
          </View>
          <View style={styles.wrap2row}>
            <View style={{ flex: 1 }}>
              <Text style={styles.font12}>Penjual</Text>
              <Text style={styles.font12Bold}>{data.shipper_name}</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={styles.font12}>Pembeli</Text>
              <Text style={styles.font12Bold}>{data.receiver_name}</Text>
            </View>
          </View>
          {
            dataSummary?.courier_code === "gosend" ?
              <View style={styles.wrap2row}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.font12}>Note</Text>
                  <Text style={styles.font12Bold}>{dataSummary.status_note}</Text>
                </View>
              </View>
              : false
          }
          {/* <View style={styles.marbot}>
            <Text style={styles.font12}>Estimasi tiba</Text>
            <Text style={styles.font12Bold}>27 Oktober 2022</Text>
          </View> */}
        </View>
        <View style={{ height: 8, backgroundColor: colors.gray6 }} />
        <View
          style={{
            flex: 1,
            padding: 16,
            backgroundColor: colors.white,
          }}>
          <Text style={styles.font14Bold}>Informasi Pengiriman</Text>
          <StepIndicator
            stepCount={dataTracking.length}
            customStyles={styles.customIndicator}
            currentPosition={currentPosition}
            direction="vertical"
            renderLabel={({ label }) => (
              <View
                style={{
                  flex: 1,
                  paddingLeft: 16,
                  width: windowWidth - 70,
                  marginTop: 16,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 4,
                  }}>
                  <Text
                    style={[
                      styles.font14Bold,
                      { color: colors.primary, flex: 1 },
                    ]}>
                    {
                      dataSummary.courier_code === "gosend" ? label.outbound_code : label.city_name
                    }
                  </Text>
                  <Text style={[styles.font12, { flex: 1, textAlign: 'right' }]}>
                    {moment(label.outbound_date).format('DD MMM YYYY hh:mm')} WIB
                  </Text>
                </View>
                <Text style={[styles.font12, { flex: 1 }]} numberOfLines={3}>
                  {
                    label.outbound_description
                  }
                </Text>
              </View>
            )}
            labels={dataTracking}
          />

        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TrackingPesanan;

const styles = StyleSheet.create({
  font12: { fontSize: 12 },
  font12Bold: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
  font14Bold: { fontSize: 14, color: colors.black, fontWeight: 'bold' },
  marbot: { marginBottom: 10 },
  wrap2row: { flexDirection: 'row', marginBottom: 10 },
  customIndicator: {
    stepIndicatorSize: 20,
    currentStepIndicatorSize: 20,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: colors.gray5,
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#7eaec4',
    stepStrokeUnFinishedColor: '#dedede',
    separatorFinishedColor: '#7eaec4',
    separatorUnFinishedColor: '#dedede',
    stepIndicatorFinishedColor: '#7eaec4',
    stepIndicatorUnFinishedColor: '#7eaec4',
    stepIndicatorCurrentColor: colors.primary,
    stepIndicatorLabelFontSize: 0,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#999999',
    labelSize: 1,
    currentStepLabelColor: '#7eaec4',
  },
});
