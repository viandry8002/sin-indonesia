import AsyncStorage from '@react-native-async-storage/async-storage';
import Clipboard from '@react-native-community/clipboard';
import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
    Alert,
    FlatList,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View,
} from 'react-native';
import base64 from 'react-native-base64';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import FormatMoney from '../../MultipleComponent/FormatMoney';
import Horizontal from '../../MultipleComponent/Horizontal';
import Font from '../../../Styles/Font';
import { useBackHandler } from '@react-native-community/hooks';

const DtlPesanan = ({ route, navigation }) => {
    const [tokens, setTokens] = useState(null);
    const [data, setData] = useState([]);
    const [dataAddress, setDataAddress] = useState('');
    const [subdistrictAddress, setSubdistrictAddress] = useState('');
    const [cityAddress, setCityAddress] = useState('');
    const [provinceAddress, setProvinceAddress] = useState('');
    const [dataPayment, setDataPayment] = useState('');
    const [dataPaymentRes, setDataPaymentRes] = useState('');
    const [dataShipment, setDataShipment] = useState('');
    const [listProduct, setListProduct] = useState([]);
    const [shipdeoTrackingInfo, setShipdeoTrackInfo] = useState('');
    const [typeAkun, setTypeAkun] = useState('');

    const backAction = () => {
        navigation.navigate('PesananAG')
        return true;
    };

    useBackHandler(backAction)

    const getToken = async () => {
        const token = await AsyncStorage.getItem('api_token');
        const type = await AsyncStorage.getItem('type_akun');
        getData(token);
        setTypeAkun(type);
    };

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getToken();
        });
        return unsubscribe;
    }, []);

    let param;

    // route.params.data
    //     ? (param = JSON.parse(base64.decode(route.params.data)))
    //     : (param = route.params);

    route.params.data
        ? (param = JSON.parse(route.params.data))
        : (param = route.params);

    const getData = token => {
        setTokens(token);
        Axios.get(`${api}/order/get/${param.invoice}`, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                res.data.success
                    ? (setData(res.data.data),
                        setDataAddress(res.data.data.address),
                        setSubdistrictAddress(res.data.data.address.subdistrict_id),
                        setCityAddress(res.data.data.address.city_id),
                        setProvinceAddress(res.data.data.address.province_id),
                        setDataPayment(res.data.data.payment_request),
                        setDataPaymentRes(res.data.data.payment_response),
                        setListProduct(res.data.data.items),
                        setDataShipment(res.data.data.shipment_request),
                        setShipdeoTrackInfo(res.data.data.shipdeo.tracking_info))
                    : navigation.navigate('GoLogout', res.data?.message);
            })
            .catch(err => {
                console.log(err);
            });
    };

    const postDone = () => {
        const body = {
            invoice_id: data.invoice_id,
        };

        // console.log('gg', body)
        Axios.post(`${api}/order/done`, body, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + tokens,
            },
        })
            .then(res => {
                console.log('aa', res.data);
                res.data.success
                    ? (ToastAndroid.show(
                        'Berhasil menyelesaikan pesanan',
                        ToastAndroid.SHORT,
                    ),
                        getData(tokens, data.invoice_id))
                    : Alert.alert('Peringatan', res.data.message);
            })
            .catch(err => {
                console.log(err);
            });
    };

    const copyToClipboard = (text, type) => {
        Clipboard.setString(text);
        ToastAndroid.show(type + ' telah di salin', ToastAndroid.SHORT);
    };

    const renderProduct = ({ item }) => (
        <View style={styles.wrapRender}>
            <Image
                source={
                    item.code === undefined || item.code === null ?
                        item.product_json.file
                            ? { uri: item.product_json.file }
                            : require('../../../Assets/image/notfound.png')
                        : require('../../../Assets/image/logo1.png')
                }
                style={styles.imgRender}
                resizeMode={'contain'}
            />
            <View style={[styles.wrapContent, { flex: 1 }]}>
                <Text style={styles.itemTitle}>{
                    item.code === undefined || item.code === null ?
                        item.product_json.title : item.code
                }</Text>
                <Text style={styles.itemPrice}>
                    {item.qty} x{' '}
                    {/* <FormatMoney value={item.product_json.price} /> */}
                    {
                        typeAkun === 'agent' ? <FormatMoney value={item.product_json.price_agent} /> :
                            <FormatMoney value={item.product_json.price} />
                    }
                </Text>
                {/* <View style={styles.wrapCategory}>
                    <Text style={styles.TextCategory}>Kecil</Text>
                </View> */}
            </View>
            <View style={styles.wrapContent}>
                {
                    typeAkun === 'agent' ? <FormatMoney value={item.qty * item.product_json.price_agent}
                        style={styles.itemPrice2} /> : <FormatMoney value={item.qty * item.product_json.price}
                            style={styles.itemPrice2} />}
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 8 }}>
                    <Image
                        source={require('../../../Assets/icon/poinBelanja.png')}
                        style={{ width: 24, height: 24, marginRight: 4 }}
                    />
                    {
                        typeAkun === 'agent' ?
                            <FormatMoney value={item.qty * item.product_json.poin_agent} style={Font.regular12} unit={'+'} /> :
                            <FormatMoney value={item.qty * item.product_json.poin} style={Font.regular12} unit={'+'} />}
                    {/* <FormatMoney value={item.product_json?.total_point} style={Font.regular12} unit={'+'} /> */}
                </View>

            </View>
        </View>
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
                {/* status */}
                <View style={{ padding: 16, backgroundColor: colors.white }}>
                    <View style={styles.rowSpace}>
                        <Text style={styles.font12}>Status Pemesanan</Text>
                        <Text style={styles.textStatus}>{param.type.title}</Text>
                    </View>
                    <Horizontal />
                    {data.status === 'waiting' && data.status === 'cancel' ? (
                        false
                    ) : (
                        <View style={[styles.rowSpace, { marginBottom: 16 }]}>
                            <Text style={styles.font12}>{data.invoice_id}</Text>
                            {/* <Text style={styles.textStatus}>Lihat Invoice</Text> */}
                        </View>
                    )}
                    <View style={styles.rowSpace}>
                        <Text style={styles.font12}>Tanggal Pembelian</Text>
                        <Text style={{ fontSize: 12, color: colors.black }}>
                            {moment(data.created_at).format('DD MMMM YYYY, hh:mm')}
                        </Text>
                    </View>
                    {
                        typeAkun != 'agent' ?
                            data.status === 'cancel' ? (
                                false
                            ) : data.poin > 0 ? (
                                <View style={styles.wrapPoin}>
                                    <Image
                                        source={require('../../../Assets/icon/poin.png')}
                                        style={styles.poin}
                                    />
                                    <Text style={{ fontSize: 12, color: colors.black }}>
                                        <FormatMoney value={data.poin} style={{ fontWeight: 'bold' }} unit={'+'} />{' '}
                                        yang akan didapatkan
                                    </Text>
                                </View>
                            ) : (
                                false
                            ) : false}
                </View>

                {/* pengiriman */}
                <View style={styles.wrapBodyContent}>
                    <Text style={styles.textBlack}>Pengiriman</Text>
                    <View style={{ flexDirection: 'row', marginTop: 16 }}>
                        <Ionicons
                            name="location-outline"
                            size={24}
                            color={colors.primary}
                            style={{ marginRight: 8 }}
                        />
                        <View style={{ flex: 1 }}>
                            <Text style={styles.itemPrice2}>
                                {dataAddress.recipient_name} ( {dataAddress.recipient_phone} )
                            </Text>
                            <Text style={styles.font12}>{dataAddress.address}, {subdistrictAddress.name}, {cityAddress.name}, {provinceAddress.name}</Text>
                        </View>
                    </View>
                    <View
                        style={{ flexDirection: 'row', marginTop: 16, alignItems: 'center' }}>
                        <MaterialCommunityIcons
                            name="truck-fast-outline"
                            size={24}
                            color={colors.primary}
                            style={{ marginRight: 8 }}
                        />
                        <View
                            style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.itemPrice2}>
                                    {
                                        data.payment_method === 'online' ?
                                            data.shipment_method_label === 'Ekspedisi'
                                                ? data.shipment_request.courier +
                                                // ' - ' +
                                                ' ' +
                                                data.shipment_request.duration
                                                // data.shipment_request.service
                                                : data.shipment_method_label
                                            : data.shipment_method_label
                                    }
                                </Text>
                                {(data.status_payment === 'paid' && data.status === 'send') ||
                                    (data.status_payment === 'paid' && data.status === 'done') ? (
                                    data.shipment_method_label === 'Ekspedisi' ? (
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.font12} numberOfLines={1}>
                                                No.Resi : {shipdeoTrackingInfo.airwaybill}{' '}
                                            </Text>
                                            <MaterialCommunityIcons
                                                name="content-copy"
                                                size={16}
                                                onPress={() =>
                                                    copyToClipboard(
                                                        shipdeoTrackingInfo.airwaybill,
                                                        'No Resi',
                                                    )
                                                }
                                            />
                                        </View>
                                    ) : (
                                        false
                                    )
                                ) : (
                                    false
                                )}
                            </View>
                            {(data.status_payment === 'paid' &&
                                data.status === 'send' &&
                                data.shipment_method_label === 'Ekspedisi') ||
                                (data.status_payment === 'paid' &&
                                    data.status === 'done' &&
                                    data.shipment_method_label === 'Ekspedisi') ? (
                                <TouchableOpacity
                                    style={styles.BtnOutline}
                                    onPress={() =>
                                        navigation.navigate('TrackingPesanan', data.id)
                                    }>
                                    <Text style={{ color: colors.primary, fontSize: 12 }}>
                                        Lacak Pengiriman
                                    </Text>
                                </TouchableOpacity>
                            ) : (
                                false
                            )}
                        </View>
                    </View>
                </View>

                {/* info produk */}
                <View style={styles.wrapBodyContent}>
                    <Text style={styles.textBlack}>Informasi Produk</Text>
                    <FlatList data={listProduct} renderItem={renderProduct} />
                    <Horizontal />
                    <View style={styles.rowSpace}>
                        <Text style={styles.font12}>Subtotal</Text>
                        <FormatMoney value={data.total_order}
                            style={styles.itemPrice2} />
                    </View>
                    {
                        typeAkun == 'agent' ?
                            data.poin > 0 ? (
                                <View style={styles.wrapPoin1}>
                                    <Text style={Font.regular12} >Poin Didapat</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image
                                            source={require('../../../Assets/icon/poinBelanja.png')}
                                            style={styles.poin}
                                        />
                                        <Text style={Font.semiBold12}>
                                            <FormatMoney value={data.poin} unit={'+'} /> Poin
                                        </Text>
                                    </View>
                                </View>
                            ) : (
                                false
                            ) : false}
                </View>

                {/* Metode Pembayaran */}
                <View style={styles.wrapBodyContent}>
                    <Text style={styles.textBlack}>Metode Pembayaran</Text>
                    <View style={{ flexDirection: 'row', marginTop: 16 }}>
                        <Image
                            source={{ uri: dataPayment.file }}
                            style={styles.bank}
                            resizeMode={'contain'}
                        />
                        <Text style={styles.itemTitle}>{dataPayment.title}</Text>
                    </View>
                </View>

                {/* Rincian Pesanan */}
                <View style={[styles.wrapBodyContent, { marginBottom: 8 }]}>
                    <Text style={styles.textBlack}>Rincian Pesanan</Text>
                    <View
                        style={[styles.rowSpaceMargin, { marginTop: 16, marginBottom: 8 }]}>
                        <Text style={styles.font12}>
                            Total Harga ({listProduct.length} barang)
                        </Text>
                        <FormatMoney value={data.total_order}
                            style={styles.itemTitle} />
                    </View>
                    <View style={styles.rowSpaceMargin}>
                        <Text style={styles.font12}>Ongkos Kirim</Text>
                        {
                            typeAkun === 'agent' ?
                                data.discount_ongkir != '0' ?
                                    <View style={{ flexDirection: 'row' }}>< FormatMoney value={dataShipment.price}
                                        style={[styles.itemTitle, { textDecorationLine: 'line-through', marginRight: 4 }]} />
                                        < FormatMoney value={dataShipment.price - data.discount_ongkir}
                                            style={[styles.itemTitle, { color: colors.success }]} />
                                    </View> : < FormatMoney value={dataShipment.price}
                                        style={styles.itemTitle} />
                                :
                                <FormatMoney value={dataShipment.price}
                                    style={styles.itemTitle} />
                        }
                    </View>
                    <View style={styles.rowSpaceMargin}>
                        <Text style={styles.font12}>Biaya Layanan</Text>
                        <FormatMoney value={dataPayment.fee}
                            style={styles.itemTitle} />
                    </View>
                    <Horizontal />
                    <View style={styles.rowSpaceMargin}>
                        <Text style={styles.textBlack}>Total Pesanan</Text>
                        <FormatMoney value={data.subtotal}
                            style={styles.textBlack} />
                    </View>
                </View>
            </ScrollView>

            {data.status_payment === 'unpaid' && data.status === 'waiting' ? (
                <View style={styles.wrapFooter}>
                    <TouchableOpacity
                        style={styles.footerBtn1}
                        onPress={() => navigation.navigate('BtlPesanan', data.invoice_id)}>
                        <Text style={{ color: colors.red }}>Batalkan Pesanan</Text>
                    </TouchableOpacity>
                    <View style={{ width: 16 }} />
                    {
                        data.payment_method === 'online' ?
                            <TouchableOpacity
                                style={styles.footerBtn2}
                                onPress={() =>
                                    dataPayment.type === 'internet_banking'
                                        ? navigation.navigate('PaymentWeb', dataPaymentRes.redirect_url)
                                        : navigation.navigate('Pembayaran', data.invoice_id)
                                }>
                                <Text style={{ color: colors.white }}>Bayar Sekarang</Text>
                            </TouchableOpacity>
                            : <TouchableOpacity
                                style={styles.footerBtn2}
                                onPress={() => navigation.navigate('Bantuan')}>
                                <Text style={{ color: colors.white }}>Hubungi admin</Text>
                            </TouchableOpacity>
                    }
                </View>
            ) : (
                false
            )
            }

            {
                data.is_send === 1 ? (
                    <View
                        style={{
                            backgroundColor: colors.white,
                            elevation: 6,
                            padding: 16,
                            justifyContent: 'center',
                        }}>
                        <Text style={[styles.itemTitle, { marginBottom: 8 }]}>*Jika dalam waktu 1 x 24 jam kiriman tidak di klik sampai maka kiriman dianggap sudah sampai.</Text>
                        <BtnPrimary title={'Pesanan Selesai'} pressed={() => postDone()} />
                    </View>
                ) : (
                    false
                )
            }
        </SafeAreaView >
    );
};

export default DtlPesanan;

const styles = StyleSheet.create({
    font12: { fontSize: 12, color: colors.gray3 },
    rowSpace: { flexDirection: 'row', justifyContent: 'space-between' },
    rowSpaceMargin: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
    },
    textBlack: { color: colors.black, fontWeight: 'bold' },

    textStatus: { fontSize: 12, color: colors.primary, fontWeight: 'bold' },
    poin: { width: 24, height: 24, marginRight: 8 },
    bank: { width: 48, height: 16, marginRight: 32 },

    wrapBodyContent: { padding: 16, backgroundColor: colors.white, marginTop: 8 },
    wrapPoin: {
        marginTop: 16,
        backgroundColor: colors.yellowShading,
        borderRadius: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 6,
        alignItems: 'center',
    },
    wrapPoin1: {
        marginTop: 16,
        backgroundColor: colors.yellowShading,
        borderRadius: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 6,
        alignItems: 'center',
    },

    // render
    wrapRender: { flex: 1, flexDirection: 'row', marginVertical: 8 },
    imgRender: { width: 56, height: 56, borderRadius: 15 },
    wrapContent: { marginLeft: 8, justifyContent: 'center' },
    itemTitle: { fontSize: 12, color: colors.black },
    itemPrice: { fontSize: 11, color: colors.black, marginVertical: 8 },
    itemPrice2: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
    wrapCategory: {
        flex: 1,
        backgroundColor: colors.blueShading,
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderRadius: 100,
        width: 70,
        alignItems: 'center',
    },
    TextCategory: { color: colors.primary, fontSize: 12 },

    // footer
    wrapFooter: {
        height: 66,
        backgroundColor: colors.white,
        elevation: 6,
        padding: 16,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    footerBtn1: {
        flex: 1,
        // width: 168,
        height: 38,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.red,
    },
    footerBtn2: {
        flex: 1,
        backgroundColor: colors.primary,
        // width: 168,
        height: 38,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    point: { width: 18, height: 18, marginRight: 4 },
    BtnOutline: {
        // flex: 1,
        // width: 168,
        paddingHorizontal: 12,
        height: 30,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.primary,
    },
});
