import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {WebView} from 'react-native-webview';

const PaymentWeb = ({route}) => {
  return (
    <View style={{flex: 1}}>
      <WebView source={{uri: route.params}} />
    </View>
  );
};

export default PaymentWeb;

const styles = StyleSheet.create({});
