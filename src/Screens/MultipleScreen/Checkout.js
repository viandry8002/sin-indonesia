import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import {
    FlatList,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ToastAndroid,
    RefreshControl,
    Alert,
    Linkingx
} from 'react-native';
import { Modalize } from 'react-native-modalize';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import { api, key, postAgentCheckout, postAgentCart } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import Horizontal from '../MultipleComponent/Horizontal';
import FormatMoney from '../MultipleComponent/FormatMoney';
import { useBackHandler } from '@react-native-community/hooks';
import Font from '../../Styles/Font';

const Checkout = ({ navigation, route }) => {
    const [refreshing, setRefreshing] = useState(false);
    const modalPengiriman = useRef(null);
    const modalDomisili = useRef(null);
    const [tokens, setTokens] = useState(null);
    const [dataProduct, setDataProduct] = useState([]);
    const [totalProduct, setTotalProduct] = useState([]);

    const [address, setAddress] = useState('');

    // const [delivery, setDelivery] = useState(null);
    const [shipment_method, setShipment_method] = useState(null);
    const [typeDelivery, setTypeDelivery] = useState(null);
    const [courier, setCourier] = useState(null);
    const [listCourier, setListCourier] = useState([]);
    const [typeAkun, setTypeAkun] = useState('');

    const backAction = () => {
        navigation.navigate('KeranjangAG')
        return true;
    };

    useBackHandler(backAction)

    const onOpen = modal => {
        modal.current?.open();
    };

    const onClose = modal => {
        modal === modalPengiriman ? setListCourier([]) : false;
        modal.current?.close();
    };

    const getToken = async () => {
        const token = await AsyncStorage.getItem('api_token');
        const typeAkun = await AsyncStorage.getItem('type_akun');
        getData(token, typeAkun);
        setTypeAkun(typeAkun);
    };

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setRefreshing(true);
            setTimeout(() => {
                route.params.type === 'editAlamat'
                    ? (setShipment_method(null),
                        setCourier(null),
                        (route.params.type = null))
                    : false;
                getToken();
            }, 100);
        });
        return unsubscribe;
    }, []);

    const getData = (token, typeAkunn) => {
        let body = { id: route.params.carts };
        // console.log('grgrgr', typeAkunn);
        setTokens(token);
        Axios.post(typeAkunn === 'agent' ? postAgentCart : `${api}/cart`, body, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                // console.log('hhhhh', res.data);
                res.data.success
                    ? (setDataProduct(res.data.data), setTotalProduct(res.data))
                    : navigation.navigate('GoLogout', res.data?.message);
            })
            .catch(err => {
                console.log(err);
            });

        Axios.get(`${api}/user-address/get/default`, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                setAddress(res.data.data);
                setRefreshing(false);
            })
            .catch(err => {
                console.log(err.response.data);
                setRefreshing(false);
            });
    };

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            getData(tokens, typeAkun);
        }, 1000);
    };

    const getCourier = cParam => {
        cParam === 'shipdeo-pricings'
            ? Axios.post(
                `${api}/courier/${cParam ? cParam : typeDelivery}`,
                {
                    cart: dataProduct,
                    user_address_id: address.id,
                },
                {
                    headers: {
                        key: key,
                        Authorization: 'Bearer ' + tokens,
                    },
                },
            )
                .then(res => {
                    console.log('popopopo', res.data);
                    res.data.status
                        ? (setTypeDelivery(cParam), setListCourier(res.data.data))
                        : navigation.navigate('GoLogout', res.data?.message);
                })
                .catch(err => {
                    setRefreshing(false);
                    console.log(err);
                })
            : Axios.get(`${api}/courier/${cParam ? cParam : typeDelivery}`, {
                headers: {
                    key: key,
                    Authorization: 'Bearer ' + tokens,
                },
            })
                .then(res => {
                    // console.log('popopopohhhh', res.data);
                    res.data.status
                        ? (setTypeDelivery(cParam), setListCourier(res.data.data))
                        : navigation.navigate('GoLogout', res.data?.message);
                })
                .catch(err => {
                    console.log(err);
                });
    };

    const postCheckout = () => {
        setRefreshing(true);
        let body = {
            cart_id: route.params.carts,
            address: address,
            shipment_method: shipment_method,
            shipment_request: courier,
            payment_request: route.params.payment,
            voucher_id:
                route.params.voucher === null ? null : route.params.voucher?.id,
        };
        // console.log('jj', body);
        address === null ?
            (Alert.alert('Peringatan', 'Alamat harus di isi terlebih dahulu'), setRefreshing(false)) :
            courier === null || courier === 'domisili' || courier === 'shipdeo-pricings' || courier === 'store-address' ?
                (Alert.alert('Peringatan', 'Kurir harus di isi terlebih dahulu'), setRefreshing(false)) :
                route.params.payment === null ?
                    (Alert.alert('Peringatan', 'Pembayaran harus di isi terlebih dahulu'), setRefreshing(false)) :
                    typeAkun === 'agent' ?
                        Axios.post(postAgentCheckout, body, {
                            headers: {
                                key: key,
                                Authorization: 'Bearer ' + tokens,
                            },
                        })
                            .then(res => {
                                console.log('ressss abischeckout', res.data);
                                setRefreshing(false);
                                res.data.code === 200
                                    ? (ToastAndroid.show(
                                        'Terimakasih telah berbelanja',
                                        ToastAndroid.SHORT,
                                    ),
                                        // res.data.data.payment_response.payment_type === "gopay" ?
                                        //     Linking.openURL(res.data.data.payment_response.actions[1].url)
                                        //     :
                                        // navigation.navigate('PesananAG')
                                        navigation.navigate('DtlPesanan', {
                                            invoice: res.data.data.invoice_id, type: {
                                                title: "Belum Bayar",
                                                status: "unpaid",
                                                type: null,
                                                id: null
                                            }
                                        })
                                    )
                                    : ToastAndroid.show(res.data.message, ToastAndroid.SHORT);
                            })
                            .catch(err => {
                                setRefreshing(false);
                                console.log(err.response.data);
                            }) :
                        Axios.post(`${api}/order/checkout`, body, {
                            headers: {
                                key: key,
                                Authorization: 'Bearer ' + tokens,
                            },
                        })
                            .then(res => {
                                setRefreshing(false);
                                res.data.code === 200
                                    ? (ToastAndroid.show(
                                        'Terimakasih telah berbelanja',
                                        ToastAndroid.SHORT,
                                    ),
                                        navigation.navigate('PesananSS')
                                    )
                                    : ToastAndroid.show(res.data.message, ToastAndroid.SHORT);
                            })
                            .catch(err => {
                                setRefreshing(false);
                                console.log(err.response);
                            });
    };

    const openModal = (open, close, method, couriers) => {
        onOpen(open), onClose(close), setShipment_method(method);
        getCourier(couriers);
    };

    const totalPesanan = () => {
        return route.params.voucher === null
            ? Number(totalProduct.price ?? 0) +
            Number(courier?.price ?? 0) +
            Number(route.params.payment?.fee ?? 0)
            : route.params.voucher?.type_diskon === 'nominal'
                ? Number(totalProduct.price ?? 0) +
                Number(courier?.price ?? 0) +
                Number(route.params.payment?.fee ?? 0) -
                Number(route.params.voucher?.total_diskon ?? 0)
                : Number(courier?.price ?? 0) +
                Number(route.params.payment?.fee ?? 0) +
                Number(totalProduct.price ?? 0) -
                (Number(totalProduct.voucher_price ?? 0) *
                    (Number(route.params.voucher?.total_diskon ?? 0) / 100) >
                    Number(route.params.voucher?.max_diskon ?? 0)
                    ? Number(route.params.voucher?.max_diskon ?? 0)
                    : Number(totalProduct.voucher_price ?? 0) *
                    (Number(route.params.voucher?.total_diskon ?? 0) / 100));
    };

    const totalPesanan1 = () => {
        return typeAkun === 'agent' ?
            totalProduct.price >= totalProduct.min_order_disc_ongkir_agent ?
                totalkurir() === 0 ? totalPesanan() - courier?.price : totalPesanan() - courier?.price + totalkurir() : totalPesanan()
            : totalPesanan()
    };

    const totalkurir = () => {
        return Number(courier?.price) > totalProduct.disc_ongkir_agent ? Number(courier?.price) - totalProduct.disc_ongkir_agent : 0
    };


    const renderProduct = ({ item }) => (
        <View style={styles.wrapRender}>
            <Image
                source={
                    item.product.file
                        ? { uri: item.product.file }
                        : require('../../Assets/image/notfound.png')
                }
                style={styles.imgRender}
                resizeMode={'contain'}
            />
            <View style={[styles.wrapContent, { flex: 1 }]}>
                <Text style={styles.itemTitle}>{item.product.title}</Text>
                <Text style={styles.itemPrice}>
                    {item.qty} x{' '}
                    <FormatMoney value={typeAkun === 'agent' ? item.product.price_agent : item.product.price} />
                </Text>
                {/* <View style={styles.wrapCategory}>
                <Text style={styles.TextCategory}>Kecil</Text>
            </View> */}
            </View>
            <View style={styles.wrapContent}>
                <FormatMoney value={item.price} style={styles.itemPrice2} />
                <View
                    style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
                    <Image
                        source={require('../../Assets/icon/poin.png')}
                        style={styles.iconPoin}
                    />
                    <Text style={styles.itemTitle}> +{item.poin}</Text>
                </View>
            </View>
        </View>
    );

    const renderPengiriman = ({ item }) => (
        <TouchableOpacity
            onPress={() => {
                onClose(modalDomisili), setCourier(item);
            }}>
            {item.address ? (
                <Text style={styles.itemPrice2}>{item.address}</Text>
            ) : item.domisili ? (
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.itemPrice2}>{item.domisili}</Text>
                        <Text style={[styles.itemPrice2, { marginLeft: 8 }]}>
                            ({item.armada})
                        </Text>
                    </View>
                    <Text style={styles.itemTitle}>{item.description}</Text>
                    <FormatMoney style={styles.itemTitle} value={item.price} />
                </View>
            ) : (
                <View>
                    <Text style={styles.itemPrice2}>
                        {item.courierCode} - {item.service}
                    </Text>
                    <FormatMoney style={styles.itemTitle}
                        value={item.price} />
                </View>
            )}
            <Horizontal />
        </TouchableOpacity>
    );

    const switchShipment = shipment => {
        switch (shipment) {
            case 'store-address':
                return 'Ambil Di Toko (Bebas Biaya)';
            case 'domisili':
                return 'Kurir Toko';
            case 'shipdeo-pricings':
                return 'Ekspedisi';
            default:
                break;
        }
    };

    const titleModal = title => {
        switch (title) {
            case 'store-address':
                return 'Ambil Di Toko';
            case 'domisili':
                return 'Pilih Domisili';
            case 'shipdeo-pricings':
                return 'Pilih Kurir';
            default:
                break;
        }
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>
                {/* pengiriman */}
                <View style={styles.wrapHeader}>
                    <Text style={styles.title}>Pengiriman</Text>
                    {/* not selected  */}
                    {address ? (
                        <TouchableOpacity
                            style={styles.btnSend}
                            onPress={() =>
                                navigation.navigate('Alamat', {
                                    carts: route.params.carts,
                                    payment: route.params.payment,
                                    voucher: route.params.voucher,
                                    from: 'checkout',
                                    type: 'editAlamat',
                                })
                            }>
                            <Ionicons
                                name="location-outline"
                                size={24}
                                color={colors.primary}
                                style={styles.iconSend}
                            />
                            <View style={styles.flexRight}>
                                <Text style={styles.itemPrice2}>{address.recipient_name}</Text>
                                <Text style={styles.textSend}>{address.recipient_phone}</Text>
                                <Text style={styles.textSend}>{address.address}</Text>
                            </View>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.btnSend}
                            onPress={() =>
                                navigation.navigate('Alamat', {
                                    carts: route.params.carts,
                                    payment: route.params.payment,
                                    voucher: route.params.voucher,
                                    from: 'checkout',
                                    type: 'editAlamat',
                                })
                            }>
                            <Ionicons
                                name="location-outline"
                                size={24}
                                color={colors.primary}
                                style={styles.iconSend}
                            />
                            <Text style={styles.textSend}>Masukan Alamat</Text>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    )}
                    {/* selected  */}

                    {/* pengiriman  */}
                    {/* selected  */}
                    {typeDelivery === null || courier === null ? (
                        <TouchableOpacity
                            style={styles.btnSend}
                            onPress={() => onOpen(modalPengiriman)}>
                            <MaterialCommunityIcons
                                name="truck-fast-outline"
                                size={24}
                                color={colors.primary}
                                style={styles.iconSend}
                            />
                            <Text style={styles.textSend}>Pilih Pengiriman</Text>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    ) : (
                        <View style={styles.btnSend}>
                            <View style={{ flex: 1, paddingVertical: 8 }}>
                                <TouchableOpacity
                                    style={styles.wrapCourir}
                                    onPress={() => onOpen(modalPengiriman)}>
                                    <Text style={[styles.itemPrice2, styles.flexRight]}>
                                        {switchShipment(typeDelivery)}
                                    </Text>
                                    <AntDesign name="right" size={16} color={colors.primary} />
                                </TouchableOpacity>
                                <Horizontal />
                                <TouchableOpacity
                                    style={styles.wrapCourir}
                                    onPress={() => {
                                        onOpen(modalDomisili);
                                    }}>
                                    {typeDelivery === 'store-address' ? (
                                        <Text style={[styles.itemPrice2, styles.flexRight]}>
                                            {courier.address}
                                        </Text>
                                    ) : courier.domisili ? (
                                        <View style={{ flex: 1 }}>
                                            <Text style={[styles.itemPrice2, styles.flexRight]}>
                                                {courier.domisili}
                                            </Text>
                                            <FormatMoney value={courier?.price}
                                                style={styles.itemTitle} />
                                        </View>
                                    ) : (
                                        <View style={{ flex: 1 }}>
                                            <Text style={[styles.itemPrice2, styles.flexRight]}>
                                                {courier.courierCode} - {courier.service}
                                            </Text>
                                            <FormatMoney value={courier?.price}
                                                style={styles.itemTitle} />
                                        </View>
                                    )}
                                    <AntDesign name="right" size={16} color={colors.primary} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                </View>

                {/* info produk */}
                <View style={styles.wrapBody}>
                    <Text style={styles.title}>Informasi Produk</Text>
                    <FlatList
                        style={{ marginTop: 8 }}
                        showsVerticalScrollIndicator={false}
                        data={dataProduct}
                        renderItem={renderProduct}
                    />
                    <Horizontal />
                    <View style={styles.wrapSubtotal}>
                        <Text style={styles.textSend}>Subtotal</Text>
                        <FormatMoney value={totalProduct.price}
                            style={styles.itemPrice2} />
                    </View>
                    <View style={styles.wrapGetPoin}>
                        <Text style={styles.textSend}>Poin Didapat</Text>
                        <View style={styles.wrapPoin}>
                            <Image
                                source={require('../../Assets/icon/poin.png')}
                                style={[styles.iconPoin, { marginRight: 4 }]}
                            />
                            <Text style={styles.itemPrice2}>+{totalProduct.poin} Poin</Text>
                        </View>
                    </View>
                </View>

                {/* voucher */}
                {
                    typeAkun === 'agent' ?
                        false :
                        <View style={styles.wrapBody}>
                            <TouchableOpacity
                                style={[styles.btnSend, { paddingVertical: 16 }]}
                                onPress={() =>
                                    navigation.navigate('Voucher', {
                                        carts: route.params.carts,
                                        payment: route.params.payment,
                                        voucher: route.params.voucher,
                                    })
                                }>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                    <Image
                                        source={require('../../Assets/icon/discount.png')}
                                        style={{ width: 20, height: 20, marginRight: 8 }}
                                        resizeMode="contain"
                                    />
                                    <Text style={styles.textSend}>
                                        {route.params.voucher === null
                                            ? 'Pilih Voucher'
                                            : route.params.voucher?.title}
                                    </Text>
                                </View>
                                <AntDesign name="right" size={16} color={colors.primary} />
                            </TouchableOpacity>
                        </View>
                }
                {/* metode pembayaran */}
                <View style={styles.wrapBody}>
                    <Text style={styles.title}>Metode Pembayaran</Text>
                    {route.params.payment === null ? (
                        <TouchableOpacity
                            style={[styles.btnSend, { paddingVertical: 16 }]}
                            onPress={() =>
                                courier === null || courier === 'domisili' || courier === 'shipdeo-pricings' || courier === 'store-address' ?
                                    Alert.alert('Peringatan', 'isi pengiriman terlebih dahulu')
                                    :
                                    navigation.navigate('MetodePembayaran', {
                                        carts: route.params.carts,
                                        payment: route.params.payment,
                                        voucher: route.params.voucher,
                                        shipment_method: shipment_method
                                    })
                            }>
                            <Text style={styles.textSend}>Pilih Metode Pembayaran</Text>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={[styles.btnSend, { paddingVertical: 16 }]}
                            onPress={() =>
                                courier === null || courier === 'domisili' || courier === 'shipdeo-pricings' || courier === 'store-address' ?
                                    Alert.alert('Peringatan', 'isi pengiriman terlebih dahulu')
                                    :
                                    navigation.navigate('MetodePembayaran', {
                                        carts: route.params.carts,
                                        payment: route.params.payment,
                                        voucher: route.params.voucher,
                                        shipment_method: shipment_method
                                    })
                            }>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={{ uri: route.params.payment?.file }}
                                    style={{ width: 48, height: 20, marginRight: 32 }}
                                    resizeMode="contain"
                                />
                                <Text style={{ color: colors.black }}>
                                    {route.params.payment.title}
                                </Text>
                            </View>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    )}
                </View>

                {/* rincoan pesanan */}
                <View style={[styles.wrapBody, { marginBottom: 8 }]}>
                    <Text style={styles.title}>Rincian Pesanan</Text>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.textSend}>
                            Total Harga ({dataProduct.length} barang)
                        </Text>
                        <FormatMoney value={totalProduct.price}
                            style={styles.itemTitle} />
                    </View>
                    {/* diskon */}
                    {route.params.voucher === null ? (
                        false
                    ) : (
                        <View style={styles.wrapDetailOrder}>
                            <Text style={[styles.textSend, {}]}>Diskon</Text>
                            {route.params.voucher?.type_diskon === 'nominal' ? (
                                <FormatMoney value={route.params.voucher?.total_diskon}
                                    style={[styles.itemTitle, { color: colors.success }]} unit={'- Rp'} />
                            ) : (
                                <FormatMoney value={
                                    totalProduct.voucher_price *
                                        (route.params.voucher?.total_diskon / 100) >
                                        route.params.voucher?.max_diskon
                                        ? route.params.voucher?.max_diskon
                                        : totalProduct.voucher_price *
                                        (route.params.voucher?.total_diskon / 100)
                                }
                                    style={[styles.itemTitle, { color: colors.success }]} unit={'- Rp'} />
                            )}
                        </View>
                    )}
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.textSend}>Ongkos Kirim</Text>
                        {
                            typeAkun === 'agent' ?
                                totalProduct.price >= totalProduct.min_order_disc_ongkir_agent ?
                                    <>< FormatMoney value={courier ? courier.price : 0}
                                        style={[styles.itemTitle, { textDecorationLine: 'line-through', marginRight: 4 }]} />
                                        < FormatMoney value={courier ? totalkurir() : 0}
                                            style={[styles.itemTitle, { color: colors.success }]} />
                                    </> : < FormatMoney value={courier ? courier.price : 0}
                                        style={styles.itemTitle} />
                                :
                                <FormatMoney value={courier ? courier.price : 0}
                                    style={styles.itemTitle} />
                        }
                    </View>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.textSend}>Biaya Layanan</Text>
                        <FormatMoney value={route.params.payment?.fee}
                            style={styles.itemTitle} />
                    </View>
                    <Horizontal />
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.title}>Total Pesanan</Text>
                        <FormatMoney value={totalPesanan1()}
                            style={styles.title} />
                    </View>
                </View>
            </ScrollView>

            {/* footer */}
            <View style={styles.wrapFooter}>
                <View>
                    <Text style={[styles.TextHeader, { color: colors.black }]}>
                        Total Pesanan:
                    </Text>
                    <FormatMoney value={totalPesanan1()}
                        style={styles.price} />
                </View>

                <View style={{ height: 40, width: 137 }}>
                    <BtnPrimary title={'Checkout'} pressed={() => postCheckout()} />
                </View>
            </View>

            {/* modalize pengiriman */}
            <Modalize
                ref={modalPengiriman}
                handlePosition={'inside'}
                modalHeight={400}>
                <View style={{ padding: 16 }}>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.title}>Pilih Pengiriman</Text>
                        <MaterialCommunityIcons
                            name="close"
                            size={24}
                            color={colors.black}
                            onPress={() => onClose(modalPengiriman)}
                        />
                    </View>
                    {/* kurir toko */}
                    <TouchableOpacity
                        onPress={() => {
                            setCourier('domisili'),
                                openModal(
                                    modalDomisili,
                                    modalPengiriman,
                                    'courier_store',
                                    'domisili',
                                );
                        }}>
                        <Text style={styles.itemTitle}>Kurir Toko</Text>
                        <Horizontal />
                    </TouchableOpacity>
                    {/* ekspedisi */}
                    <TouchableOpacity
                        onPress={() => {
                            setCourier('shipdeo-pricings'),
                                openModal(
                                    modalDomisili,
                                    modalPengiriman,
                                    'ekspedition',
                                    'shipdeo-pricings',
                                );
                        }}>
                        <Text style={styles.itemTitle}>Ekspedisi</Text>
                        <Horizontal />
                    </TouchableOpacity>
                    {/* ambil di toko */}
                    <TouchableOpacity
                        onPress={() => {
                            setCourier('store-address'),
                                openModal(
                                    modalDomisili,
                                    modalPengiriman,
                                    'pick_up',
                                    'store-address',
                                );
                        }}>
                        <Text style={styles.itemTitle}>Ambil Di Toko (Bebas biaya)</Text>
                        <Horizontal />
                    </TouchableOpacity>
                </View>
            </Modalize>

            {/* modalize Domisili */}
            <Modalize ref={modalDomisili} handlePosition={'inside'} modalHeight={400}>
                <View style={{ padding: 16 }}>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.title}>{titleModal(typeDelivery)}</Text>
                        <MaterialCommunityIcons
                            name="close"
                            size={24}
                            color={colors.black}
                            onPress={() => onClose(modalDomisili)}
                        />
                    </View>

                    <FlatList
                        style={{ marginTop: 8 }}
                        showsVerticalScrollIndicator={false}
                        data={listCourier}
                        renderItem={renderPengiriman}
                        ListEmptyComponent={
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Text style={Font.regular12}>{'tunggu sebentar..'}</Text>
                            </View>
                        }
                    />
                </View>
            </Modalize>
        </SafeAreaView>
    );
};

export default Checkout;

const styles = StyleSheet.create({
    // Header
    wrapHeader: { backgroundColor: colors.white, padding: 16 },
    title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },

    btnSend: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        marginVertical: 8,
        flex: 1,
    },
    iconSend: { marginRight: 8 },
    textSend: { flex: 1, fontSize: 12, color: colors.black },

    // render
    wrapRender: { flex: 1, flexDirection: 'row', marginVertical: 8 },
    imgRender: { width: 56, height: 56, borderRadius: 15 },
    wrapContent: { marginLeft: 8, justifyContent: 'center' },
    itemTitle: { fontSize: 12, color: colors.black },
    itemPrice: { fontSize: 11, color: colors.black, marginVertical: 8 },
    itemPrice2: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
    wrapCategory: {
        flex: 1,
        backgroundColor: colors.blueShading,
        paddingHorizontal: 8,
        paddingVertical: 4,
        borderRadius: 100,
        width: 70,
        alignItems: 'center',
    },
    TextCategory: { color: colors.primary, fontSize: 12 },

    // body
    wrapBody: { marginTop: 8, backgroundColor: colors.white, padding: 16 },
    wrapSubtotal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 16,
    },
    wrapGetPoin: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.yellowShading,
        marginBottom: 16,
        paddingVertical: 12,
        paddingHorizontal: 8,
        borderRadius: 100,
        alignItems: 'center',
    },
    wrapPoin: { flexDirection: 'row', alignItems: 'center' },
    iconPoin: { width: 24, height: 24 },
    wrapCourir: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
    },
    flexRight: { flex: 1, marginRight: 10 },

    wrapDetailOrder: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
    },

    // footer
    wrapFooter: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        elevation: 6,
    },
    price: {
        fontSize: 18,
        color: colors.primary,
        fontWeight: 'bold',
        marginBottom: 4,
    },
});
