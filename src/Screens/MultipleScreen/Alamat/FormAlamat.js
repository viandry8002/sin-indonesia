import AsyncStorage from '@react-native-async-storage/async-storage';
import Geolocation from '@react-native-community/geolocation';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import {
  Alert,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import { Modalize } from 'react-native-modalize';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import Horizontal from '../../MultipleComponent/Horizontal';
import InputComp from '../../MultipleComponent/InputComp';

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const FormAlamat = ({ route, navigation }) => {
  const modalize = useRef(null);
  const [listModal, setListModal] = useState([]);
  const [activeModal, setActiveModal] = useState('');
  const [tokens, setTokens] = useState('');
  //maps
  const [region, setRegion] = useState({
    latitude: -6.5776978,
    longitude: 106.7792039,
    latitudeDelta: 0.1,
    longitudeDelta: 0.1,
  });

  const onOpen = modal => {
    modal.current?.open();
  };

  const onClose = modal => {
    modal.current?.close();
  };

  const [form, setForm] = useState({
    id: '',
    recipient_name: '',
    recipient_phone: '',
    province_id: '',
    province_name: '',
    city_id: '',
    city_name: '',
    subdistrict_id: '',
    subdistrict_name: '',
    postcode: '',
    address: '',
    latitude: '',
    longitude: '',
  });

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    route.params.type === 'edit'
      ? navigation.setOptions({ title: 'Edit Alamat' })
      : false;
    getToken();
    getCurrentLocation();
    route.params.type === 'registerAgen' ?
      route.params.address != null ?
        setForm(
          route.params.address
        ) : false
      : false
  }, []);

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };

  const getProvince = () => {
    Axios.get(`${api}/area/province`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getData = token => {
    setTokens(token);
    getProvince();

    route.params.type === 'edit'
      ? Axios.get(`${api}/user-address/get/${route.params.id}`, {
        headers: {
          key: key,
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => {
          res.data.success
            ? route.params.region === null
              ? setForm({
                ...form,
                ['id']: res.data.data.id,
                ['recipient_name']: res.data.data.recipient_name,
                ['recipient_phone']: res.data.data.recipient_phone,
                ['province_id']: res.data.data.province_id,
                ['province_name']: res.data.data.province_name,
                ['city_id']: res.data.data.city_id,
                ['city_name']: res.data.data.city_name,
                ['subdistrict_id']: res.data.data.subdistrict_id,
                ['subdistrict_name']: res.data.data.subdistrict_name,
                ['postcode']: res.data.data.postcode,
                ['address']: res.data.data.address,
                ['latitude']: res.data.data.latitude,
                ['longitude']: res.data.data.longitude,
              })
              : setForm({
                ...form,
                ['id']: route.params.form.id,
                ['recipient_name']: route.params.form.recipient_name,
                ['recipient_phone']: route.params.form.recipient_phone,
                ['province_id']: route.params.form.province_id,
                ['province_name']: route.params.form.province_name,
                ['city_id']: route.params.form.city_id,
                ['city_name']: route.params.form.city_name,
                ['subdistrict_id']: route.params.form.subdistrict_id,
                ['subdistrict_name']: route.params.form.subdistrict_name,
                ['postcode']: route.params.form.postcode,
                ['address']: route.params.form.address,
                ['latitude']: route.params.region.latitude,
                ['longitude']: route.params.region.longitude,
              })
            : navigation.navigate('GoLogout', res.data?.message);
        })
        .catch(err => {
          console.log(err);
        })
      : route.params.form
        ? setForm({
          ...form,
          ['id']: route.params.form.id,
          ['recipient_name']: route.params.form.recipient_name,
          ['recipient_phone']: route.params.form.recipient_phone,
          ['province_id']: route.params.form.province_id,
          ['province_name']: route.params.form.province_name,
          ['city_id']: route.params.form.city_id,
          ['city_name']: route.params.form.city_name,
          ['subdistrict_id']: route.params.form.subdistrict_id,
          ['subdistrict_name']: route.params.form.subdistrict_name,
          ['postcode']: route.params.form.postcode,
          ['address']: route.params.form.address,
          ['latitude']: route.params.region.latitude,
          ['longitude']: route.params.region.longitude,
        })
        : false;
  };

  const getCurrentLocation = () => {
    try {
      Geolocation.getCurrentPosition(
        //Will give you the current location
        position => {
          //getting the Latitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);

          //getting the Longitude from the location json
          const currentLongitude = JSON.stringify(position.coords.longitude);
          setRegion({
            latitude: currentLatitude,
            longitude: currentLongitude,
            latitudeDelta: 0.0092,
            longitudeDelta: 0.0081,
          });
        },
        error => {
          console.log(error.message, 'error nih');
        },
        {
          enableHighAccuracy: true,
          timeout: 1000,
          maximumAge: 1000,
        },
      );
    } catch (error) {
      console.log(error, 'gagal berubah');
    }
  };

  const getCity = () => {
    // ListCity
    Axios.get(`${api}/area/city?province_id=${form.province_id}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getDistrict = () => {
    // ListDistrict
    Axios.get(`${api}/area/district?city_id=${form.city_id}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setListModal(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => {
        if (activeModal === 'Provinsi') {
          setForm({
            ...form,
            ['province_name']: item.name,
            ['province_id']: item.id,
          });
        } else if (activeModal === 'Kota') {
          setForm({
            ...form,
            ['city_name']: item.name,
            ['city_id']: item.id,
          });
        } else {
          setForm({
            ...form,
            ['subdistrict_name']: item.name,
            ['subdistrict_id']: item.id,
          });
        }

        onClose(modalize);
      }}>
      <Text style={styles.itemTitle}>{item.name}</Text>
      <Horizontal />
    </TouchableOpacity>
  );

  const openModal = nav => {
    onOpen(modalize);
    setActiveModal(nav);

    if (nav === 'Provinsi') {
      getProvince();
    } else if (nav === 'Kota') {
      getCity();
    } else {
      getDistrict();
    }
  };

  const onSubmit = () => {
    route.params.type === 'registerAgen' ?

      form.recipient_name === '' ||
        form.recipient_phone === '' ||
        form.province_id === '' ||
        form.province_name === '' ||
        form.city_id === '' ||
        form.city_name === '' ||
        form.subdistrict_id === '' ||
        form.subdistrict_name === '' ||
        form.postcode === '' ||
        form.address === '' ||
        form.latitude === '' ||
        form.longitude === '' ? Alert.alert('peringatan', 'form alamat belum terisi semua') :
        navigation.navigate('Step3', {
          // step1: route.params.step1,
          // step2: route.params.step2,
          step1: route.params.step1,
          step2: route.params.step2,
          address: form,
          courier: route.params.courier,
          payment: route.params.payment,
          total_all_buy: route.params.total_all_buy,
        }) :
      Axios.post(
        `${api}/user-address/${route.params.type === 'edit' ? 'update' : 'create'
        }`,
        form,
        {
          headers: {
            key: key,
            Authorization: 'Bearer ' + tokens,
          },
        },
      )
        .then(res => {
          res.data.success === true
            ? (ToastAndroid.show(
              route.params.type === 'edit'
                ? 'Data Alamat berhasil di ubah'
                : 'Data Alamat berhasil di simpan',
              ToastAndroid.SHORT,
            ),
              // route.params.from === 'profile' ?
              navigation.navigate('Alamat', route.params)
              // :
              // navigation.navigate('Alamat', {
              //   carts: route.params.carts,
              //   payment: route.params.payment,
              //   voucher: route.params.voucher,
              //   from: 'checkout',
              // })
              // navigation.navigate('Alamat', (route.params, route.params.etcParam))
            )
            : Alert.alert('Peringatan', res.data.message);
        })
        .catch(err => {
          Alert.alert('Peringatan', err.response.data.message);
          console.log(err);
        });
  };

  const regionParam =
    route.params.region === null
      ? form.latitude === '' && form.longitude === ''
        ? region
        : {
          latitude: Number(form.latitude),
          longitude: Number(form.longitude),
          latitudeDelta: 0.1,
          longitudeDelta: 0.1,
        }
      : route.params.region;

  return (
    <View style={{ flex: 1, backgroundColor: colors.white }}>
      <ScrollView
        style={{ marginHorizontal: 16 }}
        showsVerticalScrollIndicator={false}>
        <InputComp
          text={'Nama Penerima'}
          placeholder={'masukan nama penerima'}
          value={form.recipient_name}
          onChange={value => onInputChange(value, 'recipient_name')}
        />

        <InputComp
          text={'No. Telpon'}
          placeholder={'masukan nomor telepon'}
          tkeyboard={'numeric'}
          value={form.recipient_phone}
          onChange={value => onInputChange(value, 'recipient_phone')}
        />

        <InputComp
          text={'Provinsi'}
          placeholder={'masukan nama provinsi'}
          value={form.province_name}
          press={() => openModal('Provinsi')}
          inputfocus={false}
          ricon={
            <Ionicons
              name="chevron-down"
              size={18}
              color={colors.black}
              onPress={() => openModal('Provinsi')}
            />
          }
        />

        <InputComp
          text={'Kabupaten/Kota'}
          placeholder={'masukan nama kota'}
          value={form.city_name}
          press={() => openModal('Kota')}
          inputfocus={false}
          ricon={
            <Ionicons
              name="chevron-down"
              size={18}
              color={colors.black}
              onPress={() => openModal('Kota')}
            />
          }
        />

        <InputComp
          text={'Kecamatan'}
          placeholder={'masukan nama Kecamatan'}
          value={form.subdistrict_name}
          press={() => openModal('Kecamatan')}
          inputfocus={false}
          ricon={
            <Ionicons
              name="chevron-down"
              size={18}
              color={colors.black}
              onPress={() => openModal('Kecamatan')}
            />
          }
        />

        <InputComp
          text={'Kode Pos'}
          placeholder={'masukan nama Pos'}
          value={form.postcode}
          onChange={value => onInputChange(value, 'postcode')}
          mlength={5}
        />

        {/* maps */}
        <View style={{ height: 200 }}>
          <Text style={styles.labelInput}>Pilh dengan peta</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Maps', {
                type: route.params.type,
                id: route.params.id,
                region: regionParam,
                form: form,
                step1: route.params.step1,
                step2: route.params.step2,
                address: form,
                courier: route.params.courier,
                payment: route.params.payment,
                total_all_buy: route.params.total_all_buy,
              });
            }}
            style={styles.cardMap}>
            {/* maps */}
            <View style={{ height: 155 }}>
              <MapView
                // ref={mapRef}
                // style={styles.map}
                // region={region}
                // onRegionChangeComplete={dragMarker}
                // showsUserLocation={false}
                // showsCompass={false}
                // zoomControlEnabled={false}
                // showsTraffic={false}
                // provider={PROVIDER_GOOGLE}
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                loadingEnabled={true}
                showsMyLocationButton={true}
                // showsUserLocation={true}
                userLocationPriority="high"
                zoomControlEnabled={true}
                region={regionParam}
              />
              {/* <Image source={require('../../assets/icons/map-pin.png')} style={{ width: 35, height: 35, position: 'absolute', top: '44%', left: '46%' }} /> */}
              <Ionicons
                name="location-sharp"
                size={18}
                color={colors.primary}
                style={{
                  width: 35,
                  height: 35,
                  position: 'absolute',
                  top: '44%',
                  left: '46%',
                }}
              />
            </View>
            {/* <View style={styles.maps}>
                <Image source={require('../../assets/images/maps.png')} style={{ width: '100%', height: 110 }} />
              </View> */}
          </TouchableOpacity>
        </View>

        <InputComp
          text={'Detail Alamat'}
          placeholder={'masukan nama alamat'}
          value={form.address}
          onChange={value => onInputChange(value, 'address')}
        />
      </ScrollView>

      <View style={styles.footer}>
        <BtnPrimary title={'Konfirmasi'} pressed={() => onSubmit()} />
      </View>

      {/* modalize Province */}
      <Modalize
        ref={modalize}
        handlePosition={'inside'}
        modalHeight={400}
        HeaderComponent={
          <View style={styles.wrapModalize}>
            <View style={styles.wrapDetailOrder}>
              <Text style={styles.title}>Pilih {activeModal}</Text>
              <MaterialCommunityIcons
                name="close"
                size={24}
                color={colors.black}
                onPress={() => onClose(modalize)}
              />
            </View>
          </View>
        }>
        <View style={styles.wrapModalize}>
          <FlatList
            style={{ marginTop: 8 }}
            showsVerticalScrollIndicator={false}
            data={listModal}
            renderItem={renderItem}
          />
        </View>
      </Modalize>
    </View>
  );
};

export default FormAlamat;
const styles = StyleSheet.create({
  footer: {
    backgroundColor: colors.white,
    elevation: 6,
    height: 70,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  // modalize
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },
  itemTitle: { fontSize: 12, color: colors.black },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  cardMap: {
    height: 151,
  },
  labelInput: {
    fontWeight: 'bold',
    color: colors.black,
    fontSize: 12,
    marginTop: 16,
    marginBottom: 8,
  },
  wrapModalize: { paddingHorizontal: 16, marginTop: 16 },
});
