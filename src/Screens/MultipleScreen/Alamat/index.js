import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  RefreshControl,
  Alert
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../Styles/Colors';
import { api, key } from '../../../Variable';
import BtnOutline from '../../MultipleComponent/BtnOutline';
import BtnPrimary from '../../MultipleComponent/BtnPrimary';
import BtnDisable from '../../MultipleComponent/BtnDisable';

const Index = ({ navigation, route }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [check, setCheck] = useState(0);
  const [tokens, setTokens] = useState(null);
  const [data, setData] = useState([]);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('api_token');
      return getData(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setRefreshing(true);
      setTimeout(() => {
        getToken();
      }, 1000);
    });
    return unsubscribe;
  }, []);

  const getData = token => {
    setTokens(token);
    Axios.get(`${api}/user-address`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + token,
      },
    })
      .then(res => {
        res.data.success
          ? setData(res.data.data)
          : navigation.navigate('GoLogout', res.data?.message);
        setRefreshing(false);
      })
      .catch(err => {
        console.log(err);
        setRefreshing(false);
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData(tokens);
    }, 2000);
  };

  const deleteData = ida => {
    Axios.delete(`${api}/user-address/delete/${ida}`, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (ToastAndroid.show(res.data.message, ToastAndroid.SHORT),
            getData(tokens))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const postDefault = () => {
    let body = {
      id: check,
    };

    Axios.post(`${api}/user-address/update/default`, body, {
      headers: {
        key: key,
        Authorization: 'Bearer ' + tokens,
      },
    })
      .then(res => {
        res.data.success
          ? (ToastAndroid.show('Berhasil ubah alamat', ToastAndroid.SHORT),
            navigation.push('Checkout', {
              carts: route.params.carts,
              payment: route.params.payment,
              voucher: route.params.voucher,
              type: route.params.type,
            }))
          : navigation.navigate('GoLogout', res.data?.message);
      })
      .catch(err => {
        Alert.alert('Peringatan', err.response.data.message);
        console.log(err);
      });
  };

  const renderProduct = ({ item }) => (
    <TouchableOpacity
      style={[
        styles.wrapRender,
        item.id === check ? { backgroundColor: colors.greenContain } : false,
      ]}
      onPress={() => (route.params ? setCheck(item.id) : false)}
      disabled={route.params.from === 'profile' ? true : false}
    >
      <View style={{ flex: 1 }}>
        <Text style={styles.title}>{item.recipient_name}</Text>
        <Text style={styles.subTitle}>{item.recipient_phone}</Text>
        <Text style={styles.subTitle}>{item.address}</Text>
      </View>
      <View style={styles.wrapBtn1}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={[styles.wrapBtn, { marginRight: 8 }]}
            onPress={() =>
              navigation.navigate('FormAlamat', {
                type: 'edit',
                id: item.id,
                region: null,
                from: route.params.from === 'profile' ? 'profile' : null,
                carts: route.params.carts,
                payment: route.params.payment,
                voucher: route.params.voucher,
                // etcParam: route.params
              })
            }>
            <Text style={styles.textBtn}>Edit Alamat</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.wrapBtn}
            onPress={() => deleteData(item.id)}>
            <Text style={styles.textBtn}>Hapus Alamat</Text>
          </TouchableOpacity>
        </View>
        {item.id === check ? (
          <Image
            source={require('../../../Assets/icon/success.png')}
            style={{ width: 18, height: 18 }}
          />
        ) : (
          false
        )}
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <FlatList
        data={data}
        renderItem={renderProduct}
        ListHeaderComponent={<View style={{ marginTop: 4 }} />}
        ListFooterComponent={
          <View style={styles.lfooter}>
            <BtnOutline
              title={'Tambah Alamat'}
              pressed={() =>
                navigation.navigate('FormAlamat', {
                  type: 'add',
                  id: null,
                  region: null,
                  from: route.params.from === 'profile' ? 'profile' : null,
                  carts: route.params.carts,
                  payment: route.params.payment,
                  voucher: route.params.voucher,
                  // etcParam: route.params
                })
              }
              icon={
                <MaterialCommunityIcons
                  name="plus"
                  size={18}
                  color={colors.primary}
                />
              }
            />
          </View>
        }
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
      {
        route.params.from != 'profile' && check != 0 ? (
          <View style={styles.footer}>
            <BtnPrimary title={'Konfirmasi'} pressed={() => postDefault()} />
          </View>
        ) : (
          false
        )}
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  // render
  wrapRender: {
    padding: 16,
    flex: 1,
    marginVertical: 4,
    marginHorizontal: 8,
    backgroundColor: colors.white,
    elevation: 2,
    borderRadius: 7,
  },
  title: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
  subTitle: { fontSize: 12, color: colors.black },
  wrapBtn1: {
    marginTop: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapBtn: {
    width: 98,
    height: 34,
    borderWidth: 1,
    borderColor: colors.gray5,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBtn: { fontSize: 12, color: colors.black },

  //footer
  lfooter: { padding: 16, backgroundColor: colors.white, marginTop: 4 },
  footer: {
    backgroundColor: colors.white,
    elevation: 6,
    height: 70,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
});
