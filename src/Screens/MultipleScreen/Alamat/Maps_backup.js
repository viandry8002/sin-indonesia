// import library
import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    TouchableOpacity,
    ToastAndroid,
    Alert,
    Text,
    TextInput,
    ScrollView,
} from 'react-native';
import Permissions from 'react-native-permissions';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Icons from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Feather';
import Gps from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
//import component
// import styles
// import colors from '../../styles/colors';
// import FormStyles from '../../styles/FormStyles';
import colors from '../../../Styles/Colors';

const { height, width } = Dimensions.get('window');

// main function
const Maps = ({ navigation, route }) => {
    const [state, setState] = useState({
        markerPosition: {
            latitude: route.params.region.latitude,
            longitude: route.params.region.longitude,
        },
    });
    let mapRef = useRef(null);

    useEffect(() => {
        handleUserLocation();
    }, []);

    const dragMarker = async region => {
        // ToastAndroid.show(JSON.stringify(region.latitude), ToastAndroid.SHORT);
        const latlong = {
            latitude: route.params?.data?.latitude,
            longitude: route.params?.data?.longitude,
        };
        try {
            let markerPosition =
                latlong.latitude === undefined ? state.markerPosition : latlong;
            markerPosition.latitude = region.latitude;
            markerPosition.longitude = region.longitude;

            setState({
                markerPosition: markerPosition,
            });
        } catch (error) {
            console.log(error.response.data, 'error drag');
        }
    };

    const handleUserLocation = async () => {
        const check = await Permissions.check('location').catch(err => {
            console.error(err, 'ini ya');
            throw err;
        });
        if (check === 'undetermined') {
            const request = await Permissions.request('location').catch(err => {
                console.error(err);
                throw err;
            });
        }

        try {
            Geolocation.getCurrentPosition(
                //Will give you the current location
                async position => {
                    console.log(position, 'location get');
                    const region = {
                        latitude:
                            route.params?.data === undefined
                                ? position.coords.latitude
                                : parseFloat(route.params?.data?.latitude),
                        longitude:
                            route.params?.data === undefined
                                ? position.coords.longitude
                                : parseFloat(route.params?.data?.longitude),
                        latitudeDelta: 0.0092,
                        longitudeDelta: 0.0081,
                    };

                    setTimeout(() => {
                        mapRef.current?.animateToRegion(region);
                    }, 100);
                },
                error => {
                    console.log(error.message, 'error get');
                },
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 },
            );
        } catch (error) {
            console.log(error, 'gagal berubah');
        }
    };

    const saveLocation = async () => {
        await setState(state);
        let coordinats = {
            latitude: state.markerPosition.latitude,
            longitude: state.markerPosition.longitude,
            latitudeDelta: 0.0092,
            longitudeDelta: 0.0081,
        };

        // console.log('ressssszz', route.params.form);
        route.params.type === 'registerAgen' ?
            navigation.push('FormAlamat', {
                type: route.params.type,
                id: route.params.id,
                region: coordinats,
                form: route.params.form,
                step1: route.params.step1,
                step2: route.params.step2,
                courier: route.params.courier,
                payment: route.params.payment,
                total_all_buy: route.params.total_all_buy,
            })
            :
            route.params.from === 'profile' ?
                navigation.push('FormAlamat', {
                    type: route.params.type,
                    id: route.params.id,
                    region: coordinats,
                    form: route.params.form,
                }) :
                navigation.push('FormAlamat', {
                    type: route.params.type,
                    id: route.params.id,
                    region: coordinats,
                    form: route.params.form,
                    carts: route.params.carts,
                    payment: route.params.payment,
                    voucher: route.params.voucher,
                })
    };

    return (
        <View style={{ flex: 1 }}>
            <View style={{ height: (height * 3) / 2.9 }}>
                <MapView
                    ref={mapRef}
                    style={styles.map}
                    initialRegion={{
                        latitude: state.markerPosition.latitude,
                        longitude: state.markerPosition.longitude,
                        latitudeDelta: 0.0092,
                        longitudeDelta: 0.0081,
                    }}
                    onRegionChangeComplete={dragMarker}
                    provider={PROVIDER_GOOGLE}
                // loadingEnabled={true}
                // showsMyLocationButton={true}
                // showsUserLocation={true}
                // userLocationPriority="high"
                // zoomControlEnabled={true}
                />
                <Ionicons
                    name="location-sharp"
                    size={25}
                    color={colors.primary}
                    style={{
                        width: 35,
                        height: 35,
                        position: 'absolute',
                        top: '44%',
                        left: '46%',
                    }}
                />
                <View style={{ flexDirection: 'row', top: '10%', marginLeft: 13 }}>
                    <TouchableOpacity
                        onPress={() => {
                            navigation.goBack();
                        }}
                        style={{
                            backgroundColor: colors.white,
                            width: 44,
                            height: 44,
                            borderRadius: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Icons name="arrowleft" color={colors.black} size={20} />
                    </TouchableOpacity>
                    <GooglePlacesAutocomplete
                        placeholder={'Cari Alamat...'}
                        minLength={2}
                        autoFocus={false}
                        fetchDetails={true}
                        debounce={200}
                        nearbyPlacesAPI="GooglePlacesSearch"
                        GooglePlacesSearchQuery={{
                            rankby: 'distance',
                            region: 'id',
                            key: 'AIzaSyDCjgH_wSFMrlpKMZX0ZCTa3xLotOt-rh0',
                        }}
                        listViewDisplayed={false}
                        returnKeyType={'search'}
                        query={{
                            key: 'AIzaSyDCjgH_wSFMrlpKMZX0ZCTa3xLotOt-rh0',
                            language: 'id',
                            // types: 'geocode',
                        }}
                        onPress={async (data, details = null) => {
                            const region = {
                                latitude: details.geometry.location.lat,
                                longitude: details.geometry.location.lng,
                                latitudeDelta: 0.0092,
                                longitudeDelta: 0.0081,
                            };

                            setTimeout(() => {
                                mapRef.current?.animateToRegion(region);
                            }, 10);
                        }}
                        onFail={error => console.error(error, 'error')}
                        currentLocation={true}
                        currentLocationLabel="Current Location"
                        renderLeftButton={() => (
                            <Ionicons
                                name={'search-outline'}
                                style={{ color: colors.gray4, alignSelf: 'center', left: 7 }}
                                size={20}
                            />
                        )}
                        styles={styles.autoComplete}
                    />
                </View>
            </View>

            <View style={{ position: 'absolute', bottom: 0 }}>
                <View
                    style={{
                        width: Dimensions.get('window').width * 1,
                        backgroundColor: colors.white,
                        padding: 16,
                        // elevation: 5,
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            saveLocation();
                        }}
                        style={{
                            backgroundColor: colors.primary,
                            borderRadius: 8,
                            paddingVertical: 15,
                            marginTop: 5,
                            //   marginBottom: 14,
                        }}>
                        <Text
                            style={{ fontSize: 13, color: colors.white, textAlign: 'center' }}>
                            Konfirmasi
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default Maps;
// styles
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    autoComplete: {
        container: {
            width: '100%',
            // position: "absolute",
        },
        textInputContainer: {
            paddingHorizontal: 10,
            alignItems: 'center',
            // marginBottom: 45,
            backgroundColor: colors.white,
            borderWidth: 1,
            borderColor: 'transparent',
            height: 45,
            borderRadius: 25,
            marginHorizontal: 15,
        },
        textInput: {
            paddingVertical: 5,
            paddingHorizontal: 10,
            fontSize: 14,
            flex: 1,
            color: colors.black,
            backgroundColor: 'transparent',
            fontFamily: 'SourceSansPro-Regular',
        },
        description: {
            fontFamily: 'SourceSansPro-Bold',
            color: 'black',
            overflow: 'hidden',
            flexWrap: 'wrap',
            fontSize: 12,
            width: 285,
            fontFamily: 'Poppins-SemiBold',
        },
        separator: {
            padding: 0,
            marginVertical: 0,
        },
        listView: {
            padding: 0,
            backgroundColor: colors.white,
            borderRadius: 8,
            overflowX: 'hidden',
            width: '91%',
            marginLeft: '5%',
            // height: height*0.4
        },
        row: {
            backgroundColor: 'transparent',
        },
        poweredContainer: {
            display: 'none',
        },
    },
});
