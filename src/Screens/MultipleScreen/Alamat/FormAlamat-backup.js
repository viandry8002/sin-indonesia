import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { Alert, FlatList, ScrollView, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../../../Styles/Colors';
import { api, key } from '../../../../Variable';
import BtnPrimary from '../../../MultipleComponent/BtnPrimary';
import Horizontal from '../../../MultipleComponent/Horizontal';
import InputComp from '../../../MultipleComponent/InputComp';

const FormAlamat = ({ route, navigation }) => {
    const modalize = useRef(null);
    const [listModal, setListModal] = useState([]);
    const [activeModal, setActiveModal] = useState('');

    const onOpen = (modal) => {
        modal.current?.open();
    };

    const onClose = (modal) => {
        modal.current?.close();
    };
    const [tokens, setTokens] = useState('');
    const [form, setForm] = useState({
        id: '',
        recipient_name: '',
        recipient_phone: '',
        province_id: '',
        province_name: '',
        city_id: '',
        city_name: '',
        subdistrict_id: '',
        subdistrict_name: '',
        postcode: '',
        address: '',
        latitude: '',
        longitude: ''
    })

    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem("api_token");
            return getData(token)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getToken();
    }, [])

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        })
    }

    const getProvince = () => {

        Axios.get(`${api}/area/province`, {
            headers: {
                'key': key,
            }
        })
            .then((res) => {
                setListModal(res.data.data)
            }).catch((err) => {
                console.log(err)
            })
    }

    const getData = (token) => {
        setTokens(token)
        getProvince()

        route.params.type === 'edit' ? (
            Axios.get(`${api}/user-address/get/${route.params.id}`, {
                headers: {
                    'key': key,
                    'Authorization': 'Bearer ' + token,
                }
            })
                .then((res) => {
                    console.log('res', res.data)
                    setForm({
                        ...form,
                        ['id']: res.data.data.id,
                        ['recipient_name']: res.data.data.recipient_name,
                        ['recipient_phone']: res.data.data.recipient_phone,
                        ['province_id']: res.data.data.province_id,
                        ['city_id']: res.data.data.city_id,
                        ['subdistrict_id']: res.data.data.subdistrict_id,
                        ['postcode']: res.data.data.postcode,
                        ['address']: res.data.data.address,
                        ['latitude']: res.data.data.latitude,
                        ['longitude']: res.data.data.longitude,
                    })
                }).catch((err) => {
                    console.log(err)
                })
        ) : false
    }

    const getCity = () => {

        // ListCity
        Axios.get(`${api}/area/city?province_id=${form.province_id}`, {
            headers: {
                'key': key,
            }
        })
            .then((res) => {
                setListModal(res.data.data)
            }).catch((err) => {
                console.log(err)
            })
    }

    const getDistrict = () => {

        // ListDistrict
        Axios.get(`${api}/area/district?city_id=${form.city_id}`, {
            headers: {
                'key': key,
            }
        })
            .then((res) => {
                setListModal(res.data.data)
            }).catch((err) => {
                console.log(err)
            })
    }

    const renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => {
            if (activeModal === 'provinsi') {
                setForm({
                    ...form,
                    ['province_name']: item.name,
                    ['province_id']: item.id
                })
            } else if (activeModal === 'kota') {
                setForm({
                    ...form,
                    ['city_name']: item.name,
                    ['city_id']: item.id
                })
            } else {
                setForm({
                    ...form,
                    ['subdistrict_name']: item.name,
                    ['subdistrict_id']: item.id
                })
            }

            onClose(modalize)


        }}>
            <Text style={styles.itemTitle}>{item.name}</Text>
            <Horizontal />
        </TouchableOpacity>
    );

    const openModal = (nav) => {

        onOpen(modalize)
        setActiveModal(nav)

        if (nav === 'provinsi') {
            getProvince()
        } else if (nav === 'kota') {
            getCity()
        } else {
            getDistrict()
        }
    }

    const onSubmit = () => {

        Axios.post(`${api}/user-address/${route.params.type === 'edit' ? 'update' : 'create'}`, form, {
            headers: {
                'key': key,
                'Authorization': 'Bearer ' + tokens,
            }
        })
            .then((res) => {
                res.data.success === true ? (
                    ToastAndroid.show(route.params.type === 'edit' ? 'Data Alamat berhasil di ubah' : 'Data Alamat berhasil di simpan', ToastAndroid.SHORT),
                    navigation.goBack()
                ) : Alert.alert('Peringatan', res.data.message)
            }).catch((err) => {
                Alert.alert('Peringatan', err.response.data.message)
                console.log(err)
            })
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <ScrollView style={{ marginHorizontal: 16 }} showsVerticalScrollIndicator={false}>
                <InputComp text={'Nama Penerima'} placeholder={'masukan nama penerima'} value={form.recipient_name} onChange={value => onInputChange(value, 'recipient_name')} />

                <InputComp text={'No. Telpon'} placeholder={'masukan nama telepon'} value={form.recipient_phone} onChange={value => onInputChange(value, 'recipient_phone')} />

                <InputComp text={'Provinsi'} placeholder={'masukan nama provinsi'} value={form.province_name} ricon={(<Ionicons name='chevron-down' size={18} color={colors.black} onPress={() => openModal('provinsi')} />)} />

                <InputComp text={'Kabupaten/Kota'} placeholder={'masukan nama kota'} value={form.city_name} ricon={(<Ionicons name='chevron-down' size={18} color={colors.black}
                    onPress={() => openModal('kota')} />)} />

                <InputComp text={'Kecamatan'} placeholder={'masukan nama Kecamatan'} value={form.subdistrict_name} ricon={(<Ionicons name='chevron-down' size={18} color={colors.black} onPress={() => openModal('kecamatan')} />)} />

                <InputComp text={'Kode Pos'} placeholder={'masukan nama Pos'} value={form.postcode} onChange={value => onInputChange(value, 'postcode')} />

                <InputComp text={'Detail Alamat'} placeholder={'masukan nama alamat'} value={form.address} onChange={value => onInputChange(value, 'address')} />
            </ScrollView>

            <View style={styles.footer} >
                <BtnPrimary title={'Konfirmasi'} pressed={() => onSubmit()} />
            </View>

            {/* modalize Province */}
            <Modalize ref={modalize}
                withHandle={true}
                snapPoint={250}>
                <View style={{ padding: 16 }}>
                    <View style={styles.wrapDetailOrder}>
                        <Text style={styles.title}>Pilih Provinsi</Text>
                        <MaterialCommunityIcons name='close' size={24} color={colors.black} onPress={() => onClose(modalize)} />
                    </View>
                    <FlatList
                        style={{ marginTop: 8 }}
                        showsVerticalScrollIndicator={false}
                        data={listModal}
                        renderItem={renderItem}
                    />
                </View>
            </Modalize>

        </SafeAreaView>
    )
}

export default FormAlamat
const styles = StyleSheet.create({
    footer: { backgroundColor: colors.white, elevation: 6, height: 70, paddingHorizontal: 16, justifyContent: 'center' },
    // modalize
    wrapDetailOrder: { flexDirection: 'row', justifyContent: 'space-between', marginVertical: 8 },
    title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },
    itemTitle: { fontSize: 12, color: colors.black },
});
