import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import {
    Dimensions,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    Alert,
    TouchableOpacity,
} from 'react-native';
import colors from '../../Styles/Colors';
import { api, key } from '../../Variable';
import BtnPrimary from '../MultipleComponent/BtnPrimary';
import Gradient from '../MultipleComponent/Gradient';
import InputComp from '../MultipleComponent/InputComp';
import { launchImageLibrary } from 'react-native-image-picker';

const windowWidth = Dimensions.get('window').width;

const EditAkun = ({ navigation, route }) => {
    const [tokens, setTokens] = useState('');
    const [form, setForm] = useState({
        id: '',
        name: '',
        email: '',
        email_old: '',
        phone: '',
        // ahli_waris: '',
        no_ktp: '',
        file: '',
        password: '',
        password_confirmation: '',
    });
    const [isChose, setIsChose] = useState(false);
    const [image, setImage] = useState(null);
    const [show, setShow] = useState(true);
    const [typeAkun, setTypeAkun] = useState('');

    const getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('api_token');
            const type = await AsyncStorage.getItem('type_akun');
            setTypeAkun(type);
            return getData(token, type);
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        getToken();
    }, []);

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    };

    const getData = (token, typeAkun) => {
        setTokens(token);
        // setload(true)
        Axios.get(`${api}/user`, {
            headers: {
                key: key,
                Authorization: 'Bearer ' + token,
            },
        })
            .then(res => {
                // console.log('res', res.data)
                res.data.success
                    ? (setForm({
                        ...form,
                        ['id']: res.data.data.id,
                        ['file']: res.data.data.file,
                        ['name']: res.data.data.name,
                        ['email']: res.data.data.email,
                        ['email_old']: res.data.data.email,
                        // ['ahli_waris']: typeAkun === 'agent' ? res.data.data.agent.ahli_waris : '',
                        ['no_ktp']: typeAkun === 'agent' ? res.data.data.agent.no_ktp : '',
                        ['phone']: res.data.data.phone,
                    }),
                        setImage(
                            'https://generasibisa.com/api-sin-indonesia/public/' +
                            res.data.data.file,
                        ))
                    : navigation.navigate('GoLogout', res.data?.message);
                // setload(false)
            })
            .catch(err => {
                console.log(err);
                // setload(false)
            });
    };

    const chooseFile = type => {
        let options = {
            mediaType: type,
            maxWidth: 3000,
            maxHeight: 3000,
            quality: 1,
        };
        launchImageLibrary(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // ////console.log(
                //   'User tapped custom button: ',
                //   response.customButton
                // );
                alert(response.customButton);
            } else {
                let tmpFiles = {
                    uri: response.assets[0]?.uri,
                    type: response.assets[0]?.type,
                    name: response.assets[0]?.fileName,
                };
                setIsChose(true);
                setImage(response?.assets[0]?.uri);
                onInputChange(tmpFiles, 'file');
            }

            // onInputChange(tmpFiles, 'file');
            // onUpdateProfile(tmpFiles)
        });
    };

    const onSubmit = () => {
        const formsdata = new FormData();

        formsdata.append('name', form.name);
        formsdata.append('email', form.email);
        formsdata.append('phone', form.phone);

        // formsdata.append('ahli_waris', form.ahli_waris)
        formsdata.append('no_ktp', form.no_ktp)

        isChose
            ? formsdata.append('file', form.file)
            : formsdata.append('file', '');
        formsdata.append('password', form.password);
        formsdata.append('password_confirmation', form.password_confirmation);
        console.log('kokops', formsdata);
        Axios.post(`${api}/user/update`, formsdata, {
            headers: {
                'Content-Type': 'multipart/form-data',
                key: key,
                Authorization: 'Bearer ' + tokens,
            },
        })
            .then(res => {
                // console.log('kokooko', res.data);
                res.data.success === true
                    ? res.data.data_otp == null
                        ? (ToastAndroid.show(
                            'Data Profile berhasil di ubah',
                            ToastAndroid.SHORT,
                        ),
                            navigation.goBack())
                        : navigation.navigate('Verif', {
                            type: 3,
                            id: form.id,
                            email: form.email_old,
                        })
                    : Alert.alert('Peringatan', res.data.message);

                // setload(false)
            })
            .catch(err => {
                console.log('aass', err.response.data);
                // Alert.alert('Peringatan', err.response.data.message);
                // setload(false)
            });
    };

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View>
                    <View style={{ height: 44 }}>
                        <Gradient />
                    </View>
                    <View style={{ position: 'absolute', alignSelf: 'center' }}>
                        <Image
                            source={
                                form.file
                                    ? isChose === false
                                        ? { uri: form.file }
                                        : { uri: image }
                                    : require('../../Assets/image/person1.png')
                            }
                            style={{ width: 96, height: 96, borderRadius: 100 }}
                        />
                        <TouchableOpacity
                            style={{
                                backgroundColor: colors.white,
                                width: 32,
                                height: 32,
                                borderRadius: 32,
                                position: 'absolute',
                                alignItems: 'center',
                                justifyContent: 'center',
                                right: 0,
                                bottom: 0,
                                elevation: 6,
                            }}
                            onPress={() => chooseFile('photo')}>
                            <Image
                                source={require('../../Assets/icon/pen.png')}
                                style={{ width: 18, height: 18 }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.wrapBody}>
                        <InputComp
                            // disabled={true}
                            text={'Nama Lengkap'}
                            placeholder={'masukan nama'}
                            value={form.name}
                            onChange={value => onInputChange(value, 'name')}
                        />

                        <InputComp
                            text={'E-Mail'}
                            placeholder={'masukan email'}
                            value={form.email}
                            onChange={value => onInputChange(value, 'email')}
                        />

                        <InputComp
                            text={'No. Telepon'}
                            placeholder={'masukan telepon'}
                            value={form.phone}
                            onChange={value => onInputChange(value, 'phone')}
                        />

                        {
                            typeAkun === 'agent' ?
                                <>
                                    {/* <InputComp
                                        text={'Ahli Waris'}
                                        placeholder={'masukan ahli waris'}
                                        value={form.ahli_waris}
                                        onChange={value => onInputChange(value, 'ahli_waris')}
                                    /> */}
                                    <InputComp
                                        text={'No KTP'}
                                        placeholder={'masukan no KTP'}
                                        value={form.no_ktp}
                                        onChange={value => onInputChange(value, 'no_ktp')}
                                    />
                                </>
                                : false
                        }

                        <InputComp
                            text={'Password'}
                            placeholder={'masukan password'}
                            value={form.password}
                            onChange={value => onInputChange(value, 'password')}
                            secure={true}
                        />

                        <InputComp
                            text={'Konfirmasi Password'}
                            placeholder={'masukan konfirmasi password'}
                            value={form.password_confirmation}
                            onChange={value => onInputChange(value, 'password_confirmation')}
                            secure={true}
                        />

                        {form.password.length >= 8 &&
                            form.password_confirmation.length >= 8 ? (
                            form.password === form.password_confirmation ? (
                                <View style={{ marginVertical: 16, alignSelf: 'flex-start' }}>
                                    <Text style={styles.textFooter}>Password sesuai</Text>
                                </View>
                            ) : (
                                <View style={{ marginVertical: 16, alignSelf: 'flex-start' }}>
                                    <Text style={[styles.textFooter, { color: colors.red }]}>
                                        Password Tidak sesuai
                                    </Text>
                                </View>
                            )
                        ) : (
                            <View style={{ height: 30 }} />
                        )}

                        <BtnPrimary title={'Simpan Perubahan'} pressed={() => onSubmit()} />
                        {/* <BtnPrimary title={'Simpan Perubahan'} pressed={() => navigation.navigate('Verif', 'Regist')} /> */}
                        {/* <BtnPrimary title={'Simpan Perubahan'} pressed={() => console.log('simpan', form)} /> */}
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default EditAkun;

const styles = StyleSheet.create({
    wrapBody: { marginTop: 65, padding: 16 },
    containInput: { borderBottomWidth: 0 },
    labelInput: {
        fontWeight: 'bold',
        color: colors.black,
        fontSize: 12,
        marginTop: 16,
    },
    inputContainer: {
        marginTop: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        paddingHorizontal: 0,
        height: 48,
    },
    textFooter: { color: '#12B76A', fontSize: 12 },
});
