import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import colors from '../../Styles/Colors';

const Horizontal = () => {
  return (
    <View
      style={{
        borderBottomColor: colors.grayShading,
        borderBottomWidth: 1,
        marginVertical: 12,
      }}
    />
  );
};

export default Horizontal;

const styles = StyleSheet.create({});
