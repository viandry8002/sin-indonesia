import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Input } from 'react-native-elements';
import colors from '../../Styles/Colors';

const InputComp = ({
  inputfocus,
  press,
  text,
  placeholder,
  value,
  onChange,
  secure,
  ricon,
  licon,
  mlength,
  tkeyboard,
  disabled
}) => {
  return (
    <View >
      <Text style={styles.labelInput}>{text}</Text>
      <Input
        disabled={disabled}
        onPressIn={press}
        label={false}
        placeholder={placeholder}
        style={{ fontSize: 12 }}
        inputContainerStyle={styles.containInput}
        containerStyle={styles.inputContainer}
        value={value}
        onChangeText={onChange}
        secureTextEntry={secure}
        rightIcon={ricon}
        leftIcon={licon}
        maxLength={mlength}
        showSoftInputOnFocus={inputfocus}
        keyboardType={tkeyboard}
      />
    </View>
  );
};

export default InputComp;
const styles = StyleSheet.create({
  containInput: { borderBottomWidth: 0 },
  labelInput: {
    fontWeight: 'bold',
    color: colors.black,
    fontSize: 12,
    marginTop: 16,
  },
  inputContainer: {
    marginTop: 8,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.gray6,
    paddingHorizontal: 0,
    height: 48,
  },
});
