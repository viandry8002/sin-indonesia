import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { TextMask } from 'react-native-masked-text';
import { Modalize } from 'react-native-modalize';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import { api, key } from '../../Variable';
import Horizontal from './Horizontal';

const ListProduct = ({ navigation, route }) => {
  const [refreshing, setRefreshing] = useState(false);
  const [categories, setCategories] = useState([]);
  const [listProduct, setListProduct] = useState([]);
  const [choseCategory, setchoseCategory] = useState();
  const modalKategori = useRef(null);
  const [bodyProduct, setBodyProduct] = useState();
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  const onOpen = modal => {
    modal.current?.open();
  };

  const onClose = modal => {
    modal.current?.close();
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData();
    });
    return unsubscribe;
  }, []);

  const getData = () => {
    //category
    Axios.get(`${api}/product/categories`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        setCategories(res.data.data);
      })
      .catch(err => {
        console.log(err);
      });

    //categories
    // getProduct(
    //   route.params.idc === 'undefined' || route.params.type === 'undefined'
    //     ? ('', '')
    //     : route.params?.idc,
    //   route.params?.type,
    //   route.params?.search,
    // );
    getProduct(route.params?.idc, route.params?.type, route.params?.search);
    setRefreshing(false);
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData();
    }, 2000);
  };

  const getProduct = (idc, type, search) => {
    setchoseCategory(idc);
    const tbody1 = {
      multi_img: 0,
      type: type,
      per_page: '',
      category_id: idc,
      search: search,
    };
    console.log('aaaazz', tbody1);
    Axios.post(`${api}/product`, tbody1, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('res1', res.data.data);
        setBodyProduct(tbody1),
          setListProduct(res.data.data);
        res.data.next_page_url === null
          ? (sethasScrolled(false), setPass(true))
          : setNextLink(res.data.next_page_url);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.post(`${nextLink}`, bodyProduct, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setListProduct([...listProduct, ...res.data.data]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderCategory = ({ item }) => (
    <TouchableOpacity
      style={
        choseCategory === item.id
          ? styles.wrapRenderCategoryActive
          : styles.wrapRenderCategory
      }
      onPress={() => {
        choseCategory === item.id
          ? (getProduct(0, '', ''), setchoseCategory(0))
          : getProduct(item.id, '', '');
      }}>
      <Text
        style={
          choseCategory === item.id
            ? styles.produkTitleActive
            : styles.produkTitle
        }>
        {item.title}
      </Text>
    </TouchableOpacity>
  );

  const renderProduct = ({ item }) => (
    <TouchableOpacity
      style={styles.wrapRender}
      onPress={() => navigation.navigate('DtlProduct', item.id)}>
      <Image
        source={
          item.file
            ? { uri: item.file }
            : require('../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <TextMask
          type={'money'}
          options={{
            precision: 0,
            separator: '.',
            delimiter: '.',
            unit: 'Rp ',
            suffixUnit: '',
          }}
          value={item.price}
          style={styles.produkPrice}
          numberOfLines={1}
        />
      </View>
    </TouchableOpacity>
  );

  const renderListCategory = ({ item }) => (
    <TouchableOpacity
      onPress={() => {
        choseCategory === item.id
          ? (getData(0), onClose(modalKategori))
          : (getProduct(item.id), onClose(modalKategori));
      }}>
      <Text style={styles.produkTitle}>{item.title}</Text>
      <Horizontal />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
      <View style={{ height: 60 }}>
        <FlatList
          data={categories}
          renderItem={renderCategory}
          horizontal
          // ListHeaderComponent={
          //   <TouchableOpacity
          //     style={styles.wrapHeaderCategory}
          //     onPress={() => onOpen(modalKategori)}>
          //     <Text style={styles.produkTitle}>Pilih Kategori</Text>
          //     <Ionicons
          //       name="chevron-down"
          //       size={18}
          //       color={colors.gray3}
          //       style={{marginLeft: 8}}
          //     />
          //   </TouchableOpacity>
          // }
          showsHorizontalScrollIndicator={false}
          style={styles.flatList}
        />
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        data={listProduct}
        renderItem={renderProduct}
        style={{ margin: 4 }}
        numColumns={2}
        ListEmptyComponent={() =>
          refreshing === false ? (
            <View style={{ flex: 1, alignItems: 'center', marginTop: 120 }}>
              <Image
                source={require('../../Assets/image/no-data.png')}
                style={{ width: 200, height: 200 }}
                resizeMode={'contain'}
              />
              <Text style={[styles.subtitle, { color: colors.black }]}>
                Data tidak ditemukan
              </Text>
            </View>
          ) : (
            false
          )
        }
        ListFooterComponent={() =>
          hasScrolled === true ? (
            <View style={{ alignItems: 'center' }}>
              <Text style={{ color: colors.black }}>Load more ....</Text>
            </View>
          ) : (
            <View style={{ height: 130 }} />
          )
        }
        onScroll={() => (pass === false ? onScroll() : false)}
        onEndReached={() => (nextLink === null ? false : handleLoadMore())}
      />

      {/* modalize category */}
      <Modalize ref={modalKategori} withHandle={false} snapPoint={250}>
        <View style={{ padding: 16 }}>
          <View style={styles.wrapDetailOrder}>
            <Text style={styles.title}>Pilih Pengiriman</Text>
            <MaterialCommunityIcons
              name="close"
              size={24}
              color={colors.black}
              onPress={() => onClose(modalKategori)}
            />
          </View>
          <FlatList
            style={{ marginTop: 8 }}
            showsVerticalScrollIndicator={false}
            data={categories}
            renderItem={renderListCategory}
          />
        </View>
      </Modalize>
    </SafeAreaView>
  );
};

export default ListProduct;

const styles = StyleSheet.create({
  wrapHeaderCategory: {
    height: 36,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderColor: colors.gray6,
    marginHorizontal: 4,
    marginVertical: 8,
    justifyContent: 'center',
    borderRadius: 100,
    flexDirection: 'row',
    alignItems: 'center',
  },
  flatList: {
    elevation: 4,
    backgroundColor: colors.white,
    paddingBottom: 8,
    marginBottom: 8,
  },
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 0.5,
    margin: 4,
    height: 190,
  },
  // wrapRender: { backgroundColor: colors.white, elevation: 3, borderRadius: 7, margin: 8, height: 190, width: 160 },
  imageRender: { width: '100%', height: 107 },
  wrapProduk: { margin: 8, justifyContent: 'space-between', flex: 1 },
  produkTitle: { fontSize: 12, color: colors.black },
  produkTitleActive: { fontSize: 12, color: colors.primary },
  produkPrice: { fontSize: 16, color: colors.primary, fontWeight: 'bold' },

  // Modalize
  title: { marginBottom: 8, fontWeight: 'bold', color: colors.black },
  wrapDetailOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  wrapRenderCategory: {
    height: 36,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderColor: colors.gray6,
    marginHorizontal: 4,
    marginVertical: 8,
    justifyContent: 'center',
    borderRadius: 100,
  },
  wrapRenderCategoryActive: {
    height: 36,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderColor: colors.gray6,
    marginHorizontal: 4,
    marginVertical: 8,
    justifyContent: 'center',
    borderRadius: 100,
    backgroundColor: colors.blueShading,
  },
  subtitle: { fontSize: 12, textAlign: 'center', marginTop: 8 },
});
