import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

const Gradient = () => {
    return (
        <LinearGradient colors={['#3BA172', '#0B326D']} start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} style={{ flex: 1 }}></LinearGradient>
    );
};

export default Gradient;

const styles = StyleSheet.create({});
