import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import colors from '../../Styles/Colors';

const BtnDisable = ({ title }) => {
    return (
        <TouchableOpacity style={styles.TouchableStyle} disabled>
            <Text style={{ color: colors.white, fontSize: 12 }}>{title}</Text>
        </TouchableOpacity>
    );
};

export default BtnDisable;

const styles = StyleSheet.create({
    TouchableStyle: { backgroundColor: colors.gray4, height: 46, justifyContent: 'center', alignItems: 'center', borderRadius: 100 }
});
