import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Keyboard,
  Alert,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import React, { useState, useEffect } from 'react';
import colors from '../../Styles/Colors';
import BtnPrimary from './BtnPrimary';
import BtnDisable from './BtnDisable';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Axios from 'axios';
import { api, key } from '../../Variable';
import SpinnerLoad from './SpinnerLoad';
import { useBackHandler } from '@react-native-community/hooks';

const Verif = ({ navigation, route }) => {
  const params = route.params;
  const type = route.params.type;
  const idUser = route.params.id;
  const emailUser = route.params.email;

  const [load, setload] = useState(false);
  const [otp, setOtp] = useState('');
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const [success, setSuccess] = useState(0);
  const [count, setcount] = useState(0);
  const [disableCd, setdisableCd] = useState(false);

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStatus(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStatus(false);
    });

    countDown(60);
  }, []);

  const backAction = () => {
    Alert.alert(
      'Peringatan',
      'mohon maaf, anda harus menyelesaikan sesi ini!',
      [{ text: 'Ya', onPress: console.log('alert') }],
    );
    return true;
  };

  useBackHandler(backAction);

  const countDown = c => {
    setcount(c);
    if (c == 60) {
      let interval = setInterval(() => {
        setcount(prev => {
          setdisableCd(true);
          if (prev == 1) clearInterval(interval), setdisableCd(false);
          return prev - 1;
        });
      }, 1000);
    }
  };

  const resendToken = () => {
    disableCd === false
      ? Axios.post(
        `${api}/send-otp/${idUser}?do=${type}`,
        {},
        {
          headers: {
            key: key,
          },
        },
      )
        .then(res => {
          console.log('aaaa', res.data);
          setdisableCd(true);
          countDown(60);
        })
        .catch(err => {
          console.log(err.response.data);
        })
      : false;
  };

  const submit = () => {
    // return console.log('aaazz', type);

    let form = {
      id: idUser,
      otp: otp,
      do: type,
    };
    setload(true);
    Axios.post(`${api}/_otp`, form, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        console.log('success', res.data);
        res.data.success === true
          ? (setSuccess(true),
            type === 1
              ? navigation.navigate('Success', {
                type: 1,
                title: 'Verifikasi Sukses',
                subtitle: 'Verifikasi sukses kamu bisa lanjut ke aplikasi',
                token: res.data.token,
                id: res.data.data.id,
              })
              : type === 2
                ? navigation.navigate('CreatePass', {
                  type: 2,
                  id: res.data.data.id,
                })
                : navigation.navigate('EditAkun'))
          : setSuccess(false);
        setload(false);
      })
      .catch(err => {
        setSuccess(false);
        setload(false);
        console.log('err', err.response);
        // Alert.alert('Peringatan', err.response.data.message)
      });

    // otp === '12312' ? (setSuccess(true), navigation.navigate('Success', type === 1 ? { title: 'Verifikasi Sukses', subtitle: 'Verifikasi sukses kamu bisa lanjut login ke aplikasi' } : { title: 'Password Berhasil Direset', subtitle: 'Password baru berhasil dibuat, anda bisa langsung login dengan password baru anda' }))
    //     : setSuccess(false);
  };

  return (
    <SafeAreaView style={styles.warap}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <SpinnerLoad loads={load} />
          <Image
            source={require('../../Assets/image/imgOtp.png')}
            style={styles.image}
            resizeMode="contain"
          />
          <Text style={styles.title}>Verifikasi OTP</Text>
          <Text style={styles.subtitle}>
            Kami baru saja mengirimi anda kode OTP ke alamat email{' '}
            <Text style={{ color: colors.black }}>{params.email}</Text>
          </Text>
          <TouchableOpacity onPress={() => resendToken()} disabled={disableCd === false
            ? false : true} >
            <Text style={[styles.countd, disableCd === false
              ? { color: colors.primary } : { color: colors.black }]} >
              Kirim Ulang ({count}s)
            </Text>
          </TouchableOpacity>

          <OTPInputView
            style={{ width: '100%', height: 85, marginTop: 32 }}
            pinCount={5}
            autoFocusOnLoad
            codeInputFieldStyle={[
              {
                fontSize: 18,
                color: colors.black,
                fontWeight: 'bold',
                width: 63,
                height: 70,
                backgroundColor: colors.gray6,
                borderWidth: 0,
                borderRadius: 7,
              },
              success === false
                ? {
                  borderWidth: 1,
                  borderColor: colors.red,
                }
                : false,
            ]}
            codeInputHighlightStyle={colors.primary}
            onCodeChanged={code => {
              setOtp(code);
            }}
          />

          {success === false ? (
            <Text
              style={{ color: colors.red, fontSize: 12, textAlign: 'center' }}>
              Kode Otp Salah
            </Text>
          ) : (
            false
          )}
        </View>
      </ScrollView>

      <View style={{ bottom: 48 }}>
        {keyboardStatus === true ? (
          false
        ) : otp.length < 5 ? (
          <BtnDisable title={'Verifikasi'} />
        ) : (
          <BtnPrimary title={'Verifikasi'} pressed={() => submit()} />
        )}
      </View>
    </SafeAreaView>
  );
};

export default Verif;

const styles = StyleSheet.create({
  warap: { flex: 1, backgroundColor: colors.white, paddingHorizontal: 16 },
  image: { width: 250, height: 190, marginTop: 44 },
  title: { fontSize: 18, fontWeight: 'bold', marginTop: 16, color: colors.black },
  subtitle: {
    fontSize: 12,
    textAlign: 'center',
    width: 250,
    marginTop: 8,
    color: colors.black,
  },
  countd: { fontSize: 12, marginTop: 16 },
});
