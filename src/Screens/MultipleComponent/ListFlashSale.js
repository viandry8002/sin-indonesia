import Axios from 'axios';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import {TextMask} from 'react-native-masked-text';
import * as Progress from 'react-native-progress';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Styles/Colors';
import {api, key} from '../../Variable';
import FormatTime from './FormatTime';

const ListFlashSale = ({navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [listProduct, setListProduct] = useState([]);
  // time
  const [endDate, setendDate] = useState('');
  const [datess, setdatess] = useState('');
  // scrolled
  const [nextLink, setNextLink] = useState('');
  const [hasScrolled, sethasScrolled] = useState(false);
  const [pass, setPass] = useState(false);

  useEffect(() => {
    getData();
    let timer = setInterval(() => {
      FormatTime(moment(endDate).diff(moment(), 'seconds')) === '00:00:00'
        ? clearInterval(timer)
        : setdatess(moment(endDate).diff(moment(), 'seconds'));
    }, 1000);
  }, [endDate]);

  const getData = () => {
    //category
    Axios.get(`${api}/flashsale`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        res.data.data === null
          ? setendDate(false)
          : (setendDate(res.data.data.end_date),
            // setendDate('2022-03-15 11:06:00')
            setListProduct(res.data.data.items),
            res.data.next_page_url === null
              ? (sethasScrolled(false), setPass(true))
              : setNextLink(res.data.next_page_url));
      })
      .catch(err => {
        console.log(err);
      });

    setRefreshing(false);
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      getData();
    }, 3000);
  };

  // pass scrolled
  const onScroll = () => {
    sethasScrolled(true);
  };

  const handleLoadMore = () => {
    if (!hasScrolled) {
      return null;
    }

    Axios.get(`${nextLink}`, {
      headers: {
        key: key,
      },
    })
      .then(res => {
        sethasScrolled(!hasScrolled),
          setListProduct([...listProduct, ...res.data.data.items]),
          setNextLink(res.data.next_page_url);

        res.data.next_page_url === null
          ? (sethasScrolled(!hasScrolled), setPass(true))
          : false;
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderFlashSale = ({item}) => (
    <TouchableOpacity
      style={styles.wrapRender}
      onPress={() => navigation.navigate('DtlProduct', item.id)}>
      <Image
        source={
          item.file
            ? {uri: item.file}
            : require('../../Assets/image/notfound.png')
        }
        style={styles.imageRender}
        resizeMode={'contain'}
      />
      <View style={styles.wrapProduk}>
        <Text style={styles.produkTitle} numberOfLines={2}>
          {item.title}
        </Text>
        <TextMask
          type={'money'}
          options={{
            precision: 0,
            separator: '.',
            delimiter: '.',
            unit: 'Rp ',
            suffixUnit: '',
          }}
          value={item.price}
          style={styles.produkPrice}
          numberOfLines={1}
        />
        <Progress.Bar
          progress={100 - (item.qty - item.sold_flahsale) / 100}
          width={90}
          color={colors.red}
          borderColor="transparent"
          unfilledColor={colors.redShading}
        />
        <Text style={{fontSize: 10}}>
          {item.qty - item.sold_flahsale} Tersisa
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
      <FlatList
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        data={listProduct}
        renderItem={renderFlashSale}
        style={{marginHorizontal: 4}}
        numColumns={2}
        ListHeaderComponent={() =>
          endDate ? (
            <View style={styles.time}>
              <Text style={{color: colors.white, fontSize: 12}}>
                Berakhir Dalam
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <MaterialCommunityIcons
                  name="clock-time-three-outline"
                  size={16}
                  style={{color: colors.white}}
                  // color={colors.white}
                />
                <Text style={{color: colors.white, marginLeft: 8}}>
                  {FormatTime(moment(endDate).diff(moment(), 'seconds'))}
                </Text>
                {/* <Text style={{ color: colors.white, marginLeft: 8 }}>{formatTime(moment("2022-02-22 14:48:00").diff(moment(), 'seconds'))}</Text> */}
              </View>
            </View>
          ) : (
            false
          )
        }
        // ListFooterComponent={() =>
        //   hasScrolled === true ? (
        //     <View style={{alignItems: 'center'}}>
        //       <Text>Load more ....</Text>
        //     </View>
        //   ) : (
        //     <View style={{height: 130}} />
        //   )
        // }
        // onScroll={() => (pass === false ? onScroll() : false)}
        // onEndReached={() => (nextLink === null ? false : handleLoadMore())}
      />
    </SafeAreaView>
  );
};

export default ListFlashSale;

const styles = StyleSheet.create({
  // render
  wrapRender: {
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 7,
    flex: 0.5,
    margin: 4,
    height: 215,
  },
  imageRender: {width: '100%', height: 107},
  wrapProduk: {margin: 8, justifyContent: 'space-between', flex: 1},
  produkTitle: {fontSize: 12, color: colors.black},
  produkPrice: {fontSize: 16, color: colors.primary, fontWeight: 'bold'},
  time: {
    marginHorizontal: 16,
    marginVertical: 8,
    backgroundColor: colors.red,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    borderRadius: 100,
    height: 32,
    justifyContent: 'space-between',
  },
});
