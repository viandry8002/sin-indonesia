import { View, Text, StyleSheet } from 'react-native';
import React, { useEffect } from 'react';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder'
import LinearGradient from 'react-native-linear-gradient';

const TextShimmer = ({ type }) => {

    return (
        <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            style={type === 'large' ? styles.large : (type === 'small' ? styles.small : styles.normal)}
        />
    );
};

export default TextShimmer;

const styles = StyleSheet.create({
    small: { borderRadius: 100, width: 44 },
    normal: { borderRadius: 100, width: 100 },
    large: { borderRadius: 100 },
});
