import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native';
import React from 'react';
import colors from '../../Styles/Colors';
import BtnPrimary from './BtnPrimary';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';

const Success = ({ route, navigation }) => {
  console.log('dsdsdsdsd', route.params);
  const saveToken = async (token, type) => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
        await AsyncStorage.setItem('type_akun', type);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onSubmit = () => {
    route.params.type === 3
      ? navigation.navigate('TukarPoint')
      : route.params.type === 1
        ? (OneSignal.setAppId('8a32cd5b-0a76-4f8b-9e86-72a92c338bb2'), //app id
          OneSignal.setLogLevel(6, 0),
          OneSignal.setExternalUserId(route.params.id + '', results => {
            console.log('Results of setting external user id ' + route.params.id);
            console.log(results);
          }),
          saveToken(route.params.token, route.params.type),
          navigation.reset({
            index: 0,
            routes: [{ name: 'BerandaS' }],
          }))
        : navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={styles.warap}>
      <View style={{ flex: 1, alignItems: 'center' }}>
        <Image
          source={require('../../Assets/image/success.png')}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.title}>{route.params.title}</Text>
        <Text style={styles.subtitle}>{route.params.subtitle}</Text>
      </View>

      <View style={{ bottom: 48 }}>
        <BtnPrimary title={'Berikutnya'} pressed={() => onSubmit()} />
      </View>
    </SafeAreaView>
  );
};

export default Success;

const styles = StyleSheet.create({
  warap: { flex: 1, backgroundColor: colors.white, paddingHorizontal: 16 },
  image: { width: 152, height: 152, marginTop: 44 },
  title: { fontSize: 18, fontWeight: 'bold', marginTop: 16, color: colors.black },
  subtitle: { fontSize: 12, textAlign: 'center', marginTop: 8 },
});
