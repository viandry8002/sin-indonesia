import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import colors from '../../Styles/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Horizontal from './Horizontal';
import FormatMoney from './FormatMoney';

const Expedition = ({ pressed1, pressed2, typeDelivery, courier }) => {
    const switchShipment = shipment => {
        switch (shipment) {
            case 'store-address':
                return 'Ambil Di Toko (Bebas Biaya)';
            case 'domisili':
                return 'Kurir Toko';
            case 'shipdeo-pricings':
                return 'Ekspedisi';
            default:
                break;
        }
    };

    return (
        <View>
            {typeDelivery === null || courier === null ? (
                <TouchableOpacity
                    style={styles.btnSend}
                    onPress={pressed1}>
                    <MaterialCommunityIcons
                        name="truck-fast-outline"
                        size={24}
                        color={colors.primary}
                        style={styles.iconSend}
                    />
                    <Text style={styles.textSend}>Pilih Pengiriman</Text>
                    <AntDesign name="right" size={16} color={colors.primary} />
                </TouchableOpacity>
            ) : (
                <View style={styles.btnSend}>
                    <View style={{ flex: 1, paddingVertical: 8 }}>
                        <TouchableOpacity
                            style={styles.wrapCourir}
                            onPress={pressed1}>
                            <Text style={[styles.itemPrice2, styles.flexRight]}>
                                {switchShipment(typeDelivery)}
                            </Text>
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                        <Horizontal />
                        <TouchableOpacity
                            style={styles.wrapCourir}
                            onPress={pressed2}>
                            {typeDelivery === 'store-address' ? (
                                <Text style={[styles.itemPrice2, styles.flexRight]}>
                                    {courier.address}
                                </Text>
                            )
                                : courier.domisili ? (
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.itemPrice2, styles.flexRight]}>
                                            {courier.domisili}
                                        </Text>
                                        <FormatMoney value={courier?.price}
                                            style={styles.itemTitle} />
                                    </View>
                                )
                                    : (
                                        <View style={{ flex: 1 }}>
                                            <Text style={[styles.itemPrice2, styles.flexRight]}>
                                                {courier.courierCode} - {courier.service}
                                            </Text>
                                            <FormatMoney value={courier?.price}
                                                style={styles.itemTitle} />
                                        </View>
                                    )}
                            <AntDesign name="right" size={16} color={colors.primary} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    )
}

export default Expedition

const styles = StyleSheet.create({
    btnSend: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        marginVertical: 8,
        flex: 1,
    },
    iconSend: { marginRight: 8 },
    textSend: { flex: 1, fontSize: 12, color: colors.black },
    itemPrice2: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
    wrapCourir: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
    },
    itemTitle: { fontSize: 12, color: colors.black },
})