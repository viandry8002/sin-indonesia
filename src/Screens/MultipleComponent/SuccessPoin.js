import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import colors from '../../Styles/Colors'
import BtnPrimary from './BtnPrimary'
import BtnOutline from './BtnOutline'
import Ionicons from 'react-native-vector-icons/Ionicons';

const SuccessPoin = () => {
    return (
        <SafeAreaView style={styles.warap}>
            <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 16 }}>
                <Image
                    source={require('../../Assets/image/success.png')}
                    style={styles.image}
                    resizeMode="contain"
                />
                <Text style={styles.title}>Poin berhasil di tukar</Text>
                <Text style={styles.subtitle}>Menunggu konfirmasi dari admin SIN Indonesia</Text>

                <View style={{ width: '100%', marginVertical: 16 }}>
                    <BtnPrimary title={'Hubungi CS'} icon={<Ionicons name='call' size={16} style={{ color: colors.white, marginRight: 10 }} />} />
                </View>
                <View style={{ width: '100%' }}>
                    <BtnOutline title={'Kembali Ke Beranda'} />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default SuccessPoin

const styles = StyleSheet.create({
    warap: { flex: 1, backgroundColor: colors.white, paddingHorizontal: 16 },
    image: { width: 152, height: 152, marginTop: 44 },
    title: { fontSize: 18, fontWeight: 'bold', marginTop: 16, color: colors.black },
    subtitle: { fontSize: 12, textAlign: 'center', marginTop: 8 },
})