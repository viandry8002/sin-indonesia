import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../Styles/Colors';

const Shipment = ({ address, pressed }) => {
    return (
        <View>
            {address ? (
                <TouchableOpacity
                    style={styles.btnSend}
                    onPress={pressed}>
                    <Ionicons
                        name="location-outline"
                        size={24}
                        color={colors.primary}
                        style={styles.iconSend}
                    />
                    <View style={styles.flexRight}>
                        <Text style={styles.itemPrice2}>{address.recipient_name}</Text>
                        <Text style={styles.textSend}>{address.recipient_phone}</Text>
                        <Text style={styles.textSend}>{address.address}</Text>
                    </View>
                    <AntDesign name="right" size={16} color={colors.primary} />
                </TouchableOpacity>
            ) : (
                <TouchableOpacity
                    style={styles.btnSend}
                    onPress={pressed}>
                    <Ionicons
                        name="location-outline"
                        size={24}
                        color={colors.primary}
                        style={styles.iconSend}
                    />
                    <Text style={styles.textSend}>Masukan Alamat</Text>
                    <AntDesign name="right" size={16} color={colors.primary} />
                </TouchableOpacity>
            )}
        </View>
    )
}

export default Shipment

const styles = StyleSheet.create({
    btnSend: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        marginVertical: 8,
        flex: 1,
    },
    iconSend: { marginRight: 8 },
    textSend: { flex: 1, fontSize: 12, color: colors.black },
    flexRight: { flex: 1, marginRight: 10 },
    itemPrice2: { fontSize: 12, color: colors.black, fontWeight: 'bold' },
})