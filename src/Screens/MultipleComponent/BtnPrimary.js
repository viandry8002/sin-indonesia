import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import colors from '../../Styles/Colors';

const BtnPrimary = ({ title, pressed, icon }) => {
    return (
        <TouchableOpacity style={styles.TouchableStyle}
            onPress={pressed}>
            {icon}
            <Text style={{ color: colors.white, fontSize: 14, fontWeight: 'bold' }}>{title}</Text>
        </TouchableOpacity>
    );
};

export default BtnPrimary;

const styles = StyleSheet.create({
    TouchableStyle: { backgroundColor: colors.primary, height: 46, justifyContent: 'center', alignItems: 'center', borderRadius: 100, flexDirection: 'row' }
});
