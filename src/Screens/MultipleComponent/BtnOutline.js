import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import colors from '../../Styles/Colors';

const BtnOutline = ({ title, pressed, icon, defaultBtn }) => {
    return (
        <TouchableOpacity style={[styles.TouchableStyle, { borderColor: defaultBtn ? colors.grayShading : colors.primary, }]}
            onPress={pressed}>
            {icon}
            <Text style={[{ color: defaultBtn ? colors.black : colors.primary, fontSize: 14 }, icon ? { marginLeft: 8 } : false]}>{title}</Text>
        </TouchableOpacity>
    );
};

export default BtnOutline;

const styles = StyleSheet.create({
    TouchableStyle: { backgroundColor: colors.white, height: 46, justifyContent: 'center', alignItems: 'center', borderRadius: 100, borderWidth: 1, flexDirection: 'row' }
});
