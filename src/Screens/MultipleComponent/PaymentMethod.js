import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import colors from '../../Styles/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign'

const PaymentMethod = ({ pressed, payment }) => {
    return (
        <View>
            {payment === null ? (
                <TouchableOpacity
                    style={[styles.btnSend, { paddingVertical: 16 }]}
                    onPress={pressed}>
                    <Text style={styles.textSend}>Pilih Metode Pembayaran</Text>
                    <AntDesign name="right" size={16} color={colors.primary} />
                </TouchableOpacity>
            ) : (
                <TouchableOpacity
                    style={[styles.btnSend, { paddingVertical: 16 }]}
                    onPress={pressed}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={{ uri: payment?.file }}
                            style={{ width: 48, height: 20, marginRight: 32 }}
                            resizeMode="contain"
                        />
                        <Text style={{ color: colors.black }}>
                            {payment?.title}
                        </Text>
                    </View>
                    <AntDesign name="right" size={16} color={colors.primary} />
                </TouchableOpacity>
            )}
        </View>
    )
}

export default PaymentMethod

const styles = StyleSheet.create({
    btnSend: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.gray6,
        marginVertical: 8,
        flex: 1,
    },
    textSend: { flex: 1, fontSize: 12, color: colors.black },
})