import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native'
import React from 'react'
const windowWidth = Dimensions.get('window').width;

const SinggleBanner = ({ pressed, image }) => {
    return (
        <View style={{ marginTop: 16 }}>
            <TouchableOpacity onPress={pressed}>
                <Image
                    source={image}
                    style={{
                        width: windowWidth - 32,
                        height: 126,
                        borderRadius: 16,
                        alignSelf: 'center',
                    }}
                />
            </TouchableOpacity>
        </View>
    )
}

export default SinggleBanner

const styles = StyleSheet.create({})