import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions
} from 'react-native';
import { TextMask } from 'react-native-masked-text';
import colors from '../../Styles/Colors';
import FormatMoney from './FormatMoney';

const windowWidth = Dimensions.get('window').width;

const PoinBelanjaElevate = ({ poin, pressed }) => {
    return (
        <View style={styles.wrapPoint}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    source={require('../../Assets/icon/poinBelanja.png')}
                    style={styles.point}
                />
                <View style={{ flex: 1 }}>
                    <FormatMoney value={poin}
                        style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            color: colors.black,
                        }} unit={''} />
                    <Text style={{ fontSize: 10, color: colors.gray3 }}>
                        Poin anda
                    </Text>
                </View>
                <TouchableOpacity
                    style={styles.tukarPoint}
                    onPress={pressed}
                >
                    <Text style={{ color: colors.primary }}>Tukar Poin</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default PoinBelanjaElevate

const styles = StyleSheet.create({
    wrapPoint: {
        marginHorizontal: 16,
        backgroundColor: colors.white,
        elevation: 6,
        borderRadius: 7,
        padding: 12,
        // position: 'absolute',
        width: windowWidth - 32
    },
    point: { width: 36, height: 36, marginRight: 10 },
    tukarPoint: {
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.primary,
        height: 34,
        width: 94,
        justifyContent: 'center',
        alignItems: 'center',
    },
})