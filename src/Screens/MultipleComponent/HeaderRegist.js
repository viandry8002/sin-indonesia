import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import StepIndicator from 'react-native-step-indicator';
import colors from '../../Styles/Colors';

const HeaderRegist = ({curentp, title, subtitle}) => {
  const labels = ['Daftar Agen', 'Pilih Paket', 'Pembayaran'];
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 2,
    stepStrokeCurrentColor: '#ffffff',
    stepStrokeWidth: 2,
    stepStrokeFinishedColor: '#ffffff',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#ffffff',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: colors.primary,
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: colors.primary,
    stepIndicatorLabelFontSize: 12,
    currentStepIndicatorLabelFontSize: 12,
    stepIndicatorLabelCurrentColor: '#ffffff',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: colors.primary,
    labelColor: '#ffffff',
    labelSize: 12,
    currentStepLabelColor: '#ffffff',
  };

  return (
    <View style={styles.wrapHeader}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
      <View style={{height: 8}} />
      <StepIndicator
        customStyles={customStyles}
        currentPosition={curentp}
        labels={labels}
        stepCount={3}
      />
    </View>
  );
};

export default HeaderRegist;

const styles = StyleSheet.create({
  wrapHeader: {
    marginHorizontal: 16,
    width: '100%',
    height: 140,
    alignSelf: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 28,
    color: colors.white,
    textAlign: 'center',
  },
  subtitle: {
    color: colors.white,
    marginTop: 8,
    fontSize: 12,
    textAlign: 'center',
  },
});
