import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect} from 'react';
import {StyleSheet, View, Alert} from 'react-native';
import OneSignal from 'react-native-onesignal';
import SpinnerLoad from './SpinnerLoad';

const GoLogout = ({navigation, route}) => {
  useEffect(() => {
    // route.params.msg
    //   ? alert(route.params.msg)
    //   : route.params === null
    //   ? toLogout()
    //   : false;
    route.params?.msg ? alert(route.params.msg) : toLogout();
  }, []);

  const alert = masage => {
    Alert.alert('Peringatan', masage, [
      {
        text: 'ok',
        onPress: () => toLogout(),
      },
    ]);
  };

  const toLogout = async () => {
    try {
      await AsyncStorage.removeItem('api_token');
      OneSignal.removeExternalUserId(results => {
        // The results will contain push and email success statuses
        console.log('Results of removing external user id');
        console.log(results);
      });
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (exception) {
      console.log(exception);
    }
  };

  return <SpinnerLoad loads={true} />;
};

export default GoLogout;
