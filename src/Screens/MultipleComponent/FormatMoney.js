import React from 'react';
import { StyleSheet } from 'react-native';
import { TextMask } from 'react-native-masked-text';

const FormatMoney = ({ value, style, numberlines, unit }) => {
  return (
    <TextMask
      type={'money'}
      options={{
        precision: 0,
        separator: '.',
        delimiter: '.',
        unit: unit === '' ? '' : unit ? unit : 'Rp',
        suffixUnit: '',
      }}
      style={style}
      value={value}
      numberOfLines={numberlines ? numberlines : 1}
    />
  );
};

export default FormatMoney;

const styles = StyleSheet.create({});
