import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import colors from '../../Styles/Colors'
import BtnPrimary from './BtnPrimary'
import BtnOutline from './BtnOutline'
import Font from '../../Styles/Font'

const DirectLogin = ({ navigation }) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 80 }}>
                <Image source={require('../../Assets/image/login.png')} style={{ width: 225, height: 200 }} />
                <Text style={{ fontSize: 12, color: colors.black, width: 300, marginBottom: 32, marginTop: 16, textAlign: 'center' }}>Ayo daftar untuk membuka fitur lainnya</Text>
            </View>
            <View style={{ marginHorizontal: 62, marginBottom: 16 }}>
                <BtnPrimary title={'Login'} pressed={() => navigation.navigate('Login')} />
            </View>
            <View style={{ marginHorizontal: 62, marginBottom: 16 }}>
                <BtnOutline title={'Daftar Sebagai Member'} pressed={() => navigation.navigate('Register')} />
            </View>
            <View style={{ marginHorizontal: 62 }}>
                <TouchableOpacity
                    style={[styles.TouchableStyle]}
                    onPress={() => navigation.navigate('Step1')}>
                    <Text style={[Font.semiBold14, { color: colors.white }]}>Daftar Sebagai Agen</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

export default DirectLogin

const styles = StyleSheet.create({
    TouchableStyle: {
        backgroundColor: colors.greenPrimary,
        height: 46,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
})