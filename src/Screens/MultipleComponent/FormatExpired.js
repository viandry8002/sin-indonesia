import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const FormatExpired = (duration) => {
    var day = Math.floor(duration / 86400);
    var hours = Math.floor((duration % 86400) / 3600);
    var minutes = Math.floor((duration % 3600) / 60);
    if (day < 10) {
        day = '0' + day;
    }
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    return day + ' Hari : ' + hours + ' Jam : ' + minutes + ' Menit';
}

export default FormatExpired

const styles = StyleSheet.create({})