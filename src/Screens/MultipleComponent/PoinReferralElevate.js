import React from 'react'
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, Share, ToastAndroid } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import colors from '../../Styles/Colors'
import Font from '../../Styles/Font'
import FormatMoney from './FormatMoney'
import Horizontal from './Horizontal'
import TextShimmer from './Shimmer/TextShimmer'
import Clipboard from '@react-native-community/clipboard'
import HTML from 'react-native-render-html';

const windowWidth = Dimensions.get('window').width;

// const PoinReferralElevate = ({ loading, elevation, reward, codeInvite }) => {
const PoinReferralElevate = ({ elevation, dataReferal, btnHistory, accumulate }) => {
    const onShare = async (params) => {
        try {
            const result = await Share.share({
                message: params,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    const copyCode = (params) => {
        Clipboard.setString(params)
        ToastAndroid.showWithGravityAndOffset(
            "Kode telah tersalin",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            0,
            50
        );
    }

    const rightIcon = (icon, pressed) => {
        return <TouchableOpacity style={{ backgroundColor: colors.primary, width: 48, height: 48, alignItems: 'center', borderRadius: 100, justifyContent: 'center', marginLeft: 8 }}
            onPress={pressed}>
            {icon}
        </TouchableOpacity>
    }

    return (
        <View style={elevation ? styles.wrapPoint1 : styles.wrapPoint2}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    source={require('../../Assets/icon/poin.png')}
                    style={styles.point}
                />
                {
                    accumulate ?
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 10, color: colors.gray3 }}>
                                Sisa Reward Referral
                            </Text>
                            <FormatMoney value={dataReferal.poin} style={Font.bold18} unit={''} />
                            <Text style={{ fontSize: 10, color: colors.gray3 }}>
                                Akumulasi Reward Referral = <FormatMoney value={dataReferal.akumulasi_poin} unit={''} />
                            </Text>
                        </View> :
                        <View style={{ flex: 1 }}>
                            <FormatMoney value={dataReferal.poin} style={Font.bold18} unit={''} />
                            <Text style={{ fontSize: 10, color: colors.gray3 }}>
                                Reward Referral
                            </Text>
                        </View>
                }

            </View>
            <Horizontal />
            <Text style={Font.semiBold14}>Kode Undangan</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity style={{
                    padding: 16, borderWidth: 1, borderColor: colors.primary, borderRadius: 100, marginTop: 8, flexDirection: 'row', justifyContent: 'space-between',
                    flex: 1, backgroundColor: colors.primaryShading, alignItems: 'center'
                }}
                    onPress={() => { copyCode(dataReferal.agent_id) }}>
                    {/* {
                        loading ? <TextShimmer type={'normal'} /> : */}
                    {/* <Text style={Font.semiBold14}>{codeInvite == undefined ? '0' : codeInvite}</Text> */}
                    <Text style={[Font.semiBold14, { flex: 1 }]} numberOfLines={1}>{dataReferal.agent_id}</Text>
                    {/* <Text style={[Font.semiBold14, { flex: 1 }]}>kajshdjashdjaksjdhajskdahsjkajshdjakjsdhjaksjdhajskjdhajwkdjshjakwjdsjakjwhd</Text> */}
                    {/* } */}
                    <View style={{ flexDirection: 'row' }}>
                        <MaterialCommunityIcons
                            name="content-copy"
                            size={16}
                            color={colors.primary}
                        />
                        <Text style={[Font.semiBold12, { color: colors.primary }]}> Salin</Text>
                    </View>
                </TouchableOpacity>
                {rightIcon(<Ionicons
                    name="share-social-outline"
                    size={24}
                    color={colors.white}
                />, () => onShare(dataReferal.message_share_referral))}
                {rightIcon(<MaterialCommunityIcons
                    name="history"
                    size={24}
                    color={colors.white}
                />, btnHistory)}
            </View>
        </View>
    )
}

export default PoinReferralElevate

const styles = StyleSheet.create({
    wrapPoint1: {
        marginHorizontal: 16,
        backgroundColor: colors.white,
        elevation: 6,
        borderRadius: 7,
        padding: 12,
        width: windowWidth - 32,
        // marginBottom: 16
    },
    wrapPoint2: {
        marginHorizontal: 16,
        backgroundColor: colors.white,
        // elevation: 6,
        borderWidth: 1,
        borderRadius: 7,
        padding: 12,
        width: windowWidth - 32,
        // marginBottom: 16,
        borderColor: colors.grayShading
    },
    point: { width: 36, height: 36, marginRight: 10, borderRadius: 24 },
    tukarPoint: {
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.primary,
        height: 34,
        width: 94,
        justifyContent: 'center',
        alignItems: 'center',
    },
})